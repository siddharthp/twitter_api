sap.ui.core.Element.extend("custom.minMaxAvgBarControl", { metadata : {
		properties : {
			"Name" : {type : "string", group : "Misc", defaultValue : null},
			"Maximum" : {type : "int", group : "Misc", defaultValue : null},
			"Minimum" : {type : "int", group : "Misc", defaultValue : null},
			"Average" : {type : "int", group : "Misc", defaultValue : null}
		
		}
	}});
	/* Line chart: A UI5 custom control wrapping some D3.js code */
	sap.ui.core.Control.extend("control.minMaxAvgBar", { 
		/* the control API */
		metadata : {
			aggregations : {
				"items" : { type: "custom.minMaxAvgBarControl", multiple : true, singularName : "item"}
			},
			events: {
				"select" : {},
				"selectEnd": {}				
			}			
		},
	
		
		// the part creating the HTML:
		renderer : function(oRm, oControl) {	// static function, so use the given "oControl" instance instead of "this" in the renderer function
			oRm.write("<div"); 
			oRm.writeControlData(oControl);		// writes the Control ID and enables event handling - important!
			oRm.addClass("maxminavgchart");				// add a CSS class for styles common to all control instances
			oRm.writeClasses();					// this call writes the above class plus enables support for my.Bubble.addStyleClass(...)				
			oRm.write(">");
			oRm.write("</div>");
		},
		
		onAfterRendering: function() {
			var that = this;
			/* get the Items aggregation of the control and put the data into an array */
			var aItems = this.getItems();
		
			var data = [];
			for (var i=0;i<aItems.length;i++){
				var oEntry = {};
				for (var j in aItems[i].mProperties) {
					oEntry[j]=aItems[i].mProperties[j];
				}					
				data.push(oEntry);
			}
			//var containerWidth = jQuery.sap.byId(this.oParent.sId).width() || 800; // gets super parent width
			var containerWidth = $("#"+this.getId()).parent().width(); // gets immediate parent width
			var containerheight = $("#"+this.getId()).parent().height();
			var margin = {top: 15, right: 30, bottom: 20, left: 30},
			width = containerWidth- margin.left - margin.right,
			 height = containerheight - margin.top - margin.bottom;

			var formatPercent = d3.format(".0%");

		    var x = d3.scale.ordinal()
		      .rangeRoundBands([0, width], .9);

		    var y = d3.scale.linear()
		      .range([height, 0]);

		    var xAxis = d3.svg.axis()
		      .scale(x)
		      .orient("bottom")
		      .tickSize(-1);

		    var yAxis = d3.svg.axis()
		      .scale(y)
		      .orient("left") 
		      .ticks(4, "d")
		      .tickSize(-width, 0, 0)
		      //.ticks(d3.time.minutes, 15)
		      //.tickFormat(d3.time.minute, 15);

		
		     var chart1 = d3.select("body").append("svg")
		      .attr("class","maxminavgchart")
		      .attr("width", width + margin.left + margin.right)
		      .attr("height", height + margin.top + margin.bottom)
		      .append("g")
		      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		      x.domain(data.map(function(d) {
		        return d.Name;
		      }));
		      //x2.domain(data.map(function(d){return d.Average}));
		      y.domain([0, d3.max(data, function(d) {
		        return d.Maximum
		      })]);

		      chart1.append("g")
		        .attr("class", "x axis")
		        .attr("transform", "translate(0," + height + ")")
		        .call(xAxis);

		      chart1.append("g")
		        .attr("class", "y axis")
		        .call(yAxis)
		        .append("text")
		      .attr("transform", "rotate(90)")
		      .attr("y", 6)
		      .attr("dy", "1.71em")
		      .style("text-anchor", "start")
		      .text("Duration(mins)");
		      var eSel = chart1.selectAll(".line")
		        .data(data)
		        .enter();
		        
		      eSel.append("line")
		        .attr("class", "line")
		        .style("fill", "steelblue")
		        .attr("x1", function(d) {
		          return x(d.Name);
		        })
		        .attr("y1", function(d) {
		          return y(d.Maximum);
		        })
              .attr("x2", function(d) {
		          return x(d.Name);
		        })
		        .attr("y2", function(d) {
		          return y(d.Minimum);
		        });
		        
		     
   eSel.append("circle")
        .attr("r", 6.5)
        .attr("cx", function(d) { return x(d.Name); })
        .attr("cy", function(d) { return y(d.Average); })
        .attr("fill", "green");
   eSel.append("circle")
        .attr("r", 3.5)
        .attr("cx", function(d) { return x(d.Name); })
        .attr("cy", function(d) { return y(d.Maximum); })
        .attr("fill", "red");
   eSel.append("circle")
        .attr("r", 3.5)
        .attr("cx", function(d) { return x(d.Name); })
        .attr("cy", function(d) { return y(d.Minimum); })
        .attr("fill", "blue");
		    
		    function type(d) {
		      d.Maximum = +d.Maximum;
		      return d;
		    }	
			},
	});