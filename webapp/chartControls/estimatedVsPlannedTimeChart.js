var thisId;
	/* Bullet chart: A UI5 custom control wrapping some D3.js code */
	sap.ui.core.Control.extend("control.estvsplanline", { 
		/* the control API */
		metadata : {
			 properties : {
		            "items" : { type: "any" },
		            "colorType"	:{type:"string"},
	                "height" : {type: "int"},
	                "width" : {type: "int"},
	                "popup" : {type: "any"}
		        },
			events: {
				"select" : {},
				"selectEnd": {},
				"click": {}
			}			
		},
	
		//
		/*onclick: function(evt) {

		    // alert("Control " + this.getId() + " was clicked.");

		  },*/
		// the part creating the HTML:
		renderer : function(oRm, oControl) {	// static function, so use the given "oControl" instance instead of "this" in the renderer function
			oRm.write("<div"); 
			oRm.writeControlData(oControl);		// writes the Control ID and enables event handling - important!
			oRm.addClass("lineChartVs");				// add a CSS class for styles common to all control instances
			oRm.writeClasses();					// this call writes the above class plus enables support for my.Bubble.addStyleClass(...)				
			oRm.write(">");
			oRm.write("</div>");
		},
		onAfterRendering: function() {
			
			var that = this;
			thisId = this.getId();
			var div = d3.select("#toolTip");
			var deepClonedCopy = jQuery.extend(true, {}, this.getItems());
			var data;
			data = $.map(deepClonedCopy, function(el) { return el });
			if(data){
			//var containerWidth = jQuery.sap.byId(this.oParent.sId).width() || 800; // gets super parent width
			var containerWidth = $("#"+this.getId()).parent().width(); // gets immediate parent width
			var containerheight = $("#"+this.getId()).parent().height();
			if(containerWidth == 0){
				containerWidth = 800;
				containerheight = 200;
			}
			//var containerheight = jQuery.sap.byId(this.oParent.sId).height(); 
			var margin = {top: 15, right: 30, bottom: 20, left: 60},
			width = containerWidth- margin.left - margin.right,
			height = containerheight - margin.top - margin.bottom;
		    var maindata = data[0].maindata;
			var pointdata = data[0].pointdata;
			var tempforecastdata=data[0].forecastdata;
			
			var cities = []
			var parseDate = d3.time.format("%d-%b-%y %H:%M %p").parse;
			maindata.forEach(function(d) {
			  cities.push(d3.keys(d).filter(function(key) {
			      return key !== "date" && key !== "maxLength";
			    }));
			  });
			cities = d3.set(d3.merge(cities)).values();
			var myData = [];
			var allValues = [];
			var allDates =[];
			cities.forEach(function(city){
				var cityData = {};
				cityData.name = city;
				cityData.values = [];
				maindata.forEach(function(md){
					if (md[city]){
					  allValues.push(md[city])
						allDates.push(parseDate(md.date))
						cityData.values.push({date: parseDate(md.date), value: md[city]})
					}
				})
				myData.push(cityData)
			});
//				var passedheight = this.getHeight();
			//var containerWidth = jQuery.sap.byId(this.oParent.sId).width() || 800; // gets super parent width
		
			if (data.length >= 1) {
			  var parseDate = d3.time.format("%d-%b-%y %H:%M %p").parse;
			  
			  var values=[];
				for(var i=0;i<(tempforecastdata[0].values).length;i++){
				  values.push({"date":parseDate((tempforecastdata[0].values)[i].date),"value":"Estimated"});
				  values.sort();
				}
			  var forecastdata=[];
			  forecastdata.push({"name":"Estimated","values":values});
			  var tempPlMaxDate = d3.time.hour.offset(parseDate(d3.max(data[0].originaldata, function(d) {
			    return d.PLAN_END_TIME;
			  })), +1);
			  
			  var tempPlMinDate = d3.time.hour.offset(parseDate(d3.min(data[0].originaldata, function(d) {
			    return d.PLAN_START_TIME;
			  })), -1);
			  
			  var tempEstMaxDate = d3.time.hour.offset(parseDate(d3.max(data[0].originaldata, function(d) {
				    return d.ESTIMATED_END_TIME;
				  })), +1);
			  
			  var tempEstMinDate = d3.time.hour.offset(parseDate(d3.min(data[0].originaldata, function(d) {
				    return d.REALIZED_START_TIME;
				 })), -1);
			  
			  var maxdiff =tempPlMaxDate.getTime()-tempEstMaxDate.getTime(); 
			  var mindiff =tempPlMinDate.getTime()-tempEstMinDate.getTime();
			  
			  var minDate;
			  var maxDate;
			  
			  if(maxdiff>=0){
				  maxDate = tempPlMaxDate;
			  }
			  else{
				  maxDate = tempEstMaxDate;
			  }
			  
			  if(mindiff<=0){
				  minDate = tempPlMinDate;
			  }
			  else{
				  minDate = tempEstMinDate;
			  }

			  var x = d3.time.scale()
							  .domain([minDate, maxDate])
							    //.domain(d3.extent(allDates))
							    .range([0, width]);

			 var y=d3.scale.ordinal()
			    .domain(allValues)
			    .rangePoints([height, 0],1);

			  var color = d3.scale.category10();

			  var xAxis = d3.svg.axis()
			    .scale(x)
			    .orient("bottom").ticks(5);

			  var yAxis = d3.svg.axis()
			    .scale(y)
			    .orient("left");

			  var line = d3.svg.line()
			    .x(function(d) {
			      return x(d.date);
			    })
			    .y(function(d) {
			      return y(d.value);
			    });

			  var svg = d3.select("#"+this.getId()).append("svg")
			    .attr("width", width + margin.left + margin.right)
			    .attr("height", height + margin.top + margin.bottom)
			    .append("g")
			    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

			  svg.append("g")
			    .attr("class", "x axis")
			    .attr("transform", "translate(0," + height + ")")
			    .call(xAxis);

			  svg.append("g")
			    .attr("class", "y axis")
			    .call(yAxis)
			    .selectAll("text")
			    .attr("y", -9)
                .attr("x", 5)
			    .attr("dy", ".35em")
			    .attr("transform", "rotate(-90)");//.selectAll("text").remove();

			  var wsf = svg.selectAll(".wsf")
			    .data(myData)
			    .enter().append("g")
			    .attr("class", "wsf");


			  wsf.append("path")
			    .attr("class", "line")
			    .attr("d", function(d) {
			     
			      return line(d.values);
			    })
			    .style("stroke", function(d) {
			      if(d.name === "Estimated"){
			    	  return (data[0].colorcode)[0].toString();
			      }
			      else{
				      return color(d.name);
			      }
			    });
			var i=0;
			wsf.selectAll(".wsf")
			      .data(forecastdata)
			    .enter().append("path").attr("d", function(d) {
			        return line(d.values);
			    })
			    .style("stroke", function(d) {
				    	  return (data[0].colorcode)[0].toString();
			    }).style("stroke-dasharray", ("3, 3"));
			
			wsf.selectAll(".point")
			      .data(pointdata)
			    .enter().append("path")
			      .attr("class", "point")
			      .attr("d", d3.svg.symbol().type("triangle-up"))
			      .attr("transform", function(d) { return "translate(" + x(parseDate(d.date)) + "," + y(d.Estimated) + ") rotate(90)"; })
			      .style("fill", "red")
			      .on("mouseover", function(d) {
				      formatDate = d3.time.format("%d-%B-%Y %H:%M %p");
				      div.transition().duration(100).style("opacity", .9);
				      div.html(d.date)
				        .style("left", (d3.event.pageX) + "px").style("top", (d3.event.pageY - 28) + "px");
				      d3.select(this).attr("d", d3.svg.symbol().type("triangle-up").size(128));
				    }).on("mouseout", function(d) {
				      div.transition().duration(600).style("opacity", 0)
				      d3.select(this).attr("d", d3.svg.symbol().type("triangle-up"));
				    });
			
			
			
			  wsf.selectAll(".dot")
			    .data(function(d) {
			      dotpoints=d.values;
			      dotpoints.sort();
			      if(i==1){
			      //dotpoints.shift();
			       dotpoints.splice(1, 1);
			      dotpoints.push({"date":(forecastdata[0].values)[1].date,"value":"Estimated"});
			      }
			       i++;
			      return dotpoints;
			     
			      
			    })
			    .enter().append("circle")
			    .attr("class", "dot")
			    .attr("r", 3)
			    .attr("cx", function(d) {
			    //parseDate("21-APR-16 05:00 AM")
			      return x(d.date);
			    }
			    )
			    .attr("cy", function(d) {
			   // alert(parseDate("21-APR-16 05:00 AM"));
			    //alert(d.value);
			      return y(d.value);
			    })
			    .attr("stroke", function(d) {
			      if(this.parentNode.__data__.name === "Estimated"){
			    	  return (data[0].colorcode)[0].toString();
			      }
			      else{
			    	  return color(this.parentNode.__data__.name);
			      }
			    })
			    .attr("fill", function(d) {
			        if(this.parentNode.__data__.name === "Estimated"){
			    	  return (data[0].colorcode)[0].toString();
			      }
			      else{
			    	  return color(this.parentNode.__data__.name);
			      }
			    })
			    //.attr("fill-opacity", .5)
			    //.attr("stroke-width", 2)
			    .on("mouseover", function(d) {
			      formatDate = d3.time.format("%d-%B-%Y %H:%M %p");
			      div.transition().duration(100).style("opacity", .9);
			      div.html(formatDate(d.date))
			        .style("left", (d3.event.pageX) + "px").style("top", (d3.event.pageY - 28) + "px").attr('r', 8);
			      d3.select(this).attr('r', 8);
			    }).on("mouseout", function(d) {
			      div.transition().duration(600).style("opacity", 0)
			      d3.select(this).attr('r', 3);
			    });
              
			  
			  
			  var legendData = ["Current Job Status"];
			  var legend = svg.selectAll(".legend")
			    .data(legendData)
			    .enter().append("g")
			    .attr("class", "legend")
			    .attr("transform", function(d, i) {
                    return "translate(" + (width - 150) + "," + (i*40) + ")";
                });

			 /* legend.append("rect")
			    .attr("x", width - 18)
			    .attr("width", 18)
			    .attr("height", 4)
			    .style("fill", function(d) {
			      return color(d);
			    });*/
			  legend.append("path")
		      .attr("d", d3.svg.symbol().type("triangle-up").size(64)).attr("transform", function(d, i) {
                  return "rotate(90)";
              }).style("fill", "red");
		      //.attr("transform", function(d) { return "translate(" + x(d) + "," + y(d) + ") rotate(90)";}).style("fill", "red");
			  legend.append("text")
			   .attr("dx",10)
			   .attr("dy",".4em")
			  .text(function(d) {
			    return d;
			  })
			}
	
			
			 }
			
		  function click(d) 
		  {
			  that.fireSelect({keys:d});  
			  
		  }
		}	
	});  