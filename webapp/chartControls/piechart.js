 	/* Pie Chart: A UI5 custom control wrapping some D3.js code */
	sap.ui.core.Control.extend("control.piechart", { 
		/* the control API */
		metadata : {
			 properties : {
		            "items" : { type: "any" },
	                "height" : {type: "int"},
	                "width" : {type: "int"},
	                "popup" : {type: "any"},
	                "total" : {type: "int"}
		        },
			events: {
				"select" : {},
				"selectEnd": {},
				"click": {}
			}			
		},
	
		
		// the part creating the HTML:
		renderer : function(oRm, oControl) {	// static function, so use the given "oControl" instance instead of "this" in the renderer function
			oRm.write("<div"); 
			oRm.writeControlData(oControl);		// writes the Control ID and enables event handling - important!
			oRm.addClass("piechart");				// add a CSS class for styles common to all control instances
			oRm.writeClasses();					// this call writes the above class plus enables support for my.Bubble.addStyleClass(...)				
			oRm.write(">");
			oRm.write("</div>");
		},
		
		onAfterRendering: function() {
			
			
			var that = this;
			var deepClonedCopy = jQuery.extend(true, {}, this.getItems());
			var data;
			totalCount = 0;
			data = $.map(deepClonedCopy, function(el) { totalCount = totalCount+el.count; return el });
			
			/*$.each(data,function(i,d){
				totalCount = totalCount+d.count; 
				
			});*/
			this.setTotal(totalCount);
			//alert(this.getTotal(totalCount));
			var width = $("#"+this.getId()).parent().width(); // gets immediate parent width
			var height = $("#"+this.getId()).parent().height();
		/*	var width = 210,
		    height = 210;*/
			var margin = {top: 15, right: 15, bottom: 20, left: 40},
			/*width = containerWidth- margin.left - margin.right,
			 height = containerheight - margin.top - margin.bottom;*/
			
			//alert(height +"===="+width);
			/*var width = 960,
		    height = 500,*/
		    radius = Math.min(width, height) / 2 - 10;

		//var color = d3.scale.category20();
			var color = d3.scale.ordinal()
		     .range(["#1F78B4","#F8766D",  "#A3A500", "#00BF7D","#FFCF79", "#9B9AFF", "#22C8B2","#725885","#8a89a6",  "#D1A671", "#3399FE", "#938D6B", "#F4B2A6", "#8B6B6E","#CFC38F",]);

		var arc = d3.svg.arc()
		    .outerRadius(radius);
		  
		  var arcOver = d3.svg.arc()
		        .outerRadius(radius + 10);

		var pie = d3.layout.pie()
		    .sort(null)
		    .value(function(d) { return d.count; });

		  var labelArc = d3.svg.arc()
		    .outerRadius(radius - 40)
		    .innerRadius(radius - 40);
		  
		var svg = d3.select("#"+this.getId()).append("svg")
		    .datum(data)
		    .attr("width", width)
		    .attr("height", height)
		  .append("g")
		    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

		var arcs = svg.selectAll(".arc")
		    .data(pie)
		  .enter().append("g")
		    .attr("class", "arc");
		  
		  var arcs2 = svg.selectAll(".arc2")
		    .data(pie)
		  .enter().append("g")
		    .attr("class", "arc2");

		  
		arcs.append("path")
		.on("click", click)
		    .attr("fill", function(d, i) { return color(i); })
		    .on("mouseenter", function(d) {
		  
		  svg.selectAll("path").sort(function (a, b) { 
		      if (a != d) return -1;               
		      else return 1;                             
		  });
		  
		   /*var endAngle = d.endAngle + 0.1;
		   var startAngle = d.startAngle - 0.1;*/
		   var endAngle = d.endAngle;
		   var startAngle = d.startAngle;
		   var arcOver = d3.svg.arc()
		        .outerRadius(radius + 10).endAngle(endAngle).startAngle(startAngle);
		            d3.select(this)
		               .attr("stroke","white")
		               .transition()
		               .ease("bounce")
		               .duration(1000)
		               .attr("d", arcOver)             
		               .attr("stroke-width",6);
		        })
		        .on("mouseleave", function(d) {
		            d3.select(this).transition()            
		               .attr("d", arc)
		               .attr("stroke","none");
		        })
		  .transition()
		    .ease("bounce")
		    .duration(2000)
		    .attrTween("d", tweenPie);

		function tweenPie(b) {
		  b.innerRadius = 0;
		  var i = d3.interpolate({startAngle: 0, endAngle: 0}, b);
		  return function(t) { return arc(i(t)); };
		}
        var k=0;
		arcs2.append("text")
		.transition()
		    .ease("elastic")
			.duration(2000)
			.delay(function (d, i) {
				return i * 250;
			})
		      .attr("x","6")
		      .attr("dy", ".35em")
		      .text(function(d) { if(d.data.count >0){ k = k+1; return d.data.count;} })
		      .attr("transform", function(d) { if (k >1){return "translate(" + labelArc.centroid(d) + ") rotate(" + angle(d) + ")";} else{return "rotate(-360)";} })
		      .attr("font-size", "10px");
		
		//arcs.on("click", click);
	    

		function type(d) {
		  d.count = +d.count;
		  return d;
		}
		function angle(d) {
		      var a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
		      return a > 90 ? a - 180 : a;
		}
			
		function click(d) 
		  {
			  that.fireSelect({keys:d}); 
		  }
		
		},
			
	   
	});