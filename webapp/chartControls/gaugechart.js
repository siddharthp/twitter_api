var thisId;
	/* Gauge chart: A UI5 custom control wrapping some D3.js code */
	sap.ui.core.Control.extend("control.gaugechart", { 
		/* the control API */
		metadata : {
			 properties : {
		            "items" : { type: "any" },
		            "colorType"	:{type:"string"},
		            "tooltipTitle"	:{type:"string"},
	                "height" : {type: "int"},
	                "width" : {type: "int"},
	                "popup" : {type: "any"}
		        },
			events: {
				"select" : {},
				"selectEnd": {},
				"click": {}
			}			
		},
	
		//
		/*onclick: function(evt) {

		    // alert("Control " + this.getId() + " was clicked.");

		  },*/
		// the part creating the HTML:
		renderer : function(oRm, oControl) {	// static function, so use the given "oControl" instance instead of "this" in the renderer function
			oRm.write("<div"); 
			oRm.writeControlData(oControl);		// writes the Control ID and enables event handling - important!
			oRm.addClass("gaugechart");				// add a CSS class for styles common to all control instances
			oRm.writeClasses();					// this call writes the above class plus enables support for my.Bubble.addStyleClass(...)				
			oRm.write(">");
			oRm.write("</div>");
		},
		onAfterRendering: function() {
			
			var that = this;
			
			var div = d3.select("#toolTip");
			var deepClonedCopy = jQuery.extend(true, {}, this.getItems());
			var data;
			data = $.map(deepClonedCopy, function(el) { return el });
			//alert(JSON.stringify(data));
			
			//[{"TARGET":0,"NO":null,"MODULE":"AS","LAST_X_HRS":3,"RANGES":"40,70,100","MARKERS":80,"MEASURES":null,"TYPE":"INBOUND SLA"}]
			if(data){
			if(data[0]){
			data[0].ranges = JSON.parse("["+data[0].RANGES+"]");
			data[0].measures = JSON.parse("["+data[0].MEASURES+"]");
			data[0].markers = JSON.parse("["+data[0].MARKERS+"]");
			}
			//var containerWidth = jQuery.sap.byId(this.oParent.sId).width() || 800; // gets super parent width
			var containerWidth = $("#"+this.getId()).parent().width(); // gets immediate parent width
			if(containerWidth ==0){
				containerWidth=380;
			}
			thisId = this.getId();
			var margin = {top: 10, right: 30, bottom: 25, left: 30},
			width = containerWidth- margin.left - margin.right,
			height = 65 - margin.top - margin.bottom;
		
		    var chart = d3.bullet()
		    .width(width)
		    .height(height);
		 
		    var classType;
		    if(this.getColorType()){
		    	classType = "reversebullet";
		    }
		    else{
		    	classType = "bullet";
		    }
		    var svg = d3.select("#"+this.getId()).selectAll("svg")
		      .data(data)
		      .enter().append("svg")
		      .attr("class", classType)
		      .attr("width", width + margin.left + margin.right)
		      .attr("height", height + margin.top + margin.bottom)
		      .on("mouseover", function(d) {
		    	  var matrix = this.getScreenCTM()
		          .translate(+ this.getAttribute("cx"), + this.getAttribute("cy"));
		    	  richText = "<strong><u>"+that.getTooltipTitle()+" :</u> </strong><br/>"
	          	  +"<li><strong>No. :</strong> "+d.NO+"["+d.MEASURES+"%]</li>"
	          	  +"<li><strong>Target : ></strong> "+d.TARGET+"["+d.MARKERS+"%]</li>";
		    	  if(d.clickable){
		    		  richText += "<u>"+d.clickable+"</u>";
		    	  }
		             div.transition()		
	                   .style("opacity", .9);		
	                 div.html(richText)
	                 .style("left", (window.pageXOffset + matrix.e + 20) + "px")
	                 .style("top", (window.pageYOffset + matrix.f - 25) + "px");
				       /*.style("left",  (d3.event.pageX) + "px")
	                   .style("top",  (d3.event.pageY) + "px");	*/
	           }).on("mouseout", function() {
	         	  div.transition()        
	                .duration(500)      
	                .style("opacity", 0); 
	            })
		      .on("click", click)
		      .append("g")
		      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
		      .call(chart/*.duration(400)*/);
		    
			 }
			  // chart.duration(1200);	
		  function click(d) 
		  {
			  that.fireSelect({keys:d}); 
		  }
		}	
	});  