 // test 
	/* Line chart: A UI5 custom control wrapping some D3.js code */
	sap.ui.core.Control.extend("control.myLegend", { 
		/* the control API */
		metadata : {
			 properties : {
		            "items" : { type: "any" },
		            
		        },
			events: {
				"select" : {},
				"selectEnd": {}				
			}			
		},
	
		
		// the part creating the HTML:
		renderer : function(oRm, oControl) {	// static function, so use the given "oControl" instance instead of "this" in the renderer function
			oRm.write("<div"); 
			oRm.writeControlData(oControl);		// writes the Control ID and enables event handling - important!
			oRm.addClass("myLegend");				// add a CSS class for styles common to all control instances
			oRm.writeClasses();					// this call writes the above class plus enables support for my.Bubble.addStyleClass(...)				
			oRm.write(">");
			oRm.write("</div>");
		},
		
		onAfterRendering: function() {
			var that = this;
			/* get the Items aggregation of the control and put the data into an array */
			var aItems = this.getItems();
		
			/*var data = [];
			for (var i=0;i<aItems.length;i++){
				var oEntry = {};
				for (var j in aItems[i].mProperties) {
					oEntry[j]=aItems[i].mProperties[j];
				}					
				data.push(oEntry);
			}*/
			//var data = this.getItems();
			var deepClonedCopy = jQuery.extend(true, {}, this.getItems());
			var data;
			data = $.map(deepClonedCopy, function(el) { return el });
			
			//var containerWidth = jQuery.sap.byId(this.oParent.sId).height() || 800; // gets super parent width
			
			
			var containerWidth = $("#"+this.getId()).parent().width(); // gets immediate parent width
			var containerheight = $("#"+this.getId()).parent().height();
			var margin = {top: 15, right: 5, bottom: 10, left:5},
			width = containerWidth- margin.left - margin.right,
			 height = containerheight - margin.top - margin.bottom;
			//height = parseInt(d3.select('#sdtochart1').style('height'), 10)-margin.top - margin.bottom;


			 var color = d3.scale.ordinal()
			     .range(["#c1dad6",  "#B09EBD", "#e8d0a9", "#95BAC3", "#6b486b", "#98abc5", "#8a89a6"]);

			
			
			    
			 var svg = d3.select("#"+this.getId()).append("svg")
			     .attr("width", width + margin.left + margin.right)
			     .attr("height", height + margin.top + margin.bottom)
			   .append("g")
			     .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

			   var maxLength=0;
			   var maxIndex=0;
			   for(var i =0;i<data.length;i++){
				   
				   if(maxLength < data[i].maxLength){
					   maxLength = data[i].maxLength;
					   maxIndex = i;
				   }
			   }
			   var ageNames = d3.keys(data[maxIndex]).filter(function(key) { return key !== "Job" && key !== "Threshold" && key !== "maxLength"; });
			  
			   data.forEach(function(d) {
			     d.ages = ageNames.map(function(name) { return {name: name, value: +d[name]}; });
			   });

			
		
			   var legend = svg.selectAll(".legend")
			       .data(ageNames.slice())
			     .enter().append("g")
			       .attr("class", "legend")
			       .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

			   legend.append("rect")
			       .attr("x", 10)
			       .attr("width", 18)
			       .attr("height", 18)
			       .style("fill", color);
			   
			   legend.append("text")
			       .attr("x", 120)
			       .attr("y", 9)
			       .attr("dy", ".35em")
			       .style("text-anchor", "end")
			       .text(function(d) { return d; });
			},
	});