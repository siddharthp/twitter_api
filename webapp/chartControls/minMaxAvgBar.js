/*sap.ui.core.Element.extend("custom.minMaxAvgBarControl", { metadata : {
		properties : {
			"Name" : {type : "string", group : "Misc", defaultValue : null},
			"Maximum" : {type : "int", group : "Misc", defaultValue : null},
			"Minimum" : {type : "int", group : "Misc", defaultValue : null},
			"Average" : {type : "int", group : "Misc", defaultValue : null}
		
		}
	}});*/
	/* Line chart: A UI5 custom control wrapping some D3.js code */
	sap.ui.core.Control.extend("control.minMaxAvgBar", { 
		/* the control API */
		metadata : {
			 properties : {
		            "items" : { type: "any" },
	                "height" : {type: "int"},
	                "width" : {type: "int"},
	                "popup" : {type: "any"}
		        },
			 events: {
				"select" : {},
				"selectEnd": {}				
			}			
		},
	
		
		// the part creating the HTML:
		renderer : function(oRm, oControl) {	// static function, so use the given "oControl" instance instead of "this" in the renderer function
			oRm.write("<div"); 
			oRm.writeControlData(oControl);		// writes the Control ID and enables event handling - important!
			oRm.addClass("maxminavgchart");				// add a CSS class for styles common to all control instances
			oRm.writeClasses();					// this call writes the above class plus enables support for my.Bubble.addStyleClass(...)				
			oRm.write(">");
			oRm.write("</div>");
		},
		
		onAfterRendering: function() {
			var that = this;
			/* get the Items aggregation of the control and put the data into an array */
			var div = d3.select("#toolTip");
			var deepClonedCopy = jQuery.extend(true, {}, this.getItems());
			var data;
			data = $.map(deepClonedCopy, function(el) { return el });
			//alert(JSON.stringify(data));
			//var containerWidth = jQuery.sap.byId(this.oParent.sId).width() || 800; // gets super parent width
			var containerWidth = $("#"+this.getId()).parent().width(); // gets immediate parent width
			var containerheight = $("#"+this.getId()).parent().height();
			var margin = {top: 15, right: 15, bottom: 20, left: 40},
			width = containerWidth- margin.left - margin.right,
			 height = containerheight - margin.top - margin.bottom;

			var formatPercent = d3.format(".0%");

		    var x = d3.scale.ordinal()
		      .rangeRoundBands([0, width], .9);

		    var y = d3.scale.linear()
		      .range([height, 0]);

		    var xAxis = d3.svg.axis()
		      .scale(x)
		      .orient("bottom")
		      .tickSize(-1);

		    var yAxis = d3.svg.axis()
		      .scale(y)
		      .orient("left") 
		      .ticks(5, "d")
		      .tickSize(-width,0)
		      //.ticks(d3.time.minutes, 15)
		      //.tickFormat(d3.time.minute, 15);

		
		     var chart1 = d3.select("#"+this.getId()).append("svg")
		      .attr("class","maxminavgchart")
		      .attr("width", width + margin.left + margin.right)
		      .attr("height", height + margin.top + margin.bottom)
		      .append("g")
		      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		      x.domain(data.map(function(d) {
		        return d.Name;
		      }));
		      //x2.domain(data.map(function(d){return d.Average}));
		      y.domain([0, d3.max(data, function(d) {
		        return d.Maximum+10;
		      })]);

		      chart1.append("g")
		        .attr("class", "x axis")
		        .attr("transform", "translate(0," + height + ")")
		        .call(xAxis);
		      padding = 0;
		      chart1.append("g")
		        .attr("class", "y axis")
		        .call(yAxis)
		        .append("text")
		        .attr("transform", "translate("+ (padding/2) +","+(height/2)+")rotate(-90)")
		      //.attr("transform", "rotate(-90)")
		      .attr("y", -34)
		      .attr("dy", ".71em")
		      .style("text-anchor", "middle")
		      .text(oBundle1.getText("label.dashb.minmxaavgg.yaxislabel"));
		      var eSel = chart1.selectAll(".bar")
		        .data(data)
		        .enter();
		        
		      eSel.append("rect")
		        .attr("class", "bar")
		        .style("fill", "steelblue")
		        .attr("x", function(d) {
		          return x(d.Name);
		        })
		        .attr("width", x.rangeBand())
		        .attr("y", function(d) {
		          return y(d.Maximum);
		        })
		        .attr("height", function(d) {
		          return height - y(d.Maximum - d.Minimum);
		        }).on("mouseover", function(d) {		
		             div.transition()		
	                   .style("opacity", .9);		
	                 div.html(
	                   "Max : " +  d.Maximum+"<br/>"+
	                   "Avg : " +  d.Average+"<br/>"+
	                   "Min  : " +  d.Minimum)
				       .style("left",  (d3.event.pageX) + "px")
	                   .style("top",  (d3.event.pageY) + "px");	
	           }).on("mouseout", function() {
	         	  div.transition()        
	                .duration(500)      
	                .style("opacity", 0); 
	            });
		        
		      eSel.append("path")
		        .style("stroke", "black")
		        .style("stroke-width", 3)
		        .attr("d", function(d){
		          var rv = "M" + x(d.Name) + "," + y(d.Average);
		          rv += "L" + (x(d.Name) + x.rangeBand()) + "," + y(d.Average);
		          return rv;
		        });
		 /* var legendata = [{"Avg": "Avg."}];
		  var legend = chart1.selectAll(".legend")
		      .data(legendata.slice())
		      .enter().append("g")
		      .attr("class", "legend")
		      .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

		  legend.append("rect")
		      .attr("x", width - 18)
		      .attr("width", 10)
		      .attr("height", 3)
		      .style("fill", "black");
		  
		  legend.append("text")
		      .attr("x", width - 24)
		      .attr("y", 5)
		      .attr("dy", ".35em")
		      .style("text-anchor", "end")
		      .text("Avg.");*/
		    
		    function type(d) {
		      d.Maximum = +d.Maximum;
		      return d;
		    }	
			},
	});