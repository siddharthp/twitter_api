	/* Line chart: A UI5 custom control wrapping some D3.js code */
	sap.ui.core.Control.extend("control.myDropDown", { 
		/* the control API */
		metadata : {
			 properties : {
		            "items" : { type: "any" },
	               
		        },
			events: {
				"select" : {},
				"selectEnd": {}				
			}			
		},
	
		  
		// the part creating the HTML:
		renderer : function(oRm, oControl) {	// static function, so use the given "oControl" instance instead of "this" in the renderer function
			oRm.write("<div"); 
			oRm.writeControlData(oControl);		// writes the Control ID and enables event handling - important!
			oRm.addClass("myDropDown");				// add a CSS class for styles common to all control instances
			oRm.writeClasses();					// this call writes the above class plus enables support for my.Bubble.addStyleClass(...)				
			oRm.write(">");
			oRm.write("</div>");
		},
		
		onAfterRendering: function() {
			var that = this;
			 $("#"+this.getId()).append('<select class=\"multiDropBox\" id=\"myid\" multiple=\"multiple\"></select>');
			 
				var deepClonedCopy = jQuery.extend(true, {}, this.getItems());
				var optgroups;
				optgroups = $.map(deepClonedCopy, function(el) { return el });
			// var optgroups = this.getItems();
			 temp = [];
			 var partialyTemp	=	[];
			 var previouslySelectedOptions = oStorage.get("workstation");
			 optgroups.forEach(function (a) {
			     if (!this[a.AREA]) {
			         this[a.AREA] = { label: a.AREA, children: [] };
			         temp.push(this[a.AREA]);
			     }
			     var selectedFlag = false;
			     if(previouslySelectedOptions != null){
			    	 for(var i = 0; i < previouslySelectedOptions.length ;i++){
			    		 if(a.EQ_ID === previouslySelectedOptions[i]){
			    			 selectedFlag	=	true;
			    		 }
			    	 }
			    	
			     }
			     if(selectedFlag){
			    	 this[a.AREA].children.push({ label: a.EQ_ID, value: a.EQ_ID ,selected: true});
			     }
			     else{
			    	 this[a.AREA].children.push({ label: a.EQ_ID, value: a.EQ_ID});
			     }
			    
			 }, {});
			 partialyTemp	=	temp;
				console.log("data binding multi drop down",temp);
	         $('#myid').multiselect({
	             enableClickableOptGroups: true,
	             enableCollapsibleOptGroups: true,
	             //includeSelectAllOption: true,
	             maxHeight: 300,
	             buttonWidth: 'auto',
	             dropRight: true,
	             buttonClass: 'bdbbdropButton',
	             optionClass: function(element) {
	            	 return 'wksli';
	             },
	             nonSelectedText: 'All WKS',
	             onChange: function(option, checked, selected,element) {
	            	  var temp = jQuery.extend(true, {}, newData);

                      var selectionData = [];
                      var selectionGroup = [];
                      var partialySelectedGroup	=[];
                      $('#myid option:selected').each(function(e) {
                          for (n in newData) {
                              for (d in newData[n]) {
                                  if (newData[n][d].value == $(this).val()) {
                                      for (i in temp[n]) {
                                          if (temp[n][i].value == $(this).val())
                                              temp[n].splice(i, 1);
                                      }

                                  }
                              }

                          }
                          selectionData.push($(this).val());
                      });

                      for (t in temp) {
                          if (temp[t].length == 0) {
                              selectionGroup.push(t);
                          } else {
                              for (tt in newData[t]) {
                                  if (newData[t][tt] == temp[t][tt]) {
                                      selectionData.push(newData[t][tt]["value"]);
                                  }
                              }
                          }

                      }
                     
              
                      
                      for(var j=0;j<selectionData.length;j++){
	                	   for(var i=0;i<partialyTemp.length;i++){
	                		   
	                		   for(var k=0;k<partialyTemp[i].children.length;k++){
		                		   if(selectionData[j].toString().trim() === (partialyTemp[i].children[k].value).toString().trim()){
		                			  
		                			   partialySelectedGroup.push(partialyTemp[i].label);
		                    	   }
	                		   }
	                	   }
	                   }  
	                   
	                   var uniqueNames=[];
		       	    	for(var i = 0 ; i<partialySelectedGroup.length;i++){
		       	    		if(uniqueNames.indexOf(partialySelectedGroup[i]) === -1){
		       	    			 uniqueNames.push(partialySelectedGroup[i]);  
		       	    	    }     
		       	    	}
	       			uniqueNames.sort();
	       			var mixedCondition	=	false;
	       			if(uniqueNames.length != selectionGroup.length && selectionGroup.length != 0){
	       				mixedCondition	=	true;
	       			}
	       			  console.log("partialySelectedGroups"+uniqueNames);
                      console.log("Group : " + selectionGroup);
                      console.log("Data : " + selectionData);
                      oStorage.put("wksfinger",selectionGroup);
                      oStorage.put("workstation",selectionData);
                      oStorage.put("mixedCondition",mixedCondition);
                      sap.ui.controller("oui5mvc.odbbshift").changeEventFordropDownTemplate(mixedCondition,selectionGroup,selectionData);
	             }
	         });
	  //  alert(JSON.stringify(temp));
	  	var newData = {};
	  console.log(JSON.stringify(temp));
        $('#myid').multiselect('dataprovider', temp);
        var clonedData = jQuery.extend(true, {}, temp);
        for (i in clonedData) {
            newData[clonedData[i]["label"]] = clonedData[i]["children"];
        }
			},
	});