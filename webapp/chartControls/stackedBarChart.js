 
	/* Line chart: A UI5 custom control wrapping some D3.js code */
	sap.ui.core.Control.extend("control.stackedbarchart", { 
		/* the control API */
		metadata : {
			 properties : {
		            "items" : { type: "any" },
		            
		        },
			events: {
				"select" : {},
				"selectEnd": {},
				"click": {}			
			}			
		},
	
		
		// the part creating the HTML:
		renderer : function(oRm, oControl) {	// static function, so use the given "oControl" instance instead of "this" in the renderer function
			oRm.write("<div"); 
			oRm.writeControlData(oControl);		// writes the Control ID and enables event handling - important!
			oRm.addClass("stackedbarchart");				// add a CSS class for styles common to all control instances
			oRm.writeClasses();					// this call writes the above class plus enables support for my.Bubble.addStyleClass(...)				
			oRm.write(">");
			oRm.write("</div>");
		},
		
		onAfterRendering: function() {
			var that = this;
			/* get the Items aggregation of the control and put the data into an array */
			var aItems = this.getItems();
			var div = d3.select("#toolTip");
			var deepClonedCopy = jQuery.extend(true, {}, this.getItems());
			var data;
			data = $.map(deepClonedCopy, function(el) { return el });
			//var containerWidth = jQuery.sap.byId(this.oParent.sId).height() || 800; // gets super parent width
			
			
			var containerWidth = $("#"+this.getId()).parent().width(); // gets immediate parent width
			var containerheight = $("#"+this.getId()).parent().height();
			
			  if(containerWidth > 400){
					var margin = {top: 15, right: 30, bottom: 65, left:40};
			  }
			  else{
				  var margin = {top: 15, right: 30, bottom: 80, left:40};
			  }
		
			var width = containerWidth- margin.left - margin.right,
			 height = containerheight - margin.top - margin.bottom;
			//height = parseInt(d3.select('#sdtochart1').style('height'), 10)-margin.top - margin.bottom;
			 if(data.length < 4){
			var x = d3.scale.ordinal()
		    .rangeRoundBands([0, width], .8);
			 }
			 else{
				 var x = d3.scale.ordinal()
				    .rangeRoundBands([0, width], .1);
			 }

		var y = d3.scale.linear()
		    .rangeRound([height, 0]);

		var color = d3.scale.ordinal()
	     .range(["#1F78B4","#F8766D",  "#A3A500", "#00BF7D","#FFCF79", "#9B9AFF", "#22C8B2","#725885","#8a89a6",  "#D1A671", "#3399FE", "#938D6B", "#F4B2A6", "#8B6B6E","#CFC38F",]);

		var xAxis = d3.svg.axis()
		    .scale(x)
		    .orient("bottom");

		var yAxis = d3.svg.axis()
		    .scale(y)
		    .orient("left")
		    .tickFormat(d3.format(".2s"));

		var svg = d3.select("#"+this.getId()).append("svg")
		    .attr("width", width + margin.left + margin.right)
		    .attr("height", height + margin.top + margin.bottom)
		  .append("g")
		    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		/*!key.match(/area/);*/    /*key !== "area" && key !== "area1" && key !== "area2" && key !== "area3";*/
		var ageNames	=  d3.keys(data[0]).filter(function(key) { return  !key.match(/area/); });

		  data.forEach(function(d,i) {
		 //  alert(JSON.stringify(i));
		    var y0 = 0;
		    d.ages = d3.keys(data[i]).filter(function(key) { return  !key.match(/area/); }).map(function(name) {
		    	
		    	 return {name: name, y0: y0, y1: y0 += +d[name]}; 
		    });
		   // alert(JSON.stringify(d.ages));
		    d.total = d.ages[d.ages.length - 1].y1;
		  });

		
		//  data.sort(function(a, b) { return b.total - a.total; });
		  
		  x.domain(data.map(function(d) { return d.areas; }));
		  y.domain([0, d3.max(data, function(d) { return d.total; })]);

		  
	
			var xaxix = svg.append("g")
		      .attr("class", "x axis")
		      .attr("transform", "translate(0," + height + ")")
		      .call(xAxis)
		       .selectAll(".tick text")
		        .attr("transform", "rotate(45)")
			    .style("text-anchor", "start");
			
			     if(containerWidth > 400){
			    	 xaxix.call(wrap, x.rangeBand());
			     }
			    

		  svg.append("g")
		      .attr("class", "y axis")
		      .call(yAxis)
		    .append("text")
		      .attr("transform", "rotate(-90)")
		      .attr("y", 6)
		      .attr("dy", "-3.25em")
		      .style("text-anchor", "end")
		      .text("Count → ");

		  var state = svg.selectAll(".state")
		      .data(data)
		    .enter().append("g")
		      .attr("class", "g")
		      .attr("transform", function(d) { return "translate(" + x(d.areas) + ",0)"; })
		      .on("click", click)
		      .on("mouseover", function(d) {
		    	 
		    	   var htmlMsg="";
		           for(var i=0;i<d.ages.length;i++){
		        	   htmlMsg =htmlMsg.concat(d.ages[i].name +" : "+ d.ages[i].value +"% <br/>");
          		 }
		    	   div.transition()		
	                   .style("opacity", .9);		
	                 div.html("Click to see data in detail.")
				       .style("left",  (d3.event.pageX) + "px")
	                   .style("top",  (d3.event.pageY) + "px");	
	           })
	           .on("mouseout", function() {
	         	  div.transition()        
	                .duration(500)      
	                .style("opacity", 0); 
	            });

		  state.selectAll("rect")
		      .data(function(d) { return d.ages; })
		    .enter().append("rect")
		    .style("fill", function(d,i) { return color(i); })
		      .attr("width", x.rangeBand())
		      .attr("y", function(d) { return height; })
		      .attr("height", 0)
		      .transition()
		           //.ease("bounce")
					.duration(1000)
					.delay(function (d, i) {
						return i * 100;
					})
		      .attr("y", function(d) {
		    	//  alert(JSON.stringify(d));
		    	  return y(d.y1); })
		      .attr("height", function(d) { return y(d.y0) - y(d.y1); });

		 
		  function wrap(text, width) {
			  text.each(function() {
			    var text = d3.select(this),
			        words = text.text().split(/\s+/).reverse(),
			        word,
			        line = [],
			        lineNumber = 0,
			        lineHeight = 1.1, // ems
			        y = text.attr("y"),
			        dy = parseFloat(text.attr("dy")),
			        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
			    while (word = words.pop()) {
			      line.push(word);
			      tspan.text(line.join(" "));
			      if (tspan.node().getComputedTextLength() > width) {
			        line.pop();
			        tspan.text(line.join(" "));
			        line = [word];
			        tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
			      }
			    }
			  });
			}
		  function click(d) 
		  {
			  tempData = [];
	    	  tempData.push(d);
	    	  var tooltipData;
	    	  toolTipChart = new control.piechart({
				  layoutData: new sap.ui.layout.GridData({span: "L4 M4 S12"}),
				items: {
					path : "/", 
					},
			  });
	    	  tempData.forEach(function(k,i) {
	    	  		    var y0 = 0;
	    	  		    tooltipData = d3.keys(tempData[i]).filter(function(key) { return  !key.match(/ages/) && !key.match(/area/)  && !key.match(/total/); }).map(function(name) {
	    	  		    	
	    	  		    	 return {name: name, count : k[name]}; 
	    	  		    });
	    	  		  });
	    	  //alert(JSON.stringify(tooltipData));
			  that.fireSelect({keys:tooltipData,name: d.areas}); 
		  }
			},
	});