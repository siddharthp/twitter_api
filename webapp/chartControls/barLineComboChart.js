	/*Bar Line chart: A UI5 custom control wrapping some D3.js code */
	sap.ui.core.Control.extend("control.barNlinechart", { 
		/* the control API */
		metadata : {
			 properties : {
		            "items" : { type: "any" },
		            
		        },
			events: {
				"select" : {},
				"selectEnd": {}				
			}			
		},
	
		
		// the part creating the HTML:
		renderer : function(oRm, oControl) {	// static function, so use the given "oControl" instance instead of "this" in the renderer function
			oRm.write("<div"); 
			oRm.writeControlData(oControl);		// writes the Control ID and enables event handling - important!
			oRm.addClass("groupedBarChart");				// add a CSS class for styles common to all control instances
			oRm.writeClasses();					// this call writes the above class plus enables support for my.Bubble.addStyleClass(...)				
			oRm.write(">");
			oRm.write("</div>");
		},
		
		onAfterRendering: function() {
			var that = this;
			/* get the Items aggregation of the control and put the data into an array */
			var aItems = this.getItems();
			var div = d3.select("#toolTip");
			var deepClonedCopy = jQuery.extend(true, {}, this.getItems());
			var data;
			data = $.map(deepClonedCopy, function(el) { return el });
			//var containerWidth = jQuery.sap.byId(this.oParent.sId).height() || 800; // gets super parent width\
			var containerWidth = $("#"+this.getId()).parent().width(); // gets immediate parent width
			var containerheight = $("#"+this.getId()).parent().height();
			var margin = {top: 15, right: 30, bottom: 30, left:40},
			//width = containerWidth- margin.left - margin.right,
			width = parseInt(d3.select('.groupedBarChart').style('width'), 10)-margin.left - margin.right,
			 height = containerheight - margin.top - margin.bottom;
			//width = parseInt(d3.select('.groupedBarChart').style('width'), 10)-margin.top - margin.bottom;
		//	alert(newwidth);
			 var lineData = data;
			 
			 if(data.length < 4){
				 var x0 = d3.scale.ordinal()
			     .rangeRoundBands([0, width], .3);
			 }
			 else{
				 var x0 = d3.scale.ordinal()
			     .rangeRoundBands([0, width], .1);
			 }
			 

			 var x1 = d3.scale.ordinal();

			 var y = d3.scale.linear()
			     .range([height, 0]);
			 var color = d3.scale.ordinal()
			 
		//  .range(["#1F78B4","#F8766D",  "#A3A500", "#00BF7D", "#9B9AFF", "#9B3366","#98abc5", "#8a89a6","#663398","#FF6600",  "#663200", "#3399FE", "#129794", "#9B3366", "#98abc5", "#8a89a6","#8F824B",]);
			//  .range(["#73cdff",  "#938D6B", "#A1E8E4", "#8a89a6", "#F4B2A6", "#98abc5", "#22C8B2","#8F824B","#CFC38F","#D9BE79","#BCEFD0","#83986F","#CBC1DC","#725884","#D1A671","#7597A1","#536C45","#4A3137","#B3CBBE","#9577AD","#76889C","#466F75","#B2968B","#161616","#C66248","#8A503A"]);
			    // .range(["#1F78B4","#F8766D",  "#A3A500", "#00BF7D", "#9B9AFF", "#9B3366", "#98abc5", "#8a89a6","#8F824B",]); 
			 	
			  .range(["#1F78B4","#F8766D",  "#A3A500", "#00BF7D","#FFCF79", "#9B9AFF", "#22C8B2","#725885","#8a89a6",  "#D1A671", "#3399FE", "#938D6B", "#F4B2A6", "#8B6B6E","#CFC38F",]);
			//  .range(["#BCEFD0",  "#F4B2A6", "#A1E8E4", "#22C8B2", "#ECCCB3", "#98abc5", "#8a89a6","#663398","#FF6600",  "#663200", "#3399FE", "#129794", "#9B3366", "#98abc5", "#8a89a6","#8F824B",]);
			// .range(["#663398","#FF6600",  "#663200", "#3399FE", "#129794", "#9B3366", "#98abc5", "#8a89a6","#8F824B",]);
			// .range(["#89C766","#E74D29",  "#CC48B6", "#7D83FF", "#F33444", "#9B3366", "#98abc5", "#8a89a6","#8F824B",]);
			// .range(["#005FD7","#F6B525",  "#561D7C", "#F17104", "#D93493", "#9B3366", "#98abc5", "#8a89a6","#8F824B",]);
			 var xAxis = d3.svg.axis()
			     .scale(x0)
			     .orient("bottom");

			 var yAxis = d3.svg.axis()
			     .scale(y)
			     .orient("left")
			     .ticks(5)
			     .tickSize(-width, 0, 0)
			     .tickFormat(function(d) { return d; });

			 var svg = d3.select("#"+this.getId()).append("svg")
			     .attr("width", width + margin.left + margin.right)
			     .attr("height", height + margin.top + margin.bottom)
			   .append("g")
			     .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

			   var maxLength=0;
			   var maxIndex=0;
			   for(var i =0;i<data.length;i++){
				   
				   if(maxLength < data[i].maxLength){
					   maxLength = data[i].maxLength;
					   maxIndex = i;
				   }
			   }
			   //alert(JSON.stringify(data[maxIndex]));
			   var ageNames = d3.keys(data[maxIndex]).filter(function(key) { return key !== "name" && key !== "date" && key !== "Threshold" && key !== "maxLength" && key !== "flag"; });
			  
			   data.forEach(function(d) {
			     d.ages = ageNames.map(function(name) { return {name: name, value: +d[name]}; });
			   });

			   x0.domain(data.map(function(d) { return d.flag; }));
			   x1.domain(ageNames).rangeRoundBands([0, x0.rangeBand()]);
			   y.domain([0, d3.max(data, function(d) { return d3.max(d.ages, function(d) { return d.value; }); })]);

			   svg.append("g")
			       .attr("class", "x axis")
			       .attr("transform", "translate(0," + height + ")")
			       .call(xAxis);

			   svg.append("g")
			       .attr("class", "y axis")
			       .call(yAxis);

			   var date = svg.selectAll(".date")
			       .data(data)
			     .enter().append("g")
			       .attr("class", "date")
			       .attr("transform", function(d) { return "translate(" + x0(d.flag) + ",0)"; }).on("mouseover", function(d) {
			    	   var htmlMsg="";
			    
			           for(var i=0;i<d.ages.length;i++){
			        	 
			        	   htmlMsg =htmlMsg.concat(d.ages[i].name +" : "+ d.ages[i].value +"<br/>");
              		 }
			    	   div.transition()		
		                   .style("opacity", .9);		
		                 div.html(
		                		 "<strong><u>"+d.flag+"</u></strong> "+"<br/>"+ htmlMsg)
					       .style("left",  (d3.event.pageX) + "px")
		                   .style("top",  (d3.event.pageY) + "px");	
		           })
		           .on("mouseout", function() {
		         	  div.transition()        
		                .duration(500)      
		                .style("opacity", 0); 
		            });

			   date.selectAll("rect")
			       .data(function(d) { return d.ages; })
			       .enter().append("rect")
			       .attr("width", x1.rangeBand())
			       .attr("x", function(d) { return x1(d.name); })
			       .attr("y", function(d) { return height; })
		           .attr("height", 0)
		           .style("fill", function(d) { return color(d.name); })
		           .style("stroke","#F1F1F1")
		           .style("stroke-width", "0.5px")
		           .transition()
		           .ease("bounce")
					.duration(2000)
					.delay(function (d, i) {
						return i * 100;
					})
			       .attr("y", function(d) { if(d.value){ return y(d.value);} else {return 0;} })
			       .attr("height", function(d) { if(d.value){ return height - y(d.value);} else {return 0;}});
			   date.on("click", clickFunc);
			   function clickFunc(d){
				   that.fireSelect({keys:d});
			   }
			   
			   data.forEach(function(d) {
			   });
			},
	});