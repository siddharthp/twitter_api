/*sap.ui.core.Element.extend("custom.lineChartControl", { metadata : {
		properties : {
			"date" : {type : "string", group : "Misc", defaultValue : null},
			"tonne" : {type : "int", group : "Misc", defaultValue : null}
		}
	}});*/	    
	/* Line chart: A UI5 custom control wrapping some D3.js code */
	sap.ui.core.Control.extend("control.linechart", { 
		/* the control API */
		metadata : {
			 properties : {
		            "items" : { type: "any" },
	                "height" : {type: "int"},
	                "width" : {type: "int"},
	                "popup" : {type: "any"}
		        },
			events: {
				"select" : {},
				"selectEnd": {}				
			}			
		},
	
		
		// the part creating the HTML:
		renderer : function(oRm, oControl) {	// static function, so use the given "oControl" instance instead of "this" in the renderer function
			oRm.write("<div"); 
			oRm.writeControlData(oControl);		// writes the Control ID and enables event handling - important!
			oRm.addClass("lineChart");				// add a CSS class for styles common to all control instances
			oRm.writeClasses();					// this call writes the above class plus enables support for my.Bubble.addStyleClass(...)				
			oRm.write(">");
			oRm.write("</div>");
		},
		
		onAfterRendering: function() {
			
			//alert(oBundle1.getText("label.test"));
			var that = this;
			var deepClonedCopy = jQuery.extend(true, {}, this.getItems());
			var data;
			data = $.map(deepClonedCopy, function(el) { return el });
			
			var uniqueNames = [];
			//var parseDate = d3.time.format("%d-%b-%y %H:%M %p").parse;
			var parseDate = d3.time.format("%Y").parse;
			data.forEach(function(d) {
			  uniqueNames.push(d3.keys(d).filter(function(key) {
			      return key !== "date" && key !== "maxLength";
			    }));
			  });
			uniqueNames = d3.set(d3.merge(uniqueNames)).values();
			var refinedData = [];
			var allValues = [];
			var allDates =[];
			uniqueNames.forEach(function(tonneVal){
				var nameData = {};
				nameData.name = tonneVal;
				nameData.values = [];
				data.forEach(function(md){
					if (md[tonneVal]){
					  allValues.push(md[tonneVal]);
						allDates.push(parseDate(md.date.toString()));
						nameData.values.push({date: parseDate(md.date.toString()), value: md[tonneVal]});
					}
				})
				refinedData.push(nameData)
			});
		    //	var passedheight = this.getHeight();
			
			if(data.length >= 1){
			//var containerWidth = jQuery.sap.byId(this.oParent.sId).width() || 800; // gets super parent width
			var containerWidth = $("#"+this.getId()).parent().width(); // gets immediate parent width
			var containerheight = $("#"+this.getId()).parent().height();
			//var containerheight = 300;
			
			var margin = {top: 15, right: 20, bottom: 30, left: 50},
			//width = containerWidth- margin.left - margin.right,
			width = parseInt(d3.select('.lineChart').style('width'), 10)-margin.left - margin.right,
			 height = containerheight - margin.top - margin.bottom;
			console.log(width+"<====--------------------------====>"+height);
		
				 //var parseDate = d3.time.format("%d-%b-%y %H:%M %p").parse;
				var parseDate = d3.time.format("%Y").parse;  
				var maxDate = d3.time.year.offset(parseDate(d3.max(data, function(d) {
				    return d.date;
				  }).toString()), +1);
				  var minDate = d3.time.year.offset(parseDate(d3.min(data, function(d) {
				    return d.date;
				  }).toString()), -1);

				  var div = d3.select("#toolTip");

				  var x = d3.time.scale()
				  .domain([minDate, maxDate])
				    //.domain(d3.extent(allDates))
				    .range([0, width]);

				  var y = d3.scale.linear()
					  .domain([0, d3.max(allValues)])
				    .range([Math.ceil(height), 0]);

				  var color = d3.scale.ordinal()
				     .range(["#1F78B4","#F8766D",  "#A3A500", "#00BF7D","#FFCF79", "#9B9AFF", "#22C8B2","#725885","#8a89a6",  "#D1A671", "#3399FE", "#938D6B", "#F4B2A6", "#8B6B6E","#CFC38F",]);


				  var xAxis = d3.svg.axis()
				    .scale(x).ticks(6)
				    //.ticks(d3.time.hour,2)
				    .orient("bottom");

				  var yAxis = d3.svg.axis()
				    .scale(y)
				    .orient("left").ticks(5).tickSize(-width, 0, 0);

				  var line = d3.svg.line()
				    .x(function(d) {
				      return x(d.date);
				    })
				    .y(function(d) {
				      return y(d.value);
				    });

			var svg = d3.select("#"+this.getId()).append("svg")
			    .attr("width", width + margin.left + margin.right)
			    .attr("height", height + margin.top + margin.bottom)
			    .append("g")
			    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


			  //x.domain(d3.extent(data, function(d) { return d.date; }));

			  svg.append("g")
			    .attr("class", "x axis")
			    .attr("transform", "translate(0," + height + ")")
			    .call(xAxis);

			  svg.append("g")
			    .attr("class", "y axis")
			    .call(yAxis).append("text")
		        .attr("transform", "translate("+ (0/2) +","+(height/2)+")rotate(-90)")
			      //.attr("transform", "rotate(-90)")
			      .attr("y", -30)
			      .attr("dy", "-1.29em")
			      .style("text-anchor", "middle")
			      .text("y-axis label");

			  var wsf = svg.selectAll(".wsf")
			    .data(refinedData)
			    .enter().append("g")
			    .attr("class", "wsf");

			  wsf.append("path")
			    .attr("class", "line")
			    .attr("d", function(d) {
			      return line(d.values);
			    })
			   /* .transition()
			    .duration(1000)
			    .ease("linear")
			    .delay(function (d, i) {
					return i * 100;
				})*/
			    
			    .style("stroke", function(d) {
			      return color(d.name);
			    });
			  
			  d3.selectAll(".line").each(function(d,i){

					// Get the length of each line in turn
					var totalLength = d3.select(".line").node().getTotalLength();

						d3.selectAll(".line").attr("stroke-dasharray", totalLength + " " + totalLength)
						  .attr("stroke-dashoffset", totalLength)
						  .transition()
						  .duration(2000)
						  .delay(100*i)
						  .ease("linear") //Try linear, quad, bounce... see other examples here - http://bl.ocks.org/hunzy/9929724
						  .attr("stroke-dashoffset", 0);
						//  .style("stroke-width",3)
					});
			  
			  wsf.selectAll(".dot")
			    .data(function(d) {
			      return d.values;
			    })
			    .enter().append("circle")
			    .attr("class", "dot")
			    .attr("stroke", function(d,i) {
			      return color(this.parentNode.__data__.name)
			    })
			    .attr("fill", function(d,i) {
			      return color(this.parentNode.__data__.name)
			    })
			   
			    .on("mouseover", function(d) {/*
			    	
			      formatDate = d3.time.format("%H:%M %p");
			      div.transition().duration(100).style("opacity", .9);
			      div.html( this.parentNode.__data__.name + "<br/>" + d.value + "<br/>" + "<br/>" + " Tonne handled from <br/>" + 
			    		  formatDate(d3.time.minute.offset(d.date,-30)) +" to " +
			    		  formatDate(d3.time.minute.offset(d.date,30))
			      )
			      .style("left", (d3.event.pageX) + "px").style("top", (d3.event.pageY - 28) + "px").attr('r', 8);
			      d3.select(this).attr('r', 8)
			    */}).on("mouseout", function(d) {
			      div.transition().duration(600).style("opacity", 0)
			      d3.select(this).attr('r', 3);
			    }).transition()
		        .duration(2000)
		        .ease("elastic")
		        .delay(function (d, i) {
					return i * 300;
				})
			    .attr("r", 3)
			    .attr("cx", function(d,i) {
			      return x(d.date);
			    })
			    .attr("cy", function(d,i) {
			      return y(d.value);
			    });

			  /*var legend = svg.selectAll(".legend")
			    .data(uniqueNames.sort())
			    .enter().append("g")
			    .attr("class", "legend")
			    .attr("transform", function(d, i) {
			      return "translate(0," + i * 20 + ")";
			    });

			  legend.append("rect")
			    .attr("x", width - 18)
			    .attr("width", 18)
			    .attr("height", 4)
			    .style("fill", function(d) {
			      return color(d);
			    });

			  legend.append("text")
			    .attr("x", width - 24)
			    .attr("y", 6)
			    .attr("dy", ".35em")
			    .style("text-anchor", "end")
			    .text(function(d) {
			      return d;
			    });*/
			}
			},
	});