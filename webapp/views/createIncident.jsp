<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="-1" />
		<title>Simulator Tool</title>
		<link rel="stylesheet" href="css/main.css" type="text/css" />
		<script src="js/sockjs-0.3.4.js"></script>
    	<script src="js/stomp.js"></script>
		<script id="sap-ui-bootstrap"
		   src="sap-ui-core.js"
		   data-sap-ui-theme="sap_bluecrystal"
		   data-sap-ui-libs = "sap.m,sap.ui.commons,sap.tnt"
		   data-sap-ui-libs ="sap.m,sap.ui.core,sap.ui.commons,sap.ui.table,sap.ui.layout,sap.ui.ux3">
		</script>

		  <script type="text/javascript" src="js/commoncontrols.js"></script> 
<script type="text/javascript" src="js/commons.js"></script>
		<script>
			sap.ui.jsview("oui5mvc.createIncident");
			toastTime='${TOASTTIME}';
		</script>
		<!-- <link rel="icon" type="image/png" href="images/cpcs.png" /> -->
	</head>
	<body class='sapUiBody' role='application'>
		<div id="header"></div>
		<div id="navigation"></div>
		<div id="filters"></div>
		<div id="actions" style="margin:0;"></div>
		<div id="data" style="margin:0;"></div>
		<div id="dataArea" style="margin:0;"></div>
		<div id="footer"></div>
	</body>
</html> 