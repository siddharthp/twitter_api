<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" id="main" href="css/main.css" type="text/css" />		
		<title>Login - I Serve U Login Page</title>

		<script>
			//clear everything from session storage
			sessionStorage.clear();		
		</script>
		<script id="sap-ui-bootstrap"
		   src="sap-ui-core.js"
		   data-sap-ui-theme="sap_bluecrystal"
		   data-sap-ui-libs="sap.ui.commons,sap.ui.table">
		</script>
		<script type="text/javascript" src="js/commons.js"></script>
		<script type="text/javascript" src="js/commoncontrols.js"></script>
		<script>
			//clear everything from session storage
		 	oStorage.put("guiver", "Demo version.");
		 	oStorage.put("vendorLogoURL", "http://prosaitov.ru/userfiles/editor/medium/437_wallet1.png");
		 	sap.ui.view({type:sap.ui.core.mvc.ViewType.JS, viewName:"oui5mvc.login"});
		</script>
		<link rel="icon" type="image/png" href="images/asms.png" />
	</head>
	<body class='loginUiBody' role='application'>
        <div class="loginFormContainer">
        <div id="header" class="header"></div>
		<div id="loginArea"></div>
		<div id="footer" class="footer"></div>
        </div>
	</body>
</html>