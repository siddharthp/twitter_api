<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>Twitter Feed</title>
		<link rel="stylesheet" href="css/main.css" type="text/css" />
		<link rel="icon" type="image/jpg" href="http://iserveu.in/wp-content/uploads/2016/09/iServeU.png" />
		<script id="sap-ui-bootstrap"
		   src = "sap-ui-core.js"
		   data-sap-ui-theme = "sap_bluecrystal"
		   data-sap-ui-libs = "sap.m,sap.ui.commons,sap.tnt,sap.ui.table">
		</script>
		<script src="../js/jquery-ui.js"></script>
		<script src="../js/jquery.ui.touch-punch.min.js"></script>
		 <script>
			sap.ui.jsview("oui5mvc.twitterfeed");
		</script> 
	</head>
	<body class='sapUiBody' role='application'>
		<div id="header"></div>
		<div id="content" class="contentArea"></div>
		<div id="footer" class="footerArea"></div>
		
		<!-- <div id="toolTip" class="tooltip" style="opacity: 0;"></div> -->
	 </body>
	 </html>