//Global variables to be accessed by customHeaderTC.js and processEquipmentStatus()
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("sap.ui.commons.MessageBox");
oStorage = jQuery.sap.storage("session");
var statusMsg;
var statusCode;
var isCloseEvent = true;
var dialogIdArray=[];
var airlineData;
var cartTrolleyData;;
var unitData;
var trayData;
var dialoguecustSplitterArr=[];
var dialoguecustSplitter;
var dialoguetextAreaCol;
var totalTrayDailogWidth=2400;
var dialoguetextAreaCol;
var emptyCartAdditionPossible=true;
var inventoryheaders; 
var inventorydataList;
var oBundle1 = "";
var isAirlineChanged = true;
var previousAirlineIndexSelected = -1;
//get your appPath
var appPath = window.location.pathname.split('/')[1];

defaultLocale = "en-EN";
var countryLocale = "de-FR";
if(sessionStorage.getItem("localang")!="" && sessionStorage.getItem("localang")!= null && undefined != sessionStorage.getItem("localang")){
	oBundle1 = jQuery.sap.resources({url : "i18n/labels.properties", locale: sessionStorage.getItem("localang")});
}else{
	oBundle1 = jQuery.sap.resources({url : "i18n/labels.properties", locale: defaultLocale});
}

$('a').on('click', function() { isCloseEvent = false; //alert('a')
});
$('form').on('submit', function() { isCloseEvent = false; //alert('form submit')
});
$(window).on("load", function(){ isCloseEvent = false; //alert('reload')
});
$(window).on("unload", function() { 
    if (isCloseEvent)
    {
    	//alert("Do you really want to close ?");
    }
});
//set is back available
var isBackPageAvailable=false;

//Acerp Icon availability check
var acerpIconAvailable=localStorage.getItem("acerpIconAvailable");
var acerpMonitorFailed=localStorage.getItem("acerpMonitorFailed");
var updateAcerpArr=[];
var indexAcerpArr = [];
var acerpDialog;
var oRemDialog;

String.prototype.isValidDate = function() {
	  var IsoDateRe = new RegExp("^([0-9]{4})/([0-9]{2})/([0-9]{2})$");

	  var matches = IsoDateRe.exec(this);
	  if (!matches) return false;
	  
	  var composedDate = new Date(matches[1], (matches[2] - 1), matches[3]);

	  return ((composedDate.getMonth() == (matches[2] - 1)) && 
			  (composedDate.getDate() == matches[3]) && 
			  (composedDate.getFullYear() == matches[1]));

	}

//Generic JS method to be accessed in all PC Dialog views to register Header control
function commonHeader(){
	$.getScript("js/customHeader.js", function(){
		oAppHeader = new sap.ui.commons.Header("appHeader", {
			logoSrc: "images/cpcs_logo_82x40.jpg", 
			logoText:oBundle1.getText("labels.vcbs.title"),
			systemMenu: oMenuBar,
			settingsMenu: oSetgsMenuBar
		});
		oAppHeader.placeAt("header");
		if (Uname == undefined || Uname == "" || Uname == null)
			sap.ui.getCore().byId("settings-item-0").setVisible(false);
	});
}

function commonHeaderOnlyHelpMnu(){
	$.getScript("js/customHeader.js", function(){
		oAppHeader = new sap.ui.commons.Header("appHeader", {
			logoSrc: "images/cpcs_logo_82x40.jpg", 
			logoText:oBundle1.getText("labels.vcbs.title"),
			displaySysMenu: false,
			displaySettings: false,
			systemMenu: oMenuBar,
			settingsMenu: oSetgsMenuBar
		});
		if (!oAppHeader.getDisplaySettings())
			sap.ui.getCore().byId("settings-item-0").setVisible(false);
		if (!oAppHeader.getDisplayHelp())
			sap.ui.getCore().byId("help-item-0").setVisible(false);

		oAppHeader.placeAt("header");
	});
}

function renderDataSetTC(){//Create a custom control as template for the Dataset items 
	var horizantallayout =sap.ui.core.Control.extend("HorizontalLayout", {
	    metadata : {
	        aggregations : {
				"form" : {type : "sap.ui.commons.form.Form", multiple : false},
	        }
	    },

		renderer: function(rm, ctrl){
			rm.write("<div>");
			rm.renderControl(ctrl.getForm());
			rm.write("</div>");
	    },
	    
	});
}

//prepare touch screen dialog title
function commonTouchConsoleTitle(title){
 	new sap.ui.commons.Label("horizontalheader",{
 		text: title
 	}).addStyleClass("labelBelowHeader").placeAt("navigation");
}

//prepare title for PC dialogues
function commonPCTitle(title,dialog){
	//Back Button, place at navigation bar
	backButton(dialog);
	//prepare button, on click of it should display acerp notifications
	acerpNotifications(dialog);
	var dialogTitle=new sap.ui.commons.Link("Clear",{text: title, lite: true, tooltip:title, 
		styled: false,
		press:function(){
			checkForChangesHeader(dialog,false);
		}}).addStyleClass("labelBelowPCHeader");
	dialogTitle.placeAt("navigation");
}

function checkForChangesHeader(id,isForBack){
	oStorage.get("changedParams")
	if(oStorage.get("changedParams") == null){
		if(isForBack){
			redirectToBackPage(id);
		}else{
			window.location=id;
		}
	}else{
		changedParamList = JSON.parse(oStorage.get("changedParams"));
		if(changedParamList.length == 0){
			 if(isForBack){
				redirectToBackPage(id)
			 }else{
				window.location=id;
			 }
		}else{
			sap.ui.commons.MessageBox.confirm(oBundle1.getText("msg.ssys.confirm.to.loose.changes"), 
					 function isUserWantsToLoose(bResult){
						 if(bResult){
							 oStorage.remove("changedParams");
							 if(isForBack){
								redirectToBackPage(id)
							 }else{
								window.location=id;
							 }
						 }
					 }, 
					 oBundle1.getText("btn.confirm"));
		}
	}
}

//prepare back link/button
function backButton(dialog){
	//set current dialog to backDialogues
	backDialogues=JSON.parse(sessionStorage.getItem("previousDialogues"));
	isBackPageAvailable=false;
	if(undefined ==JSON.parse(sessionStorage.getItem("backPage"))){
		if(undefined == backDialogues){		
			backDialogues=[];
			backDialogues.push(JSON.stringify(dialog));
		}else{
			//if current dialog is already available in stack of backDailogues, remove it
			if(backDialogues.indexOf(JSON.stringify(dialog))>=0){
				backDialogues.splice(backDialogues.indexOf(JSON.stringify(dialog)),1);
			}
			//push dialog to stack of backDialogues
			backDialogues.push(JSON.stringify(dialog));
		}
		//set stack of backDialogues to session storage
		sessionStorage.setItem("previousDialogues",JSON.stringify(backDialogues));
	}else{
		sessionStorage.removeItem("backPage");
		isBackPageAvailable=true;
	}
	//if back to current dialog is available (backDialogues should have current dialog and any other dialog(s)), display back button to user
	if(undefined != backDialogues){	
		if(backDialogues.length>1){
			var backPage = backDialogues[backDialogues.length-2];
			backPage=backPage.replace(/"/g, "").toUpperCase();
			//Back Button, place at navigation bar
			var backButton=new sap.ui.commons.Button("Back",{text: oBundle1.getText("label.back",backPage), lite: true, tooltip:oBundle1.getText("label.back.tooltip",backPage),
				icon : "sap-icon://nav-back",
				styled: false,
				press:function(){
					checkForChangesHeader(dialog,true);
				}});
			backButton.placeAt("navigation");
		}else{
			new sap.ui.commons.Button().addStyleClass("invisbleBackButtonLeft").placeAt("navigation");
		}
	}else{
		new sap.ui.commons.Button().addStyleClass("invisbleBackButtonLeft").placeAt("navigation");
	}
	//new sap.ui.commons.Button().addStyleClass("invisbleBackButtonRight").placeAt("navigation");
}

//if user clicks back link, redirect to back page
function redirectToBackPage(dialog){
	//set current dialog to back dialogues 
	backDialogues=JSON.parse(sessionStorage.getItem("previousDialogues"));
	if(undefined != backDialogues){					
		if(backDialogues.length>1){
			if(backDialogues.indexOf(JSON.stringify(dialog))>=0){
				backDialogues.splice(backDialogues.indexOf(JSON.stringify(dialog)),1);
			}
			sessionStorage.setItem("backPage",JSON.stringify(dialog));
			sessionStorage.setItem("previousDialogues",JSON.stringify(backDialogues));
			var data = backDialogues[backDialogues.length-1];
			data=data.replace(/"/g, "");
		}			
	}
	window.location=data;
}

function displayListener(oEvent) {
	var bShow = oEvent.getParameter("show");
	var oControl = oEvent.getSource();

	if (bShow) {
		/*
		 * Now the application can decide how to display the bar. It can be maximized, default, minimized (please see NotificationBarStatus) 
		 */
		var sStatus = sap.ui.ux3.NotificationBarStatus.Default;
		oControl.setVisibleStatus(sStatus);
	} else {
		var sStatus = sap.ui.ux3.NotificationBarStatus.None;
		oControl.setVisibleStatus(sStatus);
	}
}

function clickListener(oEvent) {
	var oMessage = oEvent.getParameter("message");
}

function notificationBar(){
	 oNotifier = new sap.ui.ux3.Notifier({
		title : "Notifications",
		messageSelected: clickListener
	});

	var oNotiBar = new sap.ui.ux3.NotificationBar("notibar",{
		display : displayListener, visibleStatus : "Min", alwaysShowToggler: true,
	});

	oNotiBar.addNotifier(oNotifier);
	oNotiBar.placeAt("footer");
}

function handleAjaxError(xhr, status, statusText)
{
	jQuery.sap.require("sap.ui.commons.MessageBox");
	if (xhr.getResponseHeader("SESSION-EXPIRED"))
	{
		sap.ui.commons.MessageBox.show(oBundle1.getText("session.timed.out.relogin"), sap.ui.commons.MessageBox.Icon.ERROR, "Error", [sap.ui.commons.MessageBox.Action.OK], function(){
			sap.ui.core.BusyIndicator.hide();
			window.location = "login";
		});
	}
	else if (xhr.getResponseHeader("SESSION-IN-PROGRESS"))
	{
		var sessionBelongsTo = xhr.getResponseHeader("SESSION-BELONGS-TO"); 
		if (null != sessionBelongsTo && 'tc' === sessionBelongsTo){
			sap.ui.commons.MessageBox.show("An active session is in progress in this browser web session.\nClick OK to continue accessing the existing session.\nClick ABORT to terminate the existing session.", 
				sap.ui.commons.MessageBox.Icon.ERROR, "Error", [sap.ui.commons.MessageBox.Action.OK, sap.ui.commons.MessageBox.Action.ABORT], 
				function(action){
					if ('OK' === action)
						window.location = "/" + appPath + "/" + sessionBelongsTo + "/" + currentTCDialog;
					else if ('ABORT' === action){
						window.location = "/" + appPath + "/" + sessionBelongsTo + "/logout";
				}
			});
		} else{
		sap.ui.commons.MessageBox.show("An active LDS session is in progress in this browser web session. Please use a different web browser to establish a new connection to LDS", 
				sap.ui.commons.MessageBox.Icon.ERROR, "Error", [sap.ui.commons.MessageBox.Action.OK], function(){
			sap.ui.core.BusyIndicator.hide();
			window.location = "/"+appPath;
		});
		}
	}
	else if (xhr.getResponseHeader("TC-SESSION-IN-PROGRESS"))
	{
		sap.ui.core.BusyIndicator.hide();
		window.location = "/" + appPath + "/tc/" + currentTCDialog;
	}
	sap.ui.core.BusyIndicator.hide();
}

// ########################################### Touchconsole Screen Common Controls #############################################
//Generic JS method to be accessed in all Touch Console views to register Header control
function commonHeaderTC(){
	$.getScript("../js/customHeaderTC.js", function(){
		oAppHeader = new sap.ui.commons.Header("appHeader", {
			logoSrc: "../images/cpcs_logo_82x40.jpg", 
			logoText:oBundle1.getText("labels.vcbs.title"),
		});		
		oAppHeader.placeAt("header");
	});
}

function commonTitle(title){
 	new sap.ui.commons.Label("horizontalheader",{
 		text: title,
 	}).addStyleClass("labelBelowHeader").placeAt("navigation");
}

function processEquipmentStatus(equipmentStatus){
	// Get Custom Application Header instance based on ID
	var header = sap.ui.getCore().byId("appHeader");

	// Set the Equipment Status Message and Code into global variables
	if (equipmentStatus === "GREEN"){
		statusMsg = "Equipment is In Service & Automatic with no Error";
		statusCode = "";
	}
	else if (equipmentStatus === "RED"){
		statusMsg = "Equipment PLC is in Fault mode";
		statusCode = "ERR";
	}
	else if (equipmentStatus === "GRAY"){
		statusMsg = "Equipment PLC is Off";
		statusCode = "OFF";
	}
	else if (equipmentStatus === "ORANGE"){
		statusMsg = "Equipment PLC is in Manual mode";
		statusCode = "MAN";
	}
	else if (equipmentStatus === "HATCH"){
		statusMsg = "Equipment is set to Out Of Service";
		statusCode = "OOS";
	}
	else{
		equipmentStatus = "BLANK";
		statusMsg = "Equipment status is UNKNOWN";
		statusCode = "";
	}
	
	// Set the equipment status code to Header instance.
	header.setEquipmentStatusCode(statusCode);

	// Toggle the color based on equipment status, SessionStorage value and finally set the values to Header instance. 
	var prevEqStatus = window.sessionStorage.getItem("prevEqStatus");
	if (prevEqStatus){
		if (prevEqStatus != "BLANK")
			header.setRemoveStatusColor(prevEqStatus.toLowerCase() + "Color");
		else
			header.setRemoveStatusColor("Color");
	}
	if (equipmentStatus != "BLANK")
		header.setAddStatusColor(equipmentStatus.toLowerCase() + "Color");
	else
		header.setAddStatusColor("Color");
	window.sessionStorage.setItem("prevEqStatus", equipmentStatus);
}

function connect() 
{
	var socket = new SockJS('/'+appPath+'/vcbsreports');	
	var stompClient_cmn = Stomp.over(socket);
	
	stompClient_cmn.connect({}, function(frame) 
    {
		stompClient_cmn.subscribe('/topic/vcbsreports', function(PMSreportsData)
 		{         			
         	var msgBody = PMSreportsData.body;
         	var msgObj = JSON.parse( msgBody );
         	 var data = msgObj["REPORT"];
         
           	var oMessage = new sap.ui.core.Message({
	    		text : data["text"],
	    		readOnly : true,
	    		
	    	});
           	switch (data["level"]) {
    		case 0:
    			oMessage.setLevel(sap.ui.core.MessageType.Information);
    			break;
    		case 1:
    			oMessage.setLevel(sap.ui.core.MessageType.Success);
    			break;
    		case 2:
    			oMessage.setLevel(sap.ui.core.MessageType.Warning);
    			break;
    		case 3:
    		default:
    			oMessage.setLevel(sap.ui.core.MessageType.Error);
    			break;

    		}
         	
	        oNotifier.addMessage(oMessage);
        });
    	var sessiontimeoutreport='/topic/'+LdsSessId+'/sessiontimeOutreports';
    	stompClient_cmn.subscribe(sessiontimeoutreport, function(sessiontimeOutreports)
	    	 	{         			
					var msgBody = sessiontimeOutreports.body;
					var msgObj = JSON.parse( msgBody );
					var data = msgObj["REPORT"];
					if(data != undefined){
						isBusy = true;
						jQuery.sap.require("sap.ui.commons.MessageBox");
			          	sap.ui.core.BusyIndicator.hide();
			           	sap.ui.commons.MessageBox.show(oBundle1.getText("session.timed.out.relogin"), sap.ui.commons.MessageBox.Icon.ERROR, "Error", [sap.ui.commons.MessageBox.Action.OK], function(){
			    			window.location = "logout";
			    			sap.ui.core.BusyIndicator.show();
			    		});
					}
	        });
    	stompClient_cmn.subscribe('/topic/acerpErrReports', function(acerpErrReports)
    	 	{      
			var msgBody = acerpErrReports.body;
         	var msgObj = JSON.parse( msgBody );
         	var data = msgObj["acerpIsSetFlag"];
           	if(data == "0"){
         		acerpButton.setVisible(false);
         		localStorage.removeItem("acerpIconAvailable");
         		localStorage.removeItem("acerpMonitorFailed");
         	}else{
         		acerpButton.setVisible(true);
         		acerpButton.addStyleClass("acerpButtonBGC");
         		localStorage.setItem("acerpIconAvailable",true);
            }
	});
    	stompClient_cmn.subscribe('/topic/acerpErrorDataUpdate', function(acerpErrReports)
    	 		{      
    				
    	         	var msgBody = acerpErrReports.body;
    	         	var msgObj = JSON.parse( msgBody );
    	         	
    	         	var tabData=modelData1["genericTableModel"];
    				
    				for (var i = 0; i < modelData1["genericTableModel"] .length; i++) {
    					var errTimeLong = modelData1["genericTableModel"][i].err_time;
    					if(errTimeLong != 0){							
    						modelData1["genericTableModel"][i].err_time = dateFormatString(errTimeLong,true);
    					}else{
    						modelData1["genericTableModel"][i].err_time = "";
    					}
    					var msgTimeLong = modelData1["genericTableModel"][i].msg_time;
    					if(msgTimeLong != 0){
    						modelData1["genericTableModel"][i].msg_time = dateFormatString(msgTimeLong,true);
    					}else{
    						modelData1["genericTableModel"][i].msg_time = "";
    					}
    				}
    				
    				
    				acerpTable.setModel(new sap.ui.model.json.JSONModel(modelData1));
    				acerpTable.bindRows({path: "/genericTableModel"});
    				acerpTable.getModel().refresh();
    	          
    	           	
    	        });
    	
	});
}

//get date field in the format of YYYY/MM/DD HH:mm:ss
function dateFormatString(dateInMilliseconds){
    dateValue=new Date(parseFloat(dateInMilliseconds));
	dateString= dateValue.getFullYear()+"/"+getValueOfDateStr((dateValue.getMonth()+1))+"/"+getValueOfDateStr(dateValue.getDate())+" "+getValueOfDateStr(dateValue.getHours())+":"+getValueOfDateStr(dateValue.getMinutes())+":"+getValueOfDateStr(dateValue.getSeconds());
	return dateString;
}


//get date field in the format of YYYYMMDD 
function dateFormatStringYYYYmmdd(dateInMilliseconds){
    dateValue=new Date(parseFloat(dateInMilliseconds));
	dateString= dateValue.getFullYear()+""+getValueOfDateStr((dateValue.getMonth()+1))+""+getValueOfDateStr(dateValue.getDate());
	return dateString;
}


//get date field in the format of YYYY/MM/DD HH:mm:ss
//timeStampReq- true: YYYY/MM/DD HH:mm:ss
//timeStampReq- false: YYYY/MM/DD
function dateFormatString(dateInMilliseconds,timeStampReq){
	dateValue=new Date(parseFloat(dateInMilliseconds));	
	dateString= dateValue.getFullYear()+"/"+getValueOfDateStr((dateValue.getMonth()+1))+"/"+getValueOfDateStr(dateValue.getDate());	
	if(timeStampReq){
		//get date field in the format of YYYY/MM/DD HH:mm:ss
		dateString=dateString+" "+getValueOfDateStr(dateValue.getHours())+":"+getValueOfDateStr(dateValue.getMinutes())+":"+getValueOfDateStr(dateValue.getSeconds());
	}
	return dateString;
}


//date formatting, if month = 5, return it as 05
function getValueOfDateStr(field){
	if(undefined != field.toString()){
		if(field.toString().length == 2){
			return field;
		}else if(field.toString().length == 1){
			field = '0'+field;
		}else if(field.toString().length == 0){
			field = '00';
		}	
	}else{
		field = '00';
	}
	
	return field;
}



function acerpNotifications(dialogue)
{
	// create a standalone toolbar
	 acerpButton =  new sap.ui.commons.Button({tooltip : oBundle1.getText("label.acerp.notifications"),visible:false,
		press : function() {
						
						//set isBusy as true
							 isBusy=true;
							// sap.ui.core.BusyIndicator.show(0);
						 updateAcerpArr=[];
						 indexAcerpArr = [];
						 acerpDialog = new sap.ui.commons.Dialog({title: oBundle1.getText("label.acerp.messages"),width: "80%",modal: true});
						 var aForm = new sap.ui.layout.form.Form("AF1",{
								maxContainerCols: 2,
								layout: new sap.ui.layout.form.ResponsiveGridLayout("AL"),
								formContainers: [
									new sap.ui.layout.form.FormContainer("AF1C1",{
										formElements:[
													 
										    new sap.ui.layout.form.FormElement("AF1C1E1", {
										    	fields:[
													//Create an instance of the table control
													acerpTable = new sap.ui.table.Table("acerptable",{
														selectionMode: sap.ui.table.SelectionMode.single,editable:true,
														selectionBehavior: sap.ui.table.SelectionBehavior.RowOnly,
														navigationMode: sap.ui.table.NavigationMode.Scrollbar,showNoData: false,
														layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12"}),
														
														columns:
														[
															new sap.ui.table.Column({
																width: "8%",
																label: new sap.ui.commons.Label({text: oBundle1.getText("lbl.resolved")}),
																template:new sap.ui.commons.CheckBox({checked:false,
																	change : function(oEvent) {
																		if (oEvent.getSource().getChecked())
																		{	
																			updateAcerpArr.push(this.getBindingContext().getProperty("idx"));
																			indexAcerpArr.push(oEvent.getSource().getId());
																		}
																		else
																		{	
																			var index = updateAcerpArr.indexOf(this.getBindingContext().getProperty("idx"));
																			var cbIndex = indexAcerpArr.indexOf(oEvent.getSource().getId());
																			if (index > -1) {
																				updateAcerpArr.splice(index, 1);
																			}
																			if (cbIndex > -1) {
																				indexAcerpArr.splice(cbIndex, 1);
																			}
																		}
																		
																	}
																	})
																
																
															}),
															new sap.ui.table.Column("ets",{
																 width: "14%",
																label: new sap.ui.commons.Label({text: oBundle1.getText("lbl.error.ts")}),
																template: new sap.ui.commons.TextView().bindProperty("text", "err_time"), 
																sortProperty:"err_time",
																change: function (newValue) {
											                        if (this.getValueState() !== sap.ui.core.ValueState.Error) {
											                            oController.update(this.getBindingContext().getObject());
											                        }
											                    }
															}),
															new sap.ui.table.Column({
																width: "10%",
																label: new sap.ui.commons.Label({text: oBundle1.getText("lbl.tbl")}),
																template: new sap.ui.commons.TextView().bindProperty("text", "table"),
																sortProperty:"table",
																
																
															}),
															new sap.ui.table.Column({
																width: "14%",
																label: new sap.ui.commons.Label({text: oBundle1.getText("lbl.msg.ts")}),
																template: new sap.ui.commons.TextView().bindProperty("text", "msg_time"),
																sortProperty:"msg_time",
																
															}),
															new sap.ui.table.Column({
																width: "9%",
																label: new sap.ui.commons.Label({text: oBundle1.getText("lbl.action")}),
																template: new sap.ui.commons.TextView().bindProperty("text", "action"),
																sortProperty:"action",
																
															}),
															new sap.ui.table.Column({
																width: "22.5%",
																label: new sap.ui.commons.Label({text: oBundle1.getText("lbl.ms")}),
																template: new sap.ui.commons.TextView().bindProperty("text", "msg"),
																sortProperty:"msg",
																
															}),
															new sap.ui.table.Column({
																width: "22.5%",
																label: new sap.ui.commons.Label({text: oBundle1.getText("lbl.err")}),
																template: new sap.ui.commons.TextView().bindProperty("text", "error"),
																sortProperty:"error",
																
															}),
															
																				
														],
														rowSelectionChange: function(e) {
															 							
														},
														
														sort:function (e1) {
															
										                 }
													
													}).addStyleClass("customTable"),
													
																		 	
													
												]
										    })
										],					
										layoutData: new sap.ui.layout.GridData({span: "L11 M11 S11"}),
									}),
								]
							});
						 	acerpTable.bindRows({path: "/genericTableModel1"});
						 	acerpDialog.addContent(aForm);
						 	isBusy=true;
							acerpDialog.addButton(new sap.ui.commons.Button({text: oBundle1.getText("label.save"),tooltip: oBundle1.getText("label.save"), icon: "sap-icon://save", style: sap.ui.commons.ButtonStyle.Accept,enabled: true,
								press:function(){
									
									//FOR DELETING RECORD
									
									if( updateAcerpArr.length > 0 ){
										var cmdArray = [] ;
										var jsonObjToSend = {} ;
										for( var i=0 ; i< updateAcerpArr.length ; i++){
											var cmdObj = {} ;
											cmdObj["dialogue"] = "SMES";
											cmdObj["cid"] = "ac_err";
											cmdObj["fcode"] = 9;
											cmdObj["background"] = 1;
											var srachmap = {} ;
											srachmap["idx"] =  updateAcerpArr[i] ;
											cmdObj["search"] =  srachmap;
											var datalist = [["1"]];
											cmdObj["header"] = ["resolved"] ;
											cmdObj["data"] = datalist ;
											
											cmdArray.push( cmdObj ) ;
										}
										jsonObjToSend["BULK_CMD"] = cmdArray ;
																			
										doAjaxCommon("/unified/cmd", jsonObjToSend).done(updateAcerpRecModelData);
										
									}else{
										jQuery.sap.require("sap.ui.commons.MessageBox");
										sap.ui.commons.MessageBox.show("No Record selected to update!!!",sap.ui.commons.MessageBox.Icon.INFO);
									}
									
									
								}
							}));
							acerpDialog.addButton(new sap.ui.commons.Button({text: oBundle1.getText("label.cancel"),tooltip: oBundle1.getText("label.cancel"), icon: "sap-icon://sys-cancel-2", style: sap.ui.commons.ButtonStyle.Reject,
								press:function(){
									acerpDialog.destroyButtons();
									acerpDialog.destroyContent();
									acerpDialog.close();
									
									isBusy=false;
									sap.ui.core.BusyIndicator.hide();
									
								}
							}));
							acerpDialog.attachClosed(function(){
								acerpDialog.destroyButtons();
								acerpDialog.destroyContent();
								isBusy=false;
								sap.ui.core.BusyIndicator.hide();
								
							});
							acerpDialog.open();
						
							var jsonObjToSend = {} ;
							jsonObjToSend["cid"] = "ac_err";
							jsonObjToSend["dialogue"] = "SMES";
							jsonObjToSend["background"] = 1;
							jsonObjToSend["fcode"] = 7;
							jsonObjToSend["start"] = 1;
							jsonObjToSend["nrec"] = 2048;
							jsonObjToSend["maxrec"] = 2048;
							var headers = ["err_time", "table", "msg_time", "action", "msg","error","resolved"] ;
							jsonObjToSend["header"] = headers ;
							var orderList = ["<err_time", ">table", ">msg_time", ">action", ">msg",">error",">resolved"] ;
							jsonObjToSend["order"] = orderList ;
							
							doAjaxCommon("/unified/cmd", jsonObjToSend).done(updateAcerpTableModelData);
					} 
	}).addStyleClass("acerpButton");
	acerpButton.placeAt("navigation");
}


function doAjaxCommon(path, content, type, async) {
	var params = {
		url : "/" +appPath+ path,
		dataType : "json",
		contentType : "application/json",
		context : this,
		cache : false,
		error : handleAjaxError
	};
	params["type"] = type || "POST";
	if (async === false) {
		params["async"] = async;
	}
	if (content) {
		params["data"] = JSON.stringify(content);
	}
	return jQuery.ajax(params);
}

function updateAcerpTableModelData(modelData1) {
	
	console.debug("Ajax response: ", modelData1);
		
	var excepData = null ;
	var resultFromUIC = modelData1["result"];
	if(resultFromUIC == "error" ){
		excepData = modelData1["errortext"];
	}
	else
	{
		excepData=resultFromUIC;
	}		
	 if (excepData != "success") {
	
		jQuery.sap.require("sap.ui.commons.MessageBox");
		sap.ui.commons.MessageBox.show(excepData,
				sap.ui.commons.MessageBox.Icon.ERROR);
		
	}
	else
	{		
		var norec = modelData1["totalMaxRec"];
		if (norec == 0) {
		
			jQuery.sap.require("sap.m.MessageToast");
	    	sap.m.MessageToast.show(oBundle1.getText("msg.no.acerp.error"), {
	    	    duration: parseInt(toastTime),                  // default
	    	    at: "center bottom",
	    	    offset:"0 -60"
	    	   
	    	});
	    	localStorage.setItem("acerpIconAvailable",false);
	    	//No Records so we have to close the Acerp Dialog
	    	//acerpDialog.destroyButtons();
			//acerpDialog.destroyContent();
			acerpDialog.close();
			
	    	
		}
		else
			{
		
		     	for (var i = 0; i < modelData1["genericTableModel"] .length; i++) {
					var errTimeLong = modelData1["genericTableModel"][i].err_time;
					if(errTimeLong != 0){							
						modelData1["genericTableModel"][i].err_time = dateFormatString(errTimeLong,true);
					}else{
						modelData1["genericTableModel"][i].err_time = "";
					}
					var msgTimeLong = modelData1["genericTableModel"][i].msg_time;
					if(msgTimeLong != 0){
						modelData1["genericTableModel"][i].msg_time = dateFormatString(msgTimeLong,true);
					}else{
						modelData1["genericTableModel"][i].msg_time = "";
					}
				}
				
				
				acerpTable.setModel(new sap.ui.model.json.JSONModel(modelData1));
				acerpTable.bindRows({path: "/genericTableModel"});
				acerpTable.getModel().refresh();
										
				
      }

		
	}
			
}

function updateAcerpRecModelData(modelData1) {
	updateAcerpArr = [];
	var updateData = null ;
	var resultFromUIC = modelData1["result"];
	if(resultFromUIC == "error" ){
		updateData = modelData1["errortext"];
	}
	else
		{
		updateData=resultFromUIC;
		}
	
	if (updateData == "success") {
		//updated to display no record found message in messagetoast cpc-208	
    	jQuery.sap.require("sap.m.MessageToast");
    	sap.m.MessageToast.show(oBundle1.getText("msg.record.update.success"), {
    	    duration: parseInt(toastTime),                 
    	    at: "center bottom",
    	    offset:"0 -60"
    	   
    	});
    	for(var i=0;i<indexAcerpArr.length;i++){
			sap.ui.getCore().byId(indexAcerpArr[i]).setChecked(false);
		}
    	acerpTable.setModel(new sap.ui.model.json.JSONModel());
    	acerpTable.getModel().refresh();
						
		var jsonObjToSend = {} ;
		jsonObjToSend["cid"] = "ac_err";
		jsonObjToSend["dialogue"] = "SMES";
		jsonObjToSend["background"] = 1;
		jsonObjToSend["fcode"] = 7;
		jsonObjToSend["start"] = 1;
		jsonObjToSend["nrec"] = 2048;
		jsonObjToSend["maxrec"] = 2048;
		var headers = ["err_time", "table", "msg_time", "action", "msg","error","resolved"] ;
		jsonObjToSend["header"] = headers ;
		var orderList = ["<err_time", ">table", ">msg_time", ">action", ">msg",">error"] ;
		jsonObjToSend["order"] = orderList ;
		
		doAjaxCommon("/unified/cmd", jsonObjToSend).done(updateAcerpTableModelData);
  }else{
	  jQuery.sap.require("sap.m.MessageToast");
  		sap.m.MessageToast.show(oBundle1.getText("msg.record.update.failed"), {
  	    duration: parseInt(toastTime),                 
  	    at: "center bottom",
	    offset:"0 -60"
  	   
  	});
  		isBusy=false;
	//	sap.ui.core.BusyIndicator.hide();
  }
}


function tcNavigation(zone,area,console,oAirlineModel,consoleType,oController){
	var zoneToBeDisplayed=zone;
	zoneToBeDisplayed=zoneToBeDisplayed.replace(/_/g,' ');
	new sap.ui.commons.FormattedTextView("navHeaderSpan",{
 		htmlText: "<span class='navZone'>"+zoneToBeDisplayed+"</span>" +
 				"<span class='navArea'>&nbsp;&nbsp;&nbsp;"+area+"</span><br/>" +
 				"<span class='navConsole'>"+console+"</span>"
 	}).placeAt("navigation");	
	airlinesReceived = oAirlineModel.getData()["genericTableModel"];
	airlineChanges ="";
	if(undefined != airlinesReceived){
		for(i=0;i<airlinesReceived.length;i++){
			if(airlineChanges!=""){
				airlineChanges=airlineChanges+" "+airlinesReceived[i]["value"];
			}else{
				airlineChanges=airlinesReceived[i]["value"];
			}
			
		}
	}
	airlineHeader = new sap.ui.commons.TextView("airlinesInHeader",{text: airlineChanges}).addStyleClass("airlinesHeader");	
	airlineHeader.placeAt("navigation");
	// status buttons
	vc1Btn =   new sap.ui.commons.Button("StatusBtn02",{tooltip:"",}).addStyleClass("statusIndicator").addStyleClass("greenColor").placeAt("navigation");
	vc2Btn =   new sap.ui.commons.Button("StatusBtn03",{tooltip:"",}).addStyleClass("statusIndicator").addStyleClass("greenColor").placeAt("navigation");
	vc3Btn =   new sap.ui.commons.Button("StatusBtn04",{tooltip:"",}).addStyleClass("statusIndicator").addStyleClass("greenColor").placeAt("navigation");
	
	prepareMenu(consoleType,oController);
	
	
}

function tcNavigationCSTA(zone,area,console,oAirlineModel,consoleType,touchConsoleRequestType,oController){
	var zoneToBeDisplayed=area;
	zoneToBeDisplayed=zoneToBeDisplayed.replace(/_/g,' ');
	NavigationText=new sap.ui.commons.FormattedTextView("navHeaderSpan",{
		
 		htmlText: "<span class='navZone'>"+zone+"</span><br/>" +
 				"<span class='cleaningnavArea'>&nbsp;&nbsp;&nbsp;"+zoneToBeDisplayed+"</span>" +
 				"<span class='cleaningnavConsole'>"+console+"</span>"
 	}).placeAt("navigation");
	
	airlinesReceived = oAirlineModel.getData()["genericTableModel"];
	airlineChanges ="";
	if(undefined != airlinesReceived){
		for(i=0;i<airlinesReceived.length;i++){
			if(airlineChanges!=""){
				airlineChanges=airlineChanges+" "+airlinesReceived[i]["value"];
			}else{
				airlineChanges=airlinesReceived[i]["value"];
			}
			
		}
	}
	airlineHeaderSC = new sap.ui.commons.TextView("airlinesInHeader",{text: airlineChanges}).addStyleClass("airlinesHeader");	
	airlineHeaderSC.placeAt("navigation");
	
	cleaningCriticalBtn= new sap.ui.commons.Image("cleaningCriticalBtn",{
		src:"../images/cleaning_icon.png",
		visible:false,
	}).placeAt("navigation");
	
	 new sap.ui.commons.Button("optimizerStatus",{
		visible:true,
	}).addStyleClass("optimizerStatusBtn").placeAt("navigation");
	
	
	prepareMenu(consoleType,touchConsoleRequestType,oController);
	//add cleaning critical icon
	
}


function prepareMenu(consoleType,oController){
	//Create a MenuButton Control
	var oMenuButton = new sap.ui.commons.MenuButton("menuButton",{lite:true, icon: "sap-icon://grid"}); 
	//Create the menu
		var oMenu1 = new sap.ui.unified.Menu("menu1", {ariaDescription: "Menu",
			items:
			[
			 	
			 	oMenuChargingItm = new sap.ui.unified.MenuItem("ChargingMenuItem",{text: oBundle1.getText("lbl.charging.menu")}),
			 	oMenuDischargingItm = new sap.ui.unified.MenuItem("DischargingMenuItem",{text: oBundle1.getText("lbl.discharging.menu")}),
			 	oMenuTerminateItm = new sap.ui.unified.MenuItem("TemplateMenuItem",{text: oBundle1.getText("lbl.template")}),
			 	oMenuCleaningItm = new sap.ui.unified.MenuItem("cleaningconsole",{text: oBundle1.getText("mnu.lbl.cleaningMode")}),
			 	oMenuInventoryItm = new sap.ui.unified.MenuItem("inventoryconsole",{text: oBundle1.getText("mnu.lbl.inventoryMode")}),
			 	oLangMnu = new sap.ui.unified.MenuItem("englishlang", {text: oBundle1.getText("label.lang.english")}),
			 	ochineseLangMnu = new sap.ui.unified.MenuItem("chineselang", {text: oBundle1.getText("label.lang.chinese")}),
			 	oMenuRefItm = new sap.ui.unified.MenuItem("refresh", {text: oBundle1.getText("label.refresh")}),
			 	oMenuLogoutItm = new sap.ui.unified.MenuItem("logout", {text: oBundle1.getText("label.terminate")})
			],
			itemSelect: function(oEvent){
				if(oEvent.getParameter("item").getId() == "refresh" || oEvent.getParameter("item").getId()=="logout"){
					window.location=oEvent.getParameter("item").getId();
				}else if(oEvent.getParameter("item").getId() == "ChargingMenuItem" || oEvent.getParameter("item").getId() == "DischargingMenuItem"){	
					//if ICON is available, do not request for scan staff id
					if(oEvent.getParameter("item").getId() == "ChargingMenuItem"){
						queryTo='CCHR';
						consoleToToggle=oBundle1.getText("CCHR");
					}else{
						queryTo='CDIR';
						consoleToToggle=oBundle1.getText("CDIR")
					}
					if(sap.ui.getCore().byId(oEvent.getParameter("item").getId()).getIcon().length>0){
						window.location=touchConsole+"?queryPage="+queryTo;
					}else{
						oFirstDialog.destroyButtons();
						oFirstDialog.addButton(new sap.ui.commons.Button("scanAdd",{text: oBundle1.getText("btn.ok1"),tooltip: oBundle1.getText("btn.ok1"), icon: "sap-icon://accept", style: sap.ui.commons.ButtonStyle.Accept, enabled: true,visible: false,
							press:function(){
				 				if(!staffId.getLiveValue()){								 					
				 					sap.ui.commons.MessageBox.show(oBundle1.getText("msg.staffid.mandatory"), sap.ui.commons.MessageBox.Icon.ERROR, "Error");
				 				}else {
									oController.validateStaffCard("SHOW",queryTo);
				 				}					 				
							}
						}));
						oFirstDialog.setTitle(oBundle1.getText("lbl.scan.stf.id",consoleToToggle));
						oFirstDialog.attachClosed(function(){
							oFirstDialog.destroyButtons();
							//oFirstDialog.destroyContent();
						});
						oFirstDialog.open();
						staffId.setValue();
						//oFirstDialog.setInitialFocus(staffId);
					}
				}else if(oEvent.getParameter("item").getId() == "cleaningconsole")	{
					var jsonObjToSend = {};
					jsonObjToSend["dialogue"] = "CCLE"; //need to be confirmed
					jsonObjToSend["cid"] = "start_cleaning";
					jsonObjToSend["fcode"] = 9;
					jsonObjToSend["actions"] = true;
					var srachmap = {} ;
					srachmap["station_name"] =touchConsole.toUpperCase();
					jsonObjToSend["search"] = srachmap ;
					sap.ui.controller("oui5mvc.cleaningconsole").startCleaningDialogMode(jsonObjToSend);
					
				}
				else if(oEvent.getParameter("item").getId()=="inventoryconsole"){
					oFirstInventoryDialog = new sap.ui.commons.Dialog({title: oBundle1.getText("title.inventory") , keepInWindow: true, modal: true, width: "800px"}).addStyleClass("inventoryDialogClass");
			        oSimpleForm = new sap.ui.layout.form.Form({
				    			
				    			width: "100%",
				    			editable: true,
				    			layout:new sap.ui.layout.form.GridLayout( {singleColumn: true}),
				    			formContainers: [
				    				new sap.ui.layout.form.FormContainer({
				    					formElements: [
				    						new sap.ui.layout.form.FormElement({
				    							
				    							fields: [airlineRadioBox=new sap.ui.commons.RadioButton({
				    								select : function() {
				    									sap.ui.controller("oui5mvc.inventoryconsole").deleteExsitingTrayPositionDataForMenu();
				    									trayDDBox.setEnabled(false),airlinDropDown.setEnabled(true),typeDropDown.setEnabled(true),unitDropDown.setEnabled(true)
						    									if(noMsgTextView.getVisible()){
						    						    			noMsgTextView.setVisible(false);
						    						    		}
				    									       
				    									if(undefined==typeDropDown.getModel()){
				    										unitDropDown.setEnabled(false);
				    									}else{
				    										unitDropDown.setEnabled(true);
				    									}
				    									orderDropDown.setEnabled(true);
				    									} ,
				    								layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})}).addStyleClass("dialogueradiobutton"),
				    							         new sap.ui.commons.Label({text: oBundle1.getText("label.airline"), layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}).addStyleClass("dialoguelabel"),
				    							         airlinDropDown=new sap.ui.commons.DropdownBox({
				    							        	 change: function(oEvent){
				    							        		var jsonObjToSend = {} ;
				    							 				jsonObjToSend["dialogue"] = "CINT";
				    							 				jsonObjToSend["cid"] = "cart_trolley_types";
				    							 				jsonObjToSend["fcode"] = 7;
				    							 				jsonObjToSend["actions"] = true;
				    							 				var srachmap = {} ;
				    							 				
				    							 				if(isIHSelected)
				    							 					srachmap["airline_code"] ="-";
				    							 				else
				    							 					srachmap["airline_code"] =oEvent.oSource.getSelectedKey();
				    							 				jsonObjToSend["search"] = srachmap ;
				    							 				jsonObjToSend["start"] = 1;
				    							 				jsonObjToSend["nrec"] = 2048;
				    							 				jsonObjToSend["maxrec"] = 2048;
				    							 				var headers = ["cart_trolley_types"] ;
				    							 				jsonObjToSend["header"] = headers ;
				    							 				var orderList = [">cart_trolley_types"] ;
				    							 				jsonObjToSend["order"] = orderList ;
				    											sap.ui.controller("oui5mvc.inventoryconsole").getTypeDialogDropdown(jsonObjToSend);
											        			},enabled:false,
				    							        	 layoutData: new sap.ui.layout.form.GridElementData({hCells: "3"})}).addStyleClass("dialoguedropdown")
				    							]
				    						}),
			                        new sap.ui.layout.form.FormElement({
				    							fields: [
				    							         new sap.ui.commons.RadioButton({layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})}).setVisible(false),
				    							         new sap.ui.commons.Label({text: oBundle1.getText("label.type"), layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}).addStyleClass("dialoguelabel"),
				    									 typeDropDown=new sap.ui.commons.DropdownBox({
				    										 change: function(oEvent){
				    											    var jsonObjToSend = {};
				    												jsonObjToSend["dialogue"] = "CINT"; //need to be confirmed
				    												jsonObjToSend["cid"] = "unit_types";
				    												jsonObjToSend["fcode"] = 7;
				    												jsonObjToSend["actions"] = true;
				    												var searchmap = {};
				    												searchmap["station_name"] =  touchConsole.toUpperCase() ;
				    												searchmap["airline_code"] =airlinDropDown.getSelectedKey();
				    												searchmap["cart_trolley_type"] =oEvent.oSource.getSelectedKey();
				    												jsonObjToSend["search"] = searchmap ;
				    												jsonObjToSend["start"] = 1;
				    												jsonObjToSend["nrec"] = 2048;
				    												jsonObjToSend["maxrec"] = 2048;
				    												var headers = ["unit_type"] ;
				    												jsonObjToSend["header"] = headers ;
				    												var orderList = [">unit_type"] ;
				    												jsonObjToSend["order"] = orderList ;
				    												sap.ui.controller("oui5mvc.inventoryconsole").getUnitTypeDialogDropdown(jsonObjToSend);
											        			},enabled:false,
				    										 layoutData: new sap.ui.layout.form.GridElementData({hCells: "3"})}).addStyleClass("dialoguedropdown")
				    							]
				    						}),
								    new sap.ui.layout.form.FormElement({
				    							
				    							fields: [
				    							         new sap.ui.commons.RadioButton({layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})}).setVisible(false),
				    							         new sap.ui.commons.Label({text: oBundle1.getText("label.unit"), layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}).addStyleClass("dialoguelabel"),
				    							         unitDropDown= new sap.ui.commons.DropdownBox({enabled:false,layoutData: new sap.ui.layout.form.GridElementData({hCells: "3"})}).addStyleClass("dialoguedropdown")
				    							]
				    						}),
													new sap.ui.layout.form.FormElement({
				    							
				    							fields: [trayRbutton=new sap.ui.commons.RadioButton({
				    								select : function() {
				    									    var jsonObjToSend = {};
				    										jsonObjToSend["dialogue"] = "CINT"; //need to be confirmed
				    										jsonObjToSend["cid"] = "carts_on_tray";
				    										jsonObjToSend["fcode"] = 7;
				    										jsonObjToSend["actions"] = true;
				    										jsonObjToSend["start"] = 1;
				    										jsonObjToSend["nrec"] = parseInt(maxRecords);
				    										jsonObjToSend["maxrec"] = parseInt(maxRecords);
				    										var srachmap = {} ;
				    										srachmap["tray_id"] =trayDDBox.getValue();
				    										jsonObjToSend["search"] = srachmap ;
				    										var headers = ["request_id","tray_id", "barcode", "cart_index", "galley_standard", "airline_code","is_inhouse","is_mixed","cart_trolley_type","unit_type","quantity","position","staff_id"] ;
				    										jsonObjToSend["header"] = headers ;
				    										var orderList = [">tray_id",">xposition",">yposition"] ;
				    										jsonObjToSend["order"] = orderList ;
				    										sap.ui.controller("oui5mvc.inventoryconsole").getCartsOnTrayOnDialogForTrayId(jsonObjToSend); 
				    										
				    									airlinDropDown.setEnabled(false),typeDropDown.setEnabled(false),unitDropDown.setEnabled(false),trayDDBox.setEnabled(true),orderDropDown.setEnabled(false)} ,
				    								selected : true,layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})}).addStyleClass("dialogueradiobutton"),
				    							         new sap.ui.commons.Label({text: oBundle1.getText("label.tray.id"),
				    							        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}).addStyleClass("dialoguelabel"),
				    							         trayDDBox=new sap.ui.commons.DropdownBox({
				    							        	 change: function(oEvent){
				    							        		    var jsonObjToSend = {};
						    										jsonObjToSend["dialogue"] = "CINT"; //need to be confirmed
						    										jsonObjToSend["cid"] = "carts_on_tray";
						    										jsonObjToSend["fcode"] = 7;
						    										jsonObjToSend["actions"] = true;
						    										jsonObjToSend["start"] = 1;
						    										jsonObjToSend["nrec"] = parseInt(maxRecords);
						    										jsonObjToSend["maxrec"] = parseInt(maxRecords);
						    										var srachmap = {} ;
						    										srachmap["tray_id"] =trayDDBox.getValue();
						    										jsonObjToSend["search"] = srachmap ;
						    										var headers = ["request_id","tray_id", "barcode", "cart_index", "galley_standard", "airline_code","is_inhouse","is_mixed","cart_trolley_type","unit_type","quantity","position","staff_id"] ;
						    										jsonObjToSend["header"] = headers ;
						    										var orderList = [">tray_id",">xposition",">yposition"] ;
						    										jsonObjToSend["order"] = orderList ;
						    										sap.ui.controller("oui5mvc.inventoryconsole").getCartsOnTrayOnDialogForTrayId(jsonObjToSend); 
				    							        		
											        			},
				    							        	 layoutData: new sap.ui.layout.form.GridElementData({hCells: "3"})}).addStyleClass("dialoguedropdown")
				    							]
				    						}),
												
								   new sap.ui.layout.form.FormElement({
				    							
				    							fields: [allRbutton=new sap.ui.commons.RadioButton({
				    								 select : function() {
				    									 sap.ui.controller("oui5mvc.inventoryconsole").deleteExsitingTrayPositionDataForMenu();
				    									 trayDDBox.setEnabled(false),airlinDropDown.setEnabled(false),typeDropDown.setEnabled(false),unitDropDown.setEnabled(false)
						    									 if(noMsgTextView.getVisible()){
						    							    			noMsgTextView.setVisible(false);
						    							    		}	 
				    									 orderDropDown.setEnabled(false);
				    								 } ,
				    								layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})}).addStyleClass("dialogueradiobutton"),
				    							         new sap.ui.commons.Label({text: oBundle1.getText("label.all.automatic"), layoutData: new sap.ui.layout.form.GridElementData({hCells: "3"})}).addStyleClass("dialoguelabel")
				    									
				    							]
				    						}),
				    						new sap.ui.layout.form.FormElement({
				    							fields: [
				    							         new sap.ui.commons.RadioButton({layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})}).setVisible(false),
				    							         new sap.ui.commons.Label({text:oBundle1.getText("label.orderby"), layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),enabled:false}).addStyleClass("dialoguelabel"),
				    									 orderDropDown=new sap.ui.commons.DropdownBox({enabled:false,
				    											items: [new sap.ui.core.ListItem({text:oBundle1.getText("label.asc"), key:oBundle1.getText("label.asc")}),
				    											        new sap.ui.core.ListItem({text:oBundle1.getText("label.desc"), key:oBundle1.getText("label.desc")})],enabled:false,
				    											        layoutData: new sap.ui.layout.form.GridElementData({hCells: "3"})}).addStyleClass("dialoguedropdown")
				    							]
				    						}),
				    						
				    					]
				    				})
				    			]
				    		});
			        oFirstInventoryDialog.addContent(oSimpleForm);
			        
			        oFirstInventoryDialog.addContent(noMsgTextView = new sap.ui.commons.TextView({width: "100%", textAlign: "Center",visible:false, text: oBundle1.getText("no.trayRequired.tray")}).addStyleClass("noMsgTextView"));
			        
							//set isBusy true to make sure that screen is busy
							isBusy=true;
							oFirstInventoryDialog.addButton(new sap.ui.commons.Button({text:oBundle1.getText("label.start"),tooltip:"Start", icon: "sap-icon://save", style: sap.ui.commons.ButtonStyle.Accept,enabled: true,
								press:function(){
									orderDropDownValue=orderDropDown.getSelectedKey();
									if(airlineRadioBox.getSelected()){
										
										airlineData=airlinDropDown.getSelectedKey();
										cartTrolleyData=typeDropDown.getLiveValue();
										unitData=unitDropDown.getLiveValue();
										
										trayData=-1;
										inventoryheaders = ["airline_code", "cart_trolley_type", "unit_type", "tray_id","order_by"] ;
										inventorydataList = [[airlineData, cartTrolleyData, unitData,trayData,orderDropDownValue]] ;
										
											
									}
									else if(trayRbutton.getSelected()){
									    trayData=trayDDBox.getLiveValue();
										airlineData="";
										cartTrolleyData="";
										unitData="";
										inventoryheaders= ["airline_code", "cart_trolley_type", "unit_type", "tray_id","order_by"] ;
										inventorydataList= [[airlineData, cartTrolleyData, unitData,trayData,""]] ;
									}
									else
									{
										airlineData="*";
										cartTrolleyData="*";
										unitData="*";
										trayData=-1;
										inventoryheaders= ["order_by"] ;
										inventorydataList= [["","","",trayData,""]] ;
									}
								    var jsonObjToSend = {};
									jsonObjToSend["dialogue"] = "CINT"; //need to be confirmed
									jsonObjToSend["cid"] = "inventory_take";
									jsonObjToSend["fcode"] = 9;
									jsonObjToSend["actions"] = true;
									jsonObjToSend["start"] = 1;
									jsonObjToSend["nrec"] = 2048;
									jsonObjToSend["maxrec"] = 2048;
									var srachmap = {} ;
									srachmap["station_name"] =touchConsole.toUpperCase();
									jsonObjToSend["search"] = srachmap ;
									jsonObjToSend["header"] = inventoryheaders ;
									jsonObjToSend["data"] = inventorydataList ;
									sap.ui.controller("oui5mvc.inventoryconsole").startInventoryMode(jsonObjToSend);
									sap.ui.core.BusyIndicator.show(0);
								
								}
							}));
							oFirstInventoryDialog.addButton(new sap.ui.commons.Button({text:oBundle1.getText("label.cancel"),tooltip:oBundle1.getText("label.cancel"), icon: "sap-icon://sys-cancel-2", style: sap.ui.commons.ButtonStyle.Reject,
								press:function(){
									oFirstInventoryDialog.destroyContent();
	                                oFirstInventoryDialog.destroyButtons();
									oFirstInventoryDialog.close();
									
								}
							}));
							oFirstInventoryDialog.attachClosed(function(){
								oFirstInventoryDialog.destroyContent();
								 oFirstInventoryDialog.destroyButtons();
								 oFirstInventoryDialog.close();
							});
							oFirstInventoryDialog.open();
							oFirstInventoryDialog.setInitialFocus(airlinDropDown);
					    var jsonObjToSend = {};
						jsonObjToSend["dialogue"] = "CINT"; //need to be confirmed
						jsonObjToSend["cid"] = "airlines";
						jsonObjToSend["fcode"] = 7;
						jsonObjToSend["actions"] = true;
						var srachmap = {} ;
						srachmap["station_name"] =touchConsole.toUpperCase();
						jsonObjToSend["search"] = srachmap ;
						jsonObjToSend["start"] = 1;
						jsonObjToSend["nrec"] = 2048;
						jsonObjToSend["maxrec"] = 2048;
						var headers = ["airline_codes"] ;
						jsonObjToSend["header"] = headers ;
						var orderList = [">airline_codes"] ;
						jsonObjToSend["order"] = orderList ;
						sap.ui.controller("oui5mvc.inventoryconsole").getAirlineDropdown(jsonObjToSend);
					
				}
				else if(oEvent.getParameter("item").getId() == "TemplateMenuItem")	{
										
					typeOfTempl = "Charge";
					if(consoleType == "DR"){
						typeOfTempl="Discharge";
					}
					oTemplateDialog = new sap.ui.commons.Dialog({keepInWindow: true,modal: true, title:oBundle1.getText("title.template.header",typeOfTempl), width: "80%", height:"80%"}).addStyleClass("templateDialogue");
					var oSimpleForm1 = new sap.ui.layout.form.SimpleForm({
						maxContainerCols: 1,
						content: [	//set radio buttons in group
						          	//oRBG1,
						          	//set tables
						          	oTemplateTable = new sap.ui.table.Table({
						          		visibleRowCount: 4,
										selectionMode: sap.ui.table.SelectionMode.Single, selectionBehavior: "RowOnly",
										navigationMode: sap.ui.table.NavigationMode.Scrollable, showNoData: false,
										layoutData: new sap.ui.layout.GridData({span: "L9 M9 S9", linebreak: true}),
										columns:
										[
											new sap.ui.table.Column({
												label: new sap.ui.commons.Label({tooltip: oBundle1.getText("label.templates"), text: oBundle1.getText("label.templates")}),
												template: new sap.ui.commons.TextView().bindProperty("text", "template_name"),
											}),										
										],
										rowSelectionChange: function(e) {
											 //check any row selected
											 if(oTemplateTable.getSelectedIndex()>-1){
												 sap.ui.getCore().byId("templateCartTable").setVisible(true);
												 oController.getTemplateData();										    	
											 }else{
												 oTemplateCartTable.setModel(new sap.ui.model.json.JSONModel());
												 oTemplateCartTable.getModel().refresh();
												 sap.ui.getCore().byId("templateCartTable").setVisible(false);
												 sap.ui.getCore().byId("AddTemplate").setEnabled(false);
												
											 }										
										},
									}).addStyleClass("customTable").addStyleClass("oRBG1customTable"),	
						 	]
						
					});
					var oSimpleForm2 = new sap.ui.layout.form.SimpleForm({
						maxContainerCols: 1,
						content: [									
									oTemplateCartTable = new sap.ui.table.Table("templateCartTable",{
						          		visible:false,
										selectionMode: sap.ui.table.SelectionMode.None,
										visibleRowCount: 4,
										navigationMode: sap.ui.table.NavigationMode.Scrollable, showNoData: false,
										layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12", linebreak: true}),
										columns:
										[
											new sap.ui.table.Column({
												label: new sap.ui.commons.Label({tooltip: oBundle1.getText("label.airline"), text: oBundle1.getText("label.airline")}),
												template: new sap.ui.commons.TextView().bindProperty("text", "airline_code"),
												width:"14%"
											}),
											new sap.ui.table.Column({
												label: new sap.ui.commons.Label({tooltip: oBundle1.getText("label.ismx"), text: oBundle1.getText("label.ismx")}),
												template: new sap.ui.commons.CheckBox({editable:false}).bindProperty("checked", "is_mixed"),
												width:"10%"
											}),
											new sap.ui.table.Column({
												label: new sap.ui.commons.Label({tooltip: oBundle1.getText("label.flight"), text: oBundle1.getText("label.flight")}),
												template: new sap.ui.commons.TextView().bindProperty("text", "flight"),
												width:"15%"
											}),
											new sap.ui.table.Column({
												label: new sap.ui.commons.Label({tooltip: oBundle1.getText("label.type"), text: oBundle1.getText("label.type")}),
												template: new sap.ui.commons.TextView().bindProperty("text", "cart_trolley_type"),
												width:"19%"
											}),
											new sap.ui.table.Column({
												label: new sap.ui.commons.Label({tooltip: oBundle1.getText("label.unit"), text: oBundle1.getText("label.unit")}),
												template: new sap.ui.commons.TextView().bindProperty("text", "unit_type"),
												width:"19%"
											}),
											new sap.ui.table.Column({
												label: new sap.ui.commons.Label({tooltip: oBundle1.getText("label.gallery"), text: oBundle1.getText("label.gallery")}),
												template: new sap.ui.commons.TextView().bindProperty("text", "galley_standard"),
												width:"15%"
											}),
											new sap.ui.table.Column({
												label: new sap.ui.commons.Label({tooltip: oBundle1.getText("label.qty"), text: oBundle1.getText("label.qty")}),
												template: new sap.ui.commons.TextView().bindProperty("text", "quantity"),
												width:"8%"
											}),
										],
									}).addStyleClass("customTable"),	
										
						 	]
						
					});
					oTemplateDialog.addContent(oSimpleForm1);
					oTemplateDialog.addContent(oSimpleForm2);
					
					oTemplateDialog.addButton(new sap.ui.commons.Button("cancelTemplate",{text: oBundle1.getText("label.cancel"),tooltip: oBundle1.getText("label.cancel"), icon: "sap-icon://sys-cancel-2", style: sap.ui.commons.ButtonStyle.Reject,
						press:function(){						
						oTemplateDialog.destroyButtons();
						oTemplateDialog.destroyContent();
						oTemplateDialog.close();
					}}));
					oTemplateDialog.addButton(new sap.ui.commons.Button("AddTemplate",{text: oBundle1.getText("label.template.add"),enabled:false, tooltip: oBundle1.getText("label.template.add"), icon: "sap-icon://save", style: sap.ui.commons.ButtonStyle.Accept, 
						press:function(){
							oController.handleAddTemplateRequest() ;						
					}}));
					oTemplateDialog.attachClosed(function(){
						oTemplateDialog.destroyButtons();
						oTemplateDialog.destroyContent();
					});
					oTemplateDialog.open();
					oTemplateDialog.setInitialFocus(oTemplateTable);
					oController.prepareTemplateList() ;
				}
				else if(oEvent.getParameter("item").getId() == "englishlang") {
					sessionStorage.setItem("localang", "en-EN")
					window.location.reload();
				}
				else if(oEvent.getParameter("item").getId() == "chineselang") {
					sessionStorage.setItem("localang", "zh_HK")
					window.location.reload();
				} 
				else window.location=oEvent.getParameter("item").getId();
			}
	});
	if(sessionStorage.getItem("localang") === "en-EN" || sessionStorage.getItem("localang") === null) {
		sap.ui.getCore().byId("englishlang").setEnabled(false);
	}else if(sessionStorage.getItem("localang") === "zh_HK") {
		sap.ui.getCore().byId("chineselang").setEnabled(false);
	}
	//Attach the Menu to the MenuButton
	oMenuButton.setMenu(oMenu1);
	
	// Attach the menubar to the page
	oMenuButton.placeAt("navigation");
	
	//if console type is CR/DR-->display charging,discharging,template in the menu
	//if the console type is normal-->display cleaning,inventory in the menu
	//if the console type is CSTA-->display charging,discharging,cleaning,inventory in the menu
	if(consoleType == "CR" || consoleType== "DR"){
		sap.ui.getCore().byId("cleaningconsole").setVisible(false);
		sap.ui.getCore().byId("inventoryconsole").setVisible(false);
		if(consoleType == "CR"){
			oMenuChargingItm.setIcon("sap-icon://accept");
		}else{
			oMenuDischargingItm.setIcon("sap-icon://accept");
		}
	}else if(consoleType == "CCLE"){
		sap.ui.getCore().byId("ChargingMenuItem").setVisible(false);
		sap.ui.getCore().byId("DischargingMenuItem").setVisible(false);
		sap.ui.getCore().byId("TemplateMenuItem").setVisible(false);
		oMenuCleaningItm.setIcon("sap-icon://accept");
		
	}
	else if(consoleType == "CINT"){
		sap.ui.getCore().byId("ChargingMenuItem").setVisible(false);
		sap.ui.getCore().byId("DischargingMenuItem").setVisible(false);
		sap.ui.getCore().byId("TemplateMenuItem").setVisible(false);
		oMenuInventoryItm.setIcon("sap-icon://accept");
	}
	else if(consoleType == "CSTA"){
		sap.ui.getCore().byId("ChargingMenuItem").setVisible(false);
		sap.ui.getCore().byId("DischargingMenuItem").setVisible(false);
		sap.ui.getCore().byId("TemplateMenuItem").setVisible(false);
		if(!oos && "OK" == eqstatus && "A" == mode){
			oMenuInventoryItm.setVisible(true);
			oMenuCleaningItm.setVisible(true);
		}else{
			oMenuInventoryItm.setVisible(false);
			oMenuCleaningItm.setVisible(false);
		}
	}
}

function cleaningtcNavigation(zone,area,console,oAirlineModel,consoleType,touchConsoleRequestType,oController){
	var zoneToBeDisplayed=area;
	zoneToBeDisplayed=zoneToBeDisplayed.replace(/_/g,' ');
	NavigationText=new sap.ui.commons.FormattedTextView("navHeaderSpan",{
		
 		htmlText: "<span class='navZone'>"+zone+"</span><br/>" +
 				"<span class='cleaningnavArea'>&nbsp;&nbsp;&nbsp;"+zoneToBeDisplayed+"</span>" +
 				"<span class='cleaningnavConsole'>"+console+"</span>"
 	}).placeAt("navigation");
	
	 navHeaderText=new sap.ui.commons.FormattedTextView().addStyleClass("navHeaderText");
	navHeaderText.setHtmlText("");
	 airlinesReceived = oAirlineModel.getData()["genericTableModel"];
	if(undefined != airlinesReceived){
		for(i=0;i<airlinesReceived.length;i++){
			if(i%18 == 0){
				navHeaderText.setHtmlText(navHeaderText.getHtmlText()+"<br>");
			}
			if(airlinesReceived[i]["enabled"]){
				navHeaderText.setHtmlText(navHeaderText.getHtmlText()+"&nbsp;&nbsp;&nbsp;"+"<span class='navZoneHeader'>"+airlinesReceived[i]["value"]+"</span>");
			}else{
				navHeaderText.setHtmlText(navHeaderText.getHtmlText()+"&nbsp;&nbsp;&nbsp;"+"<span class='navZoneHeaderDisabled'+>"+airlinesReceived[i]["value"]+"</span>");
			}			
		}
	}
	
	/*for(i=0;i<airlinesarray1.length;i++){
		if(airlinesarray1[i]!="-"){
			navHeaderText.setHtmlText(navHeaderText.getHtmlText()+" "+"<span class='navZoneHeader'>"+airlinesarray1[i]+" "+"</span>");
		}
	}*/
	navHeaderText.placeAt("navigation");	
	//new sap.ui.commons.Label().addStyleClass("headerMenuButton").placeAt("navigation");
	prepareMenu(consoleType,touchConsoleRequestType,oController);
	//add cleaning critical icon
	cleaningCriticalBtn = new sap.ui.commons.Button("cleaningCriticalBtn",{
		icon : "../images/cleaning_icon.png",
		visible:false
	});
	
	cleaningCriticalBtn.placeAt("navigation");
}

function setHtmlTextForSelectedValues(){
	selectedUnitQtyText="";
	typeSelected="";
	galleyStdSelected="";
	enableButtons = false;
	if(oTypesTable.getSelectedIndex()>-1){
		typeSelected=oTypesTable.getContextByIndex(oTypesTable.getSelectedIndex()).getProperty("cart_trolley_type");
		selectedCategory = oTypesTable.getContextByIndex(oTypesTable.getSelectedIndex()).getProperty("category");
	}
	for (var i in selectedUnitQty){
		if(selectedCategory=="LOWERATOR" && !isIHSelected){
			if(selectedUnitQty[i] ==1){
				selectedUnitQtyText=selectedUnitQtyText+i+" Full, ";				
			}else if(selectedUnitQty[i]==2){
				selectedUnitQtyText=selectedUnitQtyText+i+" Half, ";
			}			
		}else{
			selectedUnitQtyText=selectedUnitQtyText+i+" "+selectedUnitQty[i]+", ";
		}		
	}
	if(selectedUnitQtyText.length > 1){
		selectedUnitQtyText = selectedUnitQtyText.substring(0,selectedUnitQtyText.length-2);
		enableButtons=true;
	}else if(oUnitsTable.getSelectedIndex()>-1){
		selectedUnitQtyText=oUnitsTable.getModel().oData["genericTableModel"][oUnitsTable.getSelectedIndex()].unit_type;
	}
	
	if(isMxSelected || isIHSelected){
		if(isMxSelected){
			flightSelected="";
			if("DR" == typeOfReq && oFlightsTable.getSelectedIndex()>-1){
				flightSelected= oFlightsTable.getContextByIndex(oFlightsTable.getSelectedIndex()).getProperty("flight_no");
				if(isMxAllBtnPressed){
					typeSelected="CART";
					allUnits = oFlightsTable.getContextByIndex(oFlightsTable.getSelectedIndex()).getProperty("unit_type");
					allQuantity = oFlightsTable.getContextByIndex(oFlightsTable.getSelectedIndex()).getProperty("quantity");
					if(undefined != allUnits){
						selectedUnitQtyText="";
						for(var unitPos=0;unitPos<allUnits.length;unitPos++){
							selectedUnitQtyText=selectedUnitQtyText+allUnits[unitPos]+" "+allQuantity[unitPos]+", ";
						}
						if(selectedUnitQtyText.length > 1){
							selectedUnitQtyText = selectedUnitQtyText.substring(0,selectedUnitQtyText.length-2);
						}
					}else{
						selectedUnitQtyText=oBundle1.getText("label.all.units");
					}
					
					enableButtons = true;
				}
			}
			chargingTitleFormmatedView.setHtmlText("<span class='selectedValues' id='flightDetails'>"+airlineSelected+" "+flightSelected+" MIXED "+typeSelected+"</span><br/>" +				
					"<span class='selectedUnits' id='unitValues'>"+selectedUnitQtyText+"</span>");
		}else{
			chargingTitleFormmatedView.setHtmlText("<span class='selectedValues' id='flightDetails'>IN-HOUSE "+typeSelected+"</span><br/>" +				
					"<span class='selectedUnits' id='unitValues'>"+selectedUnitQtyText+"</span>");
		}		
	}else{
		if(oGSSegmentedButton.getSelectedButton()!=null){
			galleyStdSelected=oGSSegmentedButton.getSelectedButton();
		}
		chargingTitleFormmatedView.setHtmlText("<span class='selectedValues' id='flightDetails'>"+airlineSelected+" "+typeSelected+" "+galleyStdSelected+"</span><br/>" +				
				"<span class='selectedUnits' id='unitValues'>"+selectedUnitQtyText+"</span>");
	}	
	
	 if(addButton.getVisible()){
		 addButton.setEnabled(enableButtons);
		 if(enableButtons){
			 addButton.focus();
		 }
	 }else if(updateButton.getVisible()){
		 updateButton.setEnabled(enableButtons);
		 if(enableButtons){
			 updateButton.focus();
		 }
	 }else if(staffId_update.getVisible()){
		 staffId_update.setEnabled(enableButtons);
		 /*if(enableButtons){
			 staffId_update.focus();
		 }*/
	 }
}

//Initialize the Dataset and the layouts
function createTemplateGeneric(isHeaderRequired,applyClass){
	
	var c = sap.ui.commons;
	return new HorizontalLayout({
		form: new c.form.Form({
			layout: new c.form.GridLayout(),
			formContainers: [
				new c.form.FormContainer({
					formElements: [					   
						new c.form.FormElement({
							fields: [b1=new c.Button({text:"{textQty}",value: "{value}",icon:"{icon}", editable: false,
								layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12", linebreak: false}),
								press:function(event){
								var selectedQty=this.getBindingContext().getProperty("value");
								if(oUnitsTable.getSelectedIndex()>-1){

									var qtyAvailable=oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["qty"];								
									//clear data
									if(selectedQty == "11"){
										qtyAvailable="";										
									}else if(selectedQty == "12"){
										//clear last digit
										if(qtyAvailable.length>0){
											qtyAvailable=qtyAvailable.slice(0,-1);
										}else{
											qtyAvailable="";
										}
										
									}else{
										//allow user to enter less than equal to max quantity accepted
										totalQtyEnetred=parseInt(qtyAvailable+""+selectedQty);
										totalQty=0;
										for (var i in selectedUnitQty){
											if(i != oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"]){
												totalQty=totalQty+parseInt(selectedUnitQty[i]);
											}
										}
										totalQty=parseInt(totalQty+totalQtyEnetred);
										//Use max_request_qty if type is not transt trolley 
										if(oTypesTable.getContextByIndex(oTypesTable.getSelectedIndex()).getProperty("category").toUpperCase().indexOf("TROLLEY") > -1){
											reqMaxQty=max_request_qty;
											ttMaxQty=max_transit_trolley_qty;
											maxQtyForTT=-1;
											if(reqMaxQty<ttMaxQty){
												maxQtyForTT=reqMaxQty;
											}else{
												maxQtyForTT=ttMaxQty;
											}
											previousMaxQty=maxQtyAllowed;
											if((maxQtyAllowed>-1 && maxQtyAllowed>maxQtyForTT)||(maxQtyAllowed == -1)){
												maxQtyAllowed=maxQtyForTT;
											}
										}else{
											maxQtyAllowed=max_request_qty;
										}
										if(totalQty>parseInt(maxQtyAllowed)){
											//updated to display entered quantity is more than max allowed qty
									    	jQuery.sap.require("sap.m.MessageToast");
									    	sap.m.MessageToast.show(oBundle1.getText("msg.entered.qty",totalQty)+" "+
									    			oBundle1.getText("msg.check.max.qty",maxQtyAllowed), {
									    	    duration: parseInt(toastTime),                 
									    	    at: "center bottom",
									    	    offset:"0 -60"
									    	   
									    	});	
									    	maxQtyAllowed=previousMaxQty;
										}else{
											if(totalQtyEnetred !=0){
												qtyAvailable=qtyAvailable+""+selectedQty;
											}
										}
										
									}
									if(undefined == qtyAvailable){
										qtyAvailable="";										
									}
									qtySelected=qtyAvailable;									
									//set values to selected unit and qty map
									if(qtyAvailable == ""){										
										delete selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()];
									}else{	
										delete selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()];											
										selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()] = qtyAvailable;
									}
									
									oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["qty"]=qtyAvailable;	
									oUnitsTable.getModel().refresh();
								}else{
									
										jQuery.sap.require("sap.ui.commons.MessageBox");
										sap.ui.commons.MessageBox.show(oBundle1.getText("msg.selecct.unit"),sap.ui.commons.MessageBox.Icon.ERROR);
								
								}	
								 if(!jQuery.isEmptyObject(selectedUnitQty)){
									 if(staffId_update.getVisible()){
										 staffId_update.setEnabled(true);
										 //staffId_update.focus();
									 }else{
										 sap.ui.getCore().byId("addSave").setEnabled(true);
									 }
									
								 }else{
									 if(staffId_update.getVisible()){
										 staffId_update.setEnabled(false);
									 }else{
										 sap.ui.getCore().byId("addSave").setEnabled(false);
									 } 
								 }
							}}).addStyleClass(applyClass)]
							,layoutData: new sap.ui.layout.GridData({span: "L4 M4 S4", linebreak: false})
						}),
					]
				})
			]
		})
	});
}



//Initialize the Dataset and the layouts
function createTemplate(isHeaderRequired,applyClass){
	var c = sap.ui.commons;
	return new HorizontalLayout({
		form: new c.form.Form({
			layout: new c.form.GridLayout(),
			formContainers: [
				new c.form.FormContainer({
					formElements: [					   
						new c.form.FormElement({
							fields: [b1=new c.Button({text:"{textQty}",value: "{value}",icon:"{icon}", editable: false,
								layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12", linebreak: false}),
								press:function(event){
								var selectedQty=this.getBindingContext().getProperty("value");
								if(((isIHSelected|| isMxSelected) && oUnitsTable.getSelectedIndex()>-1) || oGSSegmentedButton.getSelectedButton()!= null){
									var qtyAvailable=oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["qty"];
									//clear data
									if(selectedQty == "11"){
										qtyAvailable="";										
									}else if(selectedQty == "12"){
										//clear last digit
										if(qtyAvailable.length>0){
											qtyAvailable=qtyAvailable.slice(0,-1);
										}else{
											qtyAvailable="";
										}
										
									}else{
										//allow user to enter less than equal to max quantity accepted
										totalQtyEnetred=parseInt(qtyAvailable+""+selectedQty);
										totalQty=0;
										for (var i in selectedUnitQty){
											if(i != oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"]){
												totalQty=totalQty+parseInt(selectedUnitQty[i]);
											}
										}
										totalQty=parseInt(totalQty+totalQtyEnetred);
										//Use max_request_qty if type is not transt trolley 
										if(oTypesTable.getContextByIndex(oTypesTable.getSelectedIndex()).getProperty("category").toUpperCase().indexOf("TROLLEY") > -1){
											reqMaxQty=max_request_qty;
											ttMaxQty=max_transit_trolley_qty;
											maxQtyForTT=-1;
											if(reqMaxQty<ttMaxQty){
												maxQtyForTT=reqMaxQty;
											}else{
												maxQtyForTT=ttMaxQty;
											}
											previousMaxQty=maxQtyAllowed;
											if((maxQtyAllowed>-1 && maxQtyAllowed>maxQtyForTT)||(maxQtyAllowed == -1)){
												maxQtyAllowed=maxQtyForTT;
											}
										}else{
											maxQtyAllowed=max_request_qty;
										}
										if(totalQty>parseInt(maxQtyAllowed)){
											
											//updated to display entered quantity is more than max allowed qty
									    	jQuery.sap.require("sap.m.MessageToast");
									    	sap.m.MessageToast.show(oBundle1.getText("msg.entered.qty",totalQty)+" "+
									    			oBundle1.getText("msg.check.max.qty",maxQtyAllowed), {
									    	    duration: parseInt(toastTime),                 
									    	    at: "center bottom",
									    	    offset:"0 -60"
									    	   
									    	});	
									    	maxQtyAllowed=previousMaxQty;
										}else{
											if(totalQtyEnetred !=0){
												qtyAvailable=qtyAvailable+""+selectedQty;
											}
										}
										
									}
									if(undefined == qtyAvailable){
										qtyAvailable="";										
									}
									qtySelected=qtyAvailable;									
									//set values to selected unit and qty map
									if(qtyAvailable == ""){										
										delete selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()];
									}else{	
										delete selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()];											
										selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()] = qtyAvailable;
									}
									if(isHeaderRequired){
										
										setHtmlTextForSelectedValues();
									}
									oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["qty"]=qtyAvailable;	
									oUnitsTable.getModel().refresh();
									
								}else{
									checkMandatoryFields();
								}								
							}}).addStyleClass(applyClass)]
							,layoutData: new sap.ui.layout.GridData({span: "L4 M4 S4", linebreak: false})
						}),
					]
				})
			]
		})
	});
}

//Initialize the Dataset and the layouts
function createTemplateLoweratorQty(isHeaderRequired,applyClass){
	var c = sap.ui.commons;
	return new HorizontalLayout({
		form: new c.form.Form({
			layout: new c.form.GridLayout(),
			formContainers: [
				new c.form.FormContainer({
					formElements: [					   
						new c.form.FormElement({
							fields: [b1=new c.Button({text:"{textQty}",value: "{value}",icon:"{icon}", editable: false,
								layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12", linebreak: false}),
									press:function(event){															
									var selectedQty=this.getBindingContext().getProperty("value");
									var selectedQtyTxt=this.getBindingContext().getProperty("textQty");
									jQuery.sap.require("sap.m.MessageToast");
									if(((isIHSelected|| isMxSelected) && oUnitsTable.getSelectedIndex()>-1) || oGSSegmentedButton.getSelectedButton()!= null){
										var qtyAvailable="";
										var existingQty=oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["qty"];	
										if(qtyChkMap['Half'] == 2){
											sap.m.MessageToast.show(oBundle1.getText("lbl.lowerator.full"), {
									    	    duration: parseInt(toastTime),                 
									    	    at: "center bottom",
									    	    offset:"0 -60"
									    	   
									    	});
										}else{
											if(oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["qty"]!="")
											{
												qtyAvailable=selectedQty;
												
												if(undefined == qtyAvailable){
													qtyAvailable="";
												}
												qtySelected=qtyAvailable;
												//already some qty available in the selected place.so need to be removed
										
												if(existingQty=='Full'){
													qtyChkMap['Full']=0;
												}
												if(existingQty=='Half'){
													var halfQty=qtyChkMap['Half'];
													if(halfQty==2){
														qtyChkMap['Half']=1;
													}else{
														qtyChkMap['Half']=0;
													}	
												}
												
												//further to check present added full or not.In this case it is possible to add half tray only
												if(qtyChkMap['Half']==1 && selectedQtyTxt=='Full'){
													sap.m.MessageToast.show(oBundle1.getText("lbl.add.half.tray"), {
											    	    duration: parseInt(toastTime),                 
											    	    at: "center bottom",
											    	    offset:"0 -60"
											    	   
											    	});
												}else{
													//eligible for insert
													//set values to selected unit and qty map
													if(qtyAvailable == ""){
														delete selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()];
													}else{
														if(qtyAvailable=='Full'){
															selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()] = 1;
														}else if(qtyAvailable=='Half'){
															selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()] = 2;
														}													
													}	
													if(isHeaderRequired){												
														setHtmlTextForSelectedValues();
													}
													oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["qty"]=qtyAvailable;
													oUnitsTable.getModel().refresh();
													if(qtyAvailable=='Full'){
															qtyChkMap['Full']=1;
													}
													if(qtyAvailable=='Half'){
															var halfQty=qtyChkMap['Half'];
															if(halfQty==1)
																qtyChkMap['Half']=2;
															else
																qtyChkMap['Half']=1;
													}
														
												}
											}else{										
												//qty not exist. newly added .now to check the map it is already full or not.
												qtyAvailable=selectedQty;
												
												if(undefined == qtyAvailable){
													qtyAvailable="";
												}
												qtySelected=qtyAvailable;
												
												if(qtyChkMap['Full']==1){
													//updated to display entered quantity is more than max allowed qty
											    	sap.m.MessageToast.show(oBundle1.getText("lbl.lowerator.full"), {
											    	    duration: parseInt(toastTime),                 
											    	    at: "center bottom",
											    	    offset:"0 -60"
											    	   
											    	});
													
												 }else if(qtyChkMap['Half']!=''|| undefined!=qtyChkMap['Half']){										
													if(parseInt(qtyChkMap['Half'])==2){
														jQuery.sap.require("sap.m.MessageToast");
												    	sap.m.MessageToast.show(oBundle1.getText("lbl.lowerator.full"), {
												    	    duration: parseInt(toastTime),                 
												    	    at: "center bottom",
												    	    offset:"0 -60"
												    	   
												    	});
													}else if(parseInt(qtyChkMap['Half'])==1 &&selectedQtyTxt=='Full'){
														sap.m.MessageToast.show(oBundle1.getText("lbl.add.half.tray"), {
												    	    duration: parseInt(toastTime),                 
												    	    at: "center bottom",
												    	    offset:"0 -60"
												    	   
												    	});
													}else{
														qtyAvailable=selectedQty;
														
														if(undefined == qtyAvailable){
															qtyAvailable="";
														}
														qtySelected=qtyAvailable;
														
														if(qtyAvailable=='Full'){
																qtyChkMap['Full']=1;
														}
														if(qtyAvailable=='Half'){
															var halfQty=qtyChkMap['Half'];
															if(halfQty==1)
																qtyChkMap['Half']=2;
															else
																qtyChkMap['Half']=1;
														}
														//set values to selected unit and qty map
														if(qtyAvailable == ""){
															delete selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()];
														}else{
															if(qtyAvailable=='Full'){
																selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()] = 1;
															}else if(qtyAvailable=='Half'){
																selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()] = 2;
															}
														}
														if(isHeaderRequired){														
															setHtmlTextForSelectedValues();
														}
														
														oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["qty"]=qtyAvailable;	
														oUnitsTable.getModel().refresh();
													 }								 											
												}else{
													 //Final else block.
													 qtyAvailable=selectedQty;
														
														if(undefined == qtyAvailable){
															qtyAvailable="";
														}
														qtySelected=qtyAvailable;												
														if(qtyAvailable=='Full'){
																qtyChkMap['Full']=1;
														}
														if(qtyAvailable=='Half'){
															var halfQty=qtyChkMap['Half'];
															if(halfQty==1)
																qtyChkMap['Half']=2;
															else
																qtyChkMap['Half']=1;
														}
														//set values to selected unit and qty map
														if(qtyAvailable == ""){
															
															delete selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()];
														}else{
															if(qtyAvailable=='Full'){
																selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()] = 1;
															}else if(qtyAvailable=='Half'){
																selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()] = 2;
															}
														}	
														if(isHeaderRequired){
															
															setHtmlTextForSelectedValues();
														}
														oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["qty"]=qtyAvailable;	
														oUnitsTable.getModel().refresh();												
													 }
												}
										}
									
									
									}else{
										checkMandatoryFields();
									}
									if(!isHeaderRequired){
										if(!jQuery.isEmptyObject(selectedUnitQty)){
											 if(staffId_update.getVisible()){
												 staffId_update.setEnabled(true);
												 //staffId_update.focus();
											 }else{
												 sap.ui.getCore().byId("addSave").setEnabled(true);
											 }
											
										 }else{
											 if(staffId_update.getVisible()){
												 staffId_update.setEnabled(false);
											 }else{
												 sap.ui.getCore().byId("addSave").setEnabled(false);
											 } 
										 }
									}
								}}).addStyleClass(applyClass)]
							,layoutData: new sap.ui.layout.GridData({span: "L4 M4 S4", linebreak: false})
						}),
					]
				})
			]
		})
	});
}
//Initialize the Dataset and the layouts
function createTemplateForAirlines(isHeaderRequired,applyClass,oController){
	var c = sap.ui.commons;
	return new HorizontalLayout({
		form: new c.form.Form({
			layout: new c.form.GridLayout(),
			formContainers: [
				new c.form.FormContainer({
					formElements: [					   
						new c.form.FormElement({
							fields: [b1=new c.Button({text:"{value}",value: "{value}", editable: false,enabled:"{enabled}",
								layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12", linebreak: false}),
								press:function(event){
									if(this.getBindingContext().getProperty("enabled")){
										airlineSelected=this.getBindingContext().getProperty("value");
										airlineIdx = this.getBindingContext().getProperty("idx");									
										if(isHeaderRequired){
											if(inHouseToggleButton.getPressed()){
												inHouseToggleButton.setPressed(false);
												isIHSelected=false;
												sap.ui.getCore().byId("GSSegmentedButton").setVisible(true);
												sap.ui.getCore().byId("mx").setEnabled(true);
											}										
										}else{										
											sap.ui.getCore().byId("GSSegmentedButton").setVisible(true);
											galleyStdSelected="";
										/*	sap.ui.getCore().byId("QtyDataSet").setVisible(true);
											sap.ui.getCore().byId("QtyLabel").setVisible(true);
										*/	
											isIHSelected=false;
											sap.ui.getCore().byId("stationinHouse").removeStyleClass("availableAirlines-inhouseBG");
								    		sap.ui.getCore().byId("stationinHouse").addStyleClass("availableAirlines-inhouse");									
										}
										oAirlineDataSet.setLeadSelection(airlineIdx);
									}
								}}).addStyleClass(applyClass)]
							,layoutData: new sap.ui.layout.GridData({span: "L3 M3 S3", linebreak: false})
						}),
					]
				})
			]
		})
	});
}

//Station Console Add and Update Carts on tray 
function cartsOnTrayAddAndUpdate(action,oController,dialogeName){
	oAddDialog = new sap.ui.commons.Dialog({title: action+" "+"Request", keepInWindow: true, modal: true, width: "80%", height: "90%"}).addStyleClass("touchScreenDialog");
	
	//Begin
	
	// to hold data for Cart/Lowerator/Transit Trolley unit details
	var typeIndex = 0;
	var oUnitListBox;
	var clickCondition="false";
	var oLayout1 = new sap.ui.layout.form.ResponsiveGridLayout();
	var oLayout2 = new sap.ui.layout.form.ResponsiveGridLayout();
	var oForm1 = new sap.ui.layout.form.Form("F1", {
		maxContainerCols: 7,
		layout: oLayout1,
		formContainers:
		[
		new sap.ui.layout.form.FormContainer("F1C8",
				{
					formElements: 
					[
					 	new sap.ui.layout.form.FormElement
					 	("F1C8E1",{
					 		fields:
					 		[
					 		 	new sap.ui.commons.Label({text:oBundle1.getText("lbl.scan.cart.position"),layoutData:new  sap.ui.layout.GridData({span: "L7 M7 S7", linebreak: false})}).addStyleClass("airlineHeaderOfSegBtns"),
					 		 	barCode_MX=new sap.ui.commons.TextField("barCodeMX",{value: "",placeholder:oBundle1.getText("lbl.scan.item"),layoutData:new  sap.ui.layout.GridData({linebreak: false}),visible:false,
									change:function(e){
										isMxSelected=false;
										 barCodeMXSelected="";
							    		if(isIHSelected){
								    		inHouseToggleButton.setPressed(false);
								    		inHouseToggleButton.firePress();
								    	}
							    		oAirlineDataSet.setLeadSelection(-1);
								    	if(!barCode_MX.getLiveValue()){
								    		if(staffId_update.getVisible()){
												 staffId_update.setEnabled(false);
											 }else{
												 sap.ui.getCore().byId("addSave").setEnabled(false);
											 }
								    		 isMxSelected=false;
								    		 barCodeMXSelected="";
											 oTypesTable.removeStyleClass("airlines_ds_disable");
											 oUnitsTable.removeStyleClass("airlines_ds_disable");
											 sap.ui.getCore().byId("airlines_ds").removeStyleClass("airlines_ds_disable");
											 sap.ui.getCore().byId("QtyDataSet").removeStyleClass("disable_dataset");
								    		 sap.ui.getCore().byId("TQtyDataSet").removeStyleClass("disable_dataset");
								    		 sap.ui.getCore().byId("QtyDataSet").setVisible(true);
								    		 sap.ui.getCore().byId("TQtyDataSet").setVisible(false);
								    		 sap.ui.getCore().byId("clearBut").setVisible(false);
								    		 oGSSegmentedButton.setEnabled(false);
								    		 sap.ui.getCore().byId("clearBut").setEnabled(false);
								    		 inHouseToggleButton.setEnabled(true);
								    	}else{
								    		mx_Cart_Info.setText("");
								    		isMxSelected=true;
								    		 barCodeMXSelected=barCode_MX.getLiveValue();
											oTypesTable.addStyleClass("airlines_ds_disable");
											oUnitsTable.addStyleClass("airlines_ds_disable");
											 sap.ui.getCore().byId("airlines_ds").addStyleClass("airlines_ds_disable");
											 sap.ui.getCore().byId("QtyDataSet").addStyleClass("disable_dataset");
								    		 sap.ui.getCore().byId("TQtyDataSet").addStyleClass("disable_dataset");
								    		 sap.ui.getCore().byId("QtyDataSet").setVisible(true);
								    		 sap.ui.getCore().byId("TQtyDataSet").setVisible(false);
								    		 sap.ui.getCore().byId("clearBut").setVisible(false);
								    		 image.setVisible(false);
								    		oGSSegmentedButton.setEnabled(false);
								    		sap.ui.getCore().byId("clearBut").setEnabled(false);
								    		inHouseToggleButton.setEnabled(false);
								    	
								    		//Get call for carts_on_tray
										    
										
								    		if(staffId_update.getVisible()){
												 staffId_update.setEnabled(true);
												 //staffId_update.focus();
											 }else{
												 sap.ui.getCore().byId("addSave").setEnabled(true);
											 }
								    		
								    		var jsonObjToSend = {};
											jsonObjToSend["dialogue"] =dialogeName; //need to be confirmed
											jsonObjToSend["cid"] = "carts_on_tray";
											jsonObjToSend["fcode"] = 7;
											jsonObjToSend["actions"] = true;
											jsonObjToSend["start"] = 1;
											jsonObjToSend["nrec"] = parseInt(maxRecords);
											jsonObjToSend["maxrec"] = parseInt(maxRecords);
											var srachmap = {} ;
											//srachmap["tray_id"] =traySelected;
											srachmap["barcode"] =barCodeMXSelected;
											jsonObjToSend["search"] = srachmap ;
											var headers = ["cart_index", "galley_standard", "airline_code", "is_inhouse", "is_mixed","is_scanned", "category","cart_trolley_type", "unit_type", "compartment", "material_group", "quantity", "xposition", "yposition", "flight_no", "staff_id", "height", "width"] ;
											jsonObjToSend["header"] = headers ;
											var orderList = ["cart_index", "galley_standard", "airline_code", "is_inhouse", "is_mixed","is_scanned", "category","cart_trolley_type", "unit_type", "compartment", "material_group", "quantity", "xposition", "yposition", "flight_no", "staff_id", "height", "width"] ;
											jsonObjToSend["order"] = orderList ;
																						
											oController.getBarCodeDetails(jsonObjToSend);
								    	}
									}
	
					 		 	}).addStyleClass("scannedItem"),
					 		 	
					 		    mx_Cart_view=new sap.ui.commons.TextView({text:oBundle1.getText("lbl.scan.item"),layoutData:new  sap.ui.layout.GridData({span: "L3 M3 S3", linebreak: false})}).addStyleClass("scannedItem"),
					 		    mx_Cart=new sap.ui.commons.TextView({text: "",layoutData:new  sap.ui.layout.GridData({span: "L7 M7 S7", linebreak: false})}).addStyleClass("scannedItemName"),
					 		    mx_Cart_Info=new sap.ui.commons.TextView({text: "",layoutData:new  sap.ui.layout.GridData({span: "L5 M5 S5", linebreak: false})}).addStyleClass("scannedItemName"),
					 		 	new sap.ui.commons.TextView({text: oBundle1.getText("lbl.or"),layoutData:new  sap.ui.layout.GridData({span: "L12 M12 S12", linebreak: true})}).addStyleClass("headerOfMXR"),
					 		 	new sap.ui.commons.HorizontalDivider({height: sap.ui.commons.HorizontalDividerHeight.Small, layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12",linebreak: true, margin: false})})
							],
					 	}),
					 ],
				layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12", linebreak: true})
				}),
		 
		 
			new sap.ui.layout.form.FormContainer("F1C3",
			{
				formElements: 
				[
				 	new sap.ui.layout.form.FormElement
				 	("F1C3E1",{
				 		fields:
				 		[
				 		 	new sap.ui.commons.Label("airlines",{text: oBundle1.getText("label.airline"),layoutData:new  sap.ui.layout.GridData({span: "L12 M12 S12", linebreak: true})}).addStyleClass("airlineHeaderOfSegBtns"),
				 		 		
							 oAirlineDataSet = new sap.ui.ux3.DataSet("airlines_ds",{showToolbar:false,showFilter:false,showSearchField:false,
								layoutData:new  sap.ui.layout.GridData({span: "L12 M12 S12", linebreak: true}),
								items: {
									path: "/genericTableModel",
									template: new sap.ui.ux3.DataSetItem()
								},
								views: [
									new sap.ui.ux3.DataSetSimpleView({
										name: "Floating, non-responsive View",
										floating: true,
										responsive: true,
										itemMinWidth: 62,
										template: createTemplateForAirlines(false,"availableAirlines-csta",oController)
									}),
								],
								selectionChanged: function search(oEvent) {
									airlineIdx = oEvent.getParameter("newLeadSelectedIndex");
									if(!isAirlineChanged){
										isAirlineChanged = true;
									}else{
										if(undefined != airlineIdx && airlineIdx!=-1){
											if(oAirlineDataSet.getModel().getData()["genericTableModel"][airlineIdx]["enabled"]){
												oTypesTable.setModel(new sap.ui.model.json.JSONModel());
										    	oTypesTable.setSelectedIndex(-1);
												oUnitsTable.setModel(new sap.ui.model.json.JSONModel());
												oUnitsTable.setSelectedIndex(-1);
												isIHSelected=false;
												inHouseToggleButton.setPressed(false);
												image.setVisible(false);
												airlineSelected=oAirlineDataSet.getModel().getData()["genericTableModel"][airlineIdx]["value"];	
												selectedUnitQty ={};
												oController.prepareTypesData();	
												isAirlineChanged = true;
												previousAirlineIndexSelected = airlineIdx;
												if(jQuery.isEmptyObject(selectedUnitQty) && !isMxSelected){
													 if(staffId_update.getVisible()){
														 staffId_update.setEnabled(false);
													 }else{
														 sap.ui.getCore().byId("addSave").setEnabled(false);
													 }
													
												 }
											}else{
												isAirlineChanged = false;
												if(previousAirlineIndexSelected != -1){
													oAirlineDataSet.setLeadSelection(previousAirlineIndexSelected);
												}else{
													oAirlineDataSet.setLeadSelection(-1);
												}
											}
										}else{
											previousAirlineIndexSelected = -1;
											airlineSelected="";
											oAirlineDataSet.setLeadSelection(-1);
											image.setVisible(false);
											oTypesTable.setModel(new sap.ui.model.json.JSONModel());
									    	oTypesTable.setSelectedIndex(-1);
											oUnitsTable.setModel(new sap.ui.model.json.JSONModel());
											oUnitsTable.setSelectedIndex(-1);
											if(jQuery.isEmptyObject(selectedUnitQty) && !isMxSelected){
												 if(staffId_update.getVisible()){
													 staffId_update.setEnabled(false);
												 }else{
													 sap.ui.getCore().byId("addSave").setEnabled(false);
												 }
												
											 }
										}	
									}
														
									
								}
							}),
							
								inHouseToggleButton= new sap.ui.commons.ToggleButton("stationinHouse",{text:oBundle1.getText("lbl.in.house"),pressed : false,layoutData:new  sap.ui.layout.GridData({span: "L12 M12 S12", linebreak: true}),	
								press: function(){
									if(this.getEnabled()){
										typeSelected="IN-HOUSE";
										oAirlineDataSet.setLeadSelection(-1);
										oGSSegmentedButton.setSelectedButton(null);
										image.setVisible(false);
									  	sap.ui.getCore().byId('ATLAS').setEnabled(false);
										sap.ui.getCore().byId('KSSU').setEnabled(false);
										galleyStdSelected="-";
										
										if(inHouseToggleButton.getPressed()){
											inHouseToggleButton.setPressed(true);
											isIHSelected=true;	
											oAirlineDataSet.setLeadSelection(-1);
											airlineSelected="-";
											selectedUnitQty={};
											if(jQuery.isEmptyObject(selectedUnitQty) && !isMxSelected){
												 if(staffId_update.getVisible()){
													 staffId_update.setEnabled(false);
												 }else{
													 sap.ui.getCore().byId("addSave").setEnabled(false);
												 }
												
											 }
											sap.ui.core.BusyIndicator.show();
											//get Types
											oController.prepareTypesData();
											sap.ui.getCore().byId("stationinHouse").removeStyleClass("availableAirlines-inhouse");
								    		sap.ui.getCore().byId("stationinHouse").addStyleClass("availableAirlines-inhouseBG");
										}else{
											isIHSelected=false;
											sap.ui.getCore().byId("stationinHouse").removeStyleClass("availableAirlines-inhouseBG");
								    		sap.ui.getCore().byId("stationinHouse").addStyleClass("availableAirlines-inhouse");
								    		selectedUnitQty={};
											if(jQuery.isEmptyObject(selectedUnitQty) && !isMxSelected){
												 if(staffId_update.getVisible()){
													 staffId_update.setEnabled(false);
												 }else{
													 sap.ui.getCore().byId("addSave").setEnabled(false);
												 }
												
											 }
											if(oAirlineDataSet.getModel().getData()["genericTableModel"].length == 1 && isAtleastOneAirlineEnabled){
												oAirlineDataSet.setLeadSelection(0);
											}else{
												airlineSelected="";
												oTypesTable.setModel(new sap.ui.model.json.JSONModel());
										    	oTypesTable.setSelectedIndex(-1);
												oUnitsTable.setModel(new sap.ui.model.json.JSONModel());
												oUnitsTable.setSelectedIndex(-1);
												oAirlineDataSet.setLeadSelection(-1);
											}	
											
										}			
									}else{
										this.setPressed(false);
									}
								}
							
							}).addStyleClass("availableAirlines-inhouse")	 		 	
				 		 	],
				 	}),
				 ],
			layoutData: new sap.ui.layout.GridData({span: "L4 M4 S4", linebreak: false})
			}),
			new sap.ui.layout.form.FormContainer("F1C5",
							{
								formElements: 
								[
								 	new sap.ui.layout.form.FormElement
								 	("F1C5E1",{
								 		fields:
								 		[
								 		 	new sap.ui.commons.Label({text: oBundle1.getText("label.details"), textAlign: "Center", 
								 		 		layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12"})}).addStyleClass("detailsLabel"),
								 		 		oTypesTable = new sap.ui.table.Table("TypesTable", {
								 		 			visibleRowCount:5, selectionMode: sap.ui.table.SelectionMode.Single,
													selectionBehavior: sap.ui.table.SelectionBehavior.RowOnly,
													navigationMode: sap.ui.table.NavigationMode.None, 
													showNoData: false,enabled:true,selectedIndex: -1,
													rowHeight:55,
													path: "/genericTableModel",
													layoutData: new sap.ui.layout.GridData({span: "L6 M6 S6", linebreak: false}),
													columns:
													[
														new sap.ui.table.Column({
															label: new sap.ui.commons.Label({text: oBundle1.getText("label.type")}),
															template: new sap.ui.commons.TextView().bindProperty("text", "cart_trolley_type")
															
														}),
													],
													rowSelectionChange: function(e) {
														qtyIdx = -1;
														 oUnitsTable.setModel(new sap.ui.model.json.JSONModel());
														 oUnitsTable.setSelectedIndex(-1);
														 oUnitsTable.getModel().refresh();
														 
														 oGSSegmentedButton.setSelectedButton(null);
														 sap.ui.getCore().byId("GSSegmentedButton").setEnabled(false);
														 selectedUnitQty={};
														 	sap.ui.getCore().byId("QtyLabel").setVisible(false);
															sap.ui.getCore().byId("QtyDataSet").setVisible(false);																		
															sap.ui.getCore().byId("TQtyDataSet").setVisible(false);
															sap.ui.getCore().byId("clearBut").setVisible(false);
															sap.ui.getCore().byId("clearBut").setEnabled(false);
														 if(oTypesTable.getSelectedIndex()>-1){
																if("LOWERATOR" == oTypesTable.getContextByIndex(oTypesTable.getSelectedIndex()).getProperty("category").toUpperCase() && !isIHSelected){
																	sap.ui.getCore().byId("QtyLabel").setVisible(true);
																	sap.ui.getCore().byId("QtyDataSet").setVisible(false);																		
																	sap.ui.getCore().byId("TQtyDataSet").setVisible(true);
																	sap.ui.getCore().byId("clearBut").setVisible(true);
																}else{
																	sap.ui.getCore().byId("QtyLabel").setVisible(true);
																	sap.ui.getCore().byId("QtyDataSet").setVisible(true);
																	sap.ui.getCore().byId("TQtyDataSet").setVisible(false);
																	sap.ui.getCore().byId("clearBut").setVisible(false);
																	sap.ui.getCore().byId("clearBut").setEnabled(false);
																	if(isIHSelected){
																		sap.ui.getCore().byId("QtyDataSet").addStyleClass("disable_dataset");
																		sap.ui.getCore().byId("TQtyDataSet").addStyleClass("disable_dataset");
																	}
																}
																sap.ui.core.BusyIndicator.show();
																oController.updateUnitsDataModelByTypeTable(typeSelected);																											
																//setHtmlTextForSelectedValues();
																maxQtyAllowed=-1;
														 }else{
															 sap.ui.getCore().byId("QtyDataSet").addStyleClass("disable_dataset");
															 sap.ui.getCore().byId("TQtyDataSet").addStyleClass("disable_dataset");
															 sap.ui.getCore().byId("QtyDataSet").setVisible(true);
															 sap.ui.getCore().byId("TQtyDataSet").setVisible(false);
															 sap.ui.getCore().byId("clearBut").setVisible(false);
															 sap.ui.getCore().byId("clearBut").setEnabled(false);
														 }
														 image.setVisible(false);
														 if((jQuery.isEmptyObject(selectedUnitQty))  && !isMxSelected){
															 if(staffId_update.getVisible()){
																 staffId_update.setEnabled(false);
															 }else{
																 sap.ui.getCore().byId("addSave").setEnabled(false);
															 }
															
														 }
														// }
													},
													
												}).addStyleClass("customTable"),									 		 		
												oUnitsTable = new sap.ui.table.Table("UnitTable", {
													selectionMode: sap.ui.table.SelectionMode.Single,
													selectionBehavior: sap.ui.table.SelectionBehavior.RowOnly,
													visibleRowCount:5,navigationMode: sap.ui.table.NavigationMode.Scrollable, showNoData: false,enabled:true,selectedIndex: -1,
													rowHeight:55,
													layoutData: new sap.ui.layout.GridData({span: "L6 M6 S6", linebreak: false}),
													columns:
													[
														new sap.ui.table.Column({
															label: new sap.ui.commons.Label({text: oBundle1.getText("label.unit")}),
															template: new sap.ui.commons.TextView().bindProperty("text", "unit_type"),width:"70%",
														}),
														new sap.ui.table.Column({
															label: new sap.ui.commons.Label({text: oBundle1.getText("label.qty")}),
															template: new sap.ui.commons.TextView().bindProperty("text", "qty"),width:"30%",
														}),
													],
													rowSelectionChange: function(e) {
													
														previousQtyIdx=qtyIdx;
														
												 if(oUnitsTable.getSelectedIndex()>-1){
														qtyIdx=e.getParameter('rowIndex');	
														typeSelected=oTypesTable.getContextByIndex(oTypesTable.getSelectedIndex()).getProperty("cart_trolley_type");
														cateogorySelected=oTypesTable.getContextByIndex(oTypesTable.getSelectedIndex()).getProperty("category");
														if((cateogorySelected == "CART" || isIHSelected || touchConsoleRequestType == "DR" )&&(previousQtyIdx != qtyIdx)){
															selectedUnitQty={};
															maxQtyAllowed=-1;
															if(previousQtyIdx != -1){
																qtyChkMap={'Full':'',
												             			   'Half':''};
																oUnitsTable.getModel().getData()["genericTableModel"][previousQtyIdx]["qty"]="";
															
																
															}
														}	
														if(!isIHSelected){
															sap.ui.getCore().byId("GSSegmentedButton").setEnabled(true);
															oController.prepareGalleryData();																			
														}else{
															sap.ui.getCore().byId("GSSegmentedButton").setEnabled(false);
															if(isIHSelected){
																oController.getImageAndQtyLimits();
																 if(isIHSelected){
																		oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["qty"]="1";
																		selectedUnitQty={};											
																		selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()] = "1";
																		oUnitsTable.getModel().refresh();
																		 if(staffId_update.getVisible()){
																			 staffId_update.setEnabled(true);
																			 //staffId_update.focus();
																		 }else{
																			 sap.ui.getCore().byId("addSave").setEnabled(true);
																		 }
																	}
															}else{
																sap.ui.getCore().byId("QtyDataSet").removeStyleClass("disable_dataset");
																sap.ui.getCore().byId("TQtyDataSet").removeStyleClass("disable_dataset");
															}
														}
												 }else{
													 image.setVisible(false);
													 sap.ui.getCore().byId("QtyDataSet").addStyleClass("disable_dataset");
													 sap.ui.getCore().byId("TQtyDataSet").addStyleClass("disable_dataset");
													 if((jQuery.isEmptyObject(selectedUnitQty)) && !isMxSelected){
														 if(staffId_update.getVisible()){
															 staffId_update.setEnabled(false);
														 }else{
															 sap.ui.getCore().byId("addSave").setEnabled(false);
														 }
														
													 }
													 if(oTypesTable.getSelectedIndex() == -1){
														 selectedUnitQty={};
														 if(staffId_update.getVisible()){
															 staffId_update.setEnabled(false);
														 }else{
															 sap.ui.getCore().byId("addSave").setEnabled(false);
														 }
													 }else if(jQuery.isEmptyObject(selectedUnitQty)){
														 oGSSegmentedButton.setSelectedButton(null);
														 sap.ui.getCore().byId("GSSegmentedButton").setEnabled(false);
													 }else if(previousQtyIdx != -1){
														 oUnitsTable.setSelectedIndex(previousQtyIdx);
													 }
												 }	
												},
													
												}).addStyleClass("customTable"),
												
								 		],
								 	}),
								 	
								 	new sap.ui.layout.form.FormElement
								 	("F1C55E1",{
								 		fields:
								 		[		image=new sap.ui.commons.Image("image1",{
												/*	src : imgDisplayed,
													alt : "UnitType image",
													width : "200px",
													height : "250px",
													visible:false,*/

								 					alt : "noImage",
													height : "250px",
													visible: false,
													layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12", linebreak: true}),
												}).addStyleClass("unitImg"),
										 		
								 		 		
								 		],
								 	}),
								 	
								 	
								 	
								 	
								 	
								 	
								 ],
							layoutData: new sap.ui.layout.GridData({span: "L5 M5 S5", linebreak: false})
							}),
							
							new sap.ui.layout.form.FormContainer("F1C6",
									{
										formElements: 
										[
										 	new sap.ui.layout.form.FormElement
										 	("F1C6E1",{
										 		fields:
										 		[		
										 		 		oGSSegmentedButton=	new sap.ui.commons.SegmentedButton("GSSegmentedButton",{enabled:true,layoutData:new  sap.ui.layout.GridData({span: "L12 M12 S12", linebreak: false})}),
										 		 		oGSSegmentedButton.insertButton(new sap.ui.commons.Button("ATLAS",{text: oBundle1.getText("label.atlas"),enabled:false}).addStyleClass("gSSegmentedButton"),0),
										 		 		oGSSegmentedButton.insertButton(new sap.ui.commons.Button("KSSU",{text: oBundle1.getText("label.kssu"),enabled:false}).addStyleClass("gSSegmentedButton"),1),
										 		 		oGSSegmentedButton.attachSelect(function(oEvent) {
										 		 			oController.getImageAndQtyLimits();
										 		 			if(isQtyChngeReq){
										 		 				//clear qty
											 		 			selectedUnitQty={};
											 		 			qtyChkMap={'Full':'','Half':''};
											 		 			maxQtyAllowed=-1;
																for(var noOfUnits=0;noOfUnits<oUnitsTable.getModel().getData()["genericTableModel"].length;noOfUnits++){
																	oUnitsTable.getModel().getData()["genericTableModel"][noOfUnits]["qty"]="";
																}
																oUnitsTable.getModel().refresh();
										 		 			}else{
										 		 				isQtyChngeReq=true;
										 		 			}
										 		 			
										 		 			if(oGSSegmentedButton.getSelectedButton() != null || isIHSelected){
										 		 				 sap.ui.getCore().byId("QtyDataSet").removeStyleClass("disable_dataset");
																 sap.ui.getCore().byId("TQtyDataSet").removeStyleClass("disable_dataset");
																 if(sap.ui.getCore().byId("clearBut").getVisible())
																	 sap.ui.getCore().byId("clearBut").setEnabled(true);
																 else
																	 sap.ui.getCore().byId("clearBut").setEnabled(false);
																 if(cateogorySelected == "CART" || isIHSelected){
																		oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["qty"]="1";
																		sap.ui.getCore().byId("QtyDataSet").addStyleClass("disable_dataset");
																		 selectedUnitQty={};											
																		selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"].trim()] = "1";
																		oUnitsTable.getModel().refresh();
																		 if(staffId_update.getVisible()){
																			 staffId_update.setEnabled(true);
																			 //staffId_update.focus();
																		 }else{
																			 sap.ui.getCore().byId("addSave").setEnabled(true);
																		 }
																	}
										 		 			}else{
										 		 				 sap.ui.getCore().byId("QtyDataSet").addStyleClass("disable_dataset");
																 sap.ui.getCore().byId("TQtyDataSet").addStyleClass("disable_dataset");
																 sap.ui.getCore().byId("clearBut").setEnabled(false);
										 		 			}
										 				})
										 		 		
										 		],
										 	}),
										 ],
										 layoutData: new sap.ui.layout.GridData({span: "L3 M3 S3", linebreak: false})
									}),
									new sap.ui.layout.form.FormContainer("F1C7",
											{
												formElements: 
												[
												 	new sap.ui.layout.form.FormElement
												 	("F1C7E1",{
												 		fields:
												 		[		
												 		 	new sap.ui.commons.Label("QtyLabel",{text: oBundle1.getText("label.qty"),layoutData: new sap.ui.layout.GridData({span: "L9 M9 S9", linebreak: true})}).addStyleClass("qtyLabel"),	
															 oQtyDataSet = new sap.ui.ux3.DataSet("QtyDataSet",{showToolbar:false,showFilter:false,showSearchField:false,visible:false,
																 layoutData: new sap.ui.layout.GridData({span: "L10 M10 S10", linebreak: true}),
																items: {
																	path: "/qtys",						
																	template: new sap.ui.ux3.DataSetItem()
																},
																views: [
																	new sap.ui.ux3.DataSetSimpleView({
																		name: "Floating, non-responsive View",
																		floating: true,
																		responsive: true,
																		itemMinWidth: 50,
																		template: createTemplateGeneric(false,"renderedQty-csta")
																	}),
																	
																
																],
																selectionChanged: function search(oEvent) {
																	
																}
															}).addStyleClass("disable_dataset"),
															
												 		],
												 	}),
												 ],
												 layoutData: new sap.ui.layout.GridData({span: "L3 M3 S3", linebreak: false})
											}),
											new sap.ui.layout.form.FormContainer("F1C18",
													{
														formElements: 
														[
														 	new sap.ui.layout.form.FormElement
														 	("F1C18E1",{
														 		fields:
														 		[		
														 		 	new sap.ui.commons.Label("TQtyLabel",{text: oBundle1.getText("label.qty"),visible:false,layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12", linebreak: true})}).addStyleClass("qtyLabel"),	
																	 oTQtyDataSet = new sap.ui.ux3.DataSet("TQtyDataSet",{showToolbar:false,visible:false,showFilter:false,showSearchField:false,visible:false,
																		 layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12", linebreak: true}),
																		 items: {
																			path: "/qtys",
																			template: new sap.ui.ux3.DataSetItem()
																		},
																		views: [
																			new sap.ui.ux3.DataSetSimpleView({
																				name: "Floating, non-responsive View",
																				floating: true,
																				responsive: true,
																				itemMinWidth: 50,
																				template: createTemplateLoweratorQty(false,"renderedTQty-csta")
																			}),
																		],
																		selectionChanged: function search(oEvent) {
																			var idx = oEvent.getParameter("newLeadSelectedIndex");
																																				
																		}
																	}).addStyleClass("disable_dataset"),
																	new sap.ui.commons.Button("clearBut",{text: oBundle1.getText("label.clear"),visible:false,enabled:false,layoutData:new  sap.ui.layout.GridData({span: "L12 M12 S12", linebreak: true}),
																		press: function(oEvent){
																			var idx = oEvent.getParameter("newLeadSelectedIndex");
																			if(oUnitsTable.getSelectedIndex()>-1){
																				var qtyAvailable=oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["qty"];																		
																				//clear hashmap
																				if(qtyAvailable=='Full'){
																					if(qtyChkMap['Full'] != '')
																						qtyChkMap['Full']='';
																					
																				}
																				if(qtyAvailable=='Half'){
																					if(qtyChkMap['Half']!= ''){
																						if(qtyChkMap['Half']==2)
																							qtyChkMap['Half']=1;
																						else if(qtyChkMap['Half']==1)
																							qtyChkMap['Half']='';
																					} 
																				}
																				//clear data
																				qtyAvailable="";
																				//set values to selected unit and qty map
																				delete selectedUnitQty[oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["unit_type"]];
																				oUnitsTable.getModel().getData()["genericTableModel"][qtyIdx]["qty"]=qtyAvailable;	
																				oUnitsTable.getModel().refresh();
																				
																				
																			}
																			
																		}
																	}).addStyleClass("clearButton-CSTA")
														 		],
														 	}),
														 ],
														 layoutData: new sap.ui.layout.GridData({span: "L3 M3 S3", linebreak: false})
													}),
							
			]
	});
	//End
	oAddDialog.addContent(oForm1);
	//set isBusy true to make sure that screen is busy
	isBusy=true;
	oAddDialog.addButton(new sap.ui.commons.Button("addSave",{text: oBundle1.getText("btn.confirm"),tooltip: oBundle1.getText("btn.confirm"), icon: "sap-icon://save", style: sap.ui.commons.ButtonStyle.Accept,enabled: false,visible:!correction_required_staff_id,
		press:function(){
			sap.ui.getCore().byId("addSave").setEnabled(false);
			if(oGSSegmentedButton.getSelectedButton()!=null){
				galleyStdSelected=oGSSegmentedButton.getSelectedButton();
			}
			if(barCodeMXSelected != ""){
				isMxSelected=true;
				if(!jQuery.isEmptyObject(cartsOnTrayMapForBarcode)){
					if(action=="Add"){
						oController.formUpdateQueryAndSubmitForBarcode("Add");
					}else{
						oController.formUpdateQueryAndSubmitForBarcode("Update");
					}
				}else{
					sap.ui.commons.MessageBox.show(oBundle1.getText("msg.scan.bar.code"), sap.ui.commons.MessageBox.Icon.ERROR, "Error", [sap.ui.commons.MessageBox.Action.OK],function(){
						sap.ui.getCore().byId("addSave").setEnabled(true);
					});
					barCodeMXSelected="";
				}	
			}else{
				if(action=="Add"){
					oController.formUpdateQueryAndSubmit("Add");
				}else{
					oController.formUpdateQueryAndSubmit("Update");
				}
			}
		}
			
	}));
	oAddDialog.addButton(staffId_update=new sap.ui.commons.TextField({value: "",maxLength:15,placeholder: oBundle1.getText("instruction.scan.staff.id"), enabled: true,visible:false,
			layoutData: new sap.ui.layout.GridData({linebreak: false}),
		change:function (e) {
            if (this.getValueState() !== sap.ui.core.ValueState.Error) {
            	scanStaffId.setValue(staffId_update.getLiveValue());
            	staffId_update.setValue("");
            	if(isMxSelected){
            		if(action=="Add")
						oController.validateStaffCard("BarcodeAdd",dialogeName);
					else
						oController.validateStaffCard("BarcodeUpdate",dialogeName);
	 			}else{
            		if(action=="Add")
						oController.validateStaffCard("Add",dialogeName);
					else
						oController.validateStaffCard("Update",dialogeName);
            	}
 				
            }
        }
	}));
	oAddDialog.addButton(staffId_update_lable=new sap.ui.commons.TextView({text:oBundle1.getText("instruction.scan.staff.id",oBundle1.getText("label.update")), enabled: false,visible:correction_required_staff_id,
		layoutData: new sap.ui.layout.GridData({span: "L8 M8 S8", linebreak: false}),
	
}).addStyleClass("popupForCartsAdding"));
	oAddDialog.addButton(new sap.ui.commons.Button("addCancel",{text: oBundle1.getText("label.cancel"),tooltip: oBundle1.getText("label.cancel"), icon: "sap-icon://sys-cancel-2", style: sap.ui.commons.ButtonStyle.Reject,
		press:function(){
			oAddDialog.destroyButtons();
			oAddDialog.destroyContent();
			oAddDialog.close();
			halfCartSelected=false;
			barCodeMXSelected = "";
			isAirlineChanged = true;
			isBusy=false;
			sap.ui.core.BusyIndicator.hide();
		}
	}));
	oAddDialog.attachClosed(function(){
		oAddDialog.destroyButtons();
		oAddDialog.destroyContent();
		halfCartSelected=false;
		barCodeMXSelected = "";
		isAirlineChanged = true;
		isBusy=false;
		sap.ui.core.BusyIndicator.hide();
	});
	oAddDialog.open();
	isAirlineChanged = true;
	oController.addNew();
}

//check image url is existig or not
function testImage(URL,good,bad) {
	var testImg=new Image();
	testImg.onload=good;
	testImg.onerror=bad;
	testImg.src=URL;
}

//check mandatory fields
function checkMandatoryFields(){
	if(!isIHSelected && oAirlineDataSet.getLeadSelection() == -1){
		 //airline is mandatory
		if(isMxSelected){
			sap.ui.commons.MessageBox.show(oBundle1.getText("msg.airline.spl.mandatory"), sap.ui.commons.MessageBox.Icon.ERROR, "Error");
		}else{
			sap.ui.commons.MessageBox.show(oBundle1.getText("msg.airline.mandatory"), sap.ui.commons.MessageBox.Icon.ERROR, "Error");
		}
	 }else if(oTypesTable.getSelectedIndex() == -1){
		 //type is mandatory
		 if(isIHSelected || isMxSelected){
			 sap.ui.commons.MessageBox.show(oBundle1.getText("msg.type.spl.mandatory"), sap.ui.commons.MessageBox.Icon.ERROR, "Error");
		 }else{
			 sap.ui.commons.MessageBox.show(oBundle1.getText("msg.type.mandatory"), sap.ui.commons.MessageBox.Icon.ERROR, "Error");
		 }
	 }else if(oUnitsTable.getSelectedIndex() == -1){
		 //unit is mandatory
		 if(isIHSelected || isMxSelected){
			 sap.ui.commons.MessageBox.show(oBundle1.getText("msg.unit.spl.mandatory"), sap.ui.commons.MessageBox.Icon.ERROR, "Error");
		 }else{
			 sap.ui.commons.MessageBox.show(oBundle1.getText("msg.unit.mandatory"), sap.ui.commons.MessageBox.Icon.ERROR, "Error");
		 }
	 }else if(oGSSegmentedButton.getSelectedButton() == null){
		 //galley std is mandatory
		 sap.ui.commons.MessageBox.show(oBundle1.getText("msg.galleystd.mandatory"),sap.ui.commons.MessageBox.Icon.ERROR);
	 }
}

function blinkeffect(selector, flag) {
	if(flag) {
		$(selector).fadeOut('slow', function() {
			$(this).fadeIn('slow', function() {
				blinkeffect(this);
			});
		});
	}
}

function callBlink(selector, flag, count) {
	for(var incrementCount=0; incrementCount<=count; incrementCount++) {
		blinkeffect(selector, flag);
	}
}

function returnUniqueValuesInArray(origArr){
	 origArr=origArr.sort();
	 var newArr = [],
     origLen = origArr.length,
     found, x, y;

	 for (x = 0; x < origLen; x++) {
	     found = undefined;
	     for (y = 0; y < newArr.length; y++) {
	         if (origArr[x] === newArr[y]) {
	             found = true;
	             break;
	         }
	     }
	     if (!found) {
	         newArr.push(origArr[x]);
	     }
	 }
	 return newArr;
}

$(document).ready(function(){
	if (typeof String.prototype.startsWith != 'function') {
		String.prototype.startsWith = function (prefix) {
		    return this.slice(0, prefix.length) == prefix;
		 };
	}
	if (typeof String.prototype.endsWith != 'function') {
		String.prototype.endsWith = function (suffix) {
		    return suffix == '' || this.slice(-suffix.length) == suffix;
		};
	}
	//document.oncontextmenu = document.body.oncontextmenu = function() {return false;}	
});