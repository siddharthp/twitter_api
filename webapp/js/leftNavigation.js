function showLeftNavigation(id) {
	// header height 60px and navigation bar 37px total 97px
 	new sap.ui.commons.Tree("leftNavigation", {width: "100%", height: ($( window ).height()-97) + "px", selectionMode: "Single", title: "Menu",visible: false,
 		nodes:
 		[
 		 		new sap.ui.commons.TreeNode({text: "I serve U Dashboard", expanded: false,hasExpander: false}),
 		 ], 
 		select: function(oEvent) {
 			var isTabAlreadyOpen = false;
 			var selectedNode = oEvent.getParameter("node");
			selectedNode.getExpanded() ? selectedNode.setExpanded(false) : selectedNode.setExpanded(true);
			if (!selectedNode.getHasExpander()){
				var oTabCont = sap.ui.getCore().byId("tabContainer");
				//alert(oTabCont);
				jQuery.each(oTabCont.getItems(), function(idx, item){
					if (item.getName() === selectedNode.getText()) {
						isTabAlreadyOpen = true;
						
						oTabCont.setSelectedItem(item);
					
					}
				});
				if (!isTabAlreadyOpen){
				
					var oTab = new sap.m.TabContainerItem({
								name: selectedNode.getText(),
								isSelected: true,
								content: 
									[ 
									  new sap.ui.core.mvc.JSView({viewName:"oui5mvc."+selectedNode.getText().replace(/ /g,'').toLowerCase()}).addStyleClass("tabContentArea"),
									]
							});
					
					oTabCont.addItem(oTab);
					oTabCont.setSelectedItem(oTab);
					tabCount++;
				}
				var oTree = sap.ui.getCore().byId("leftNavigation");
				oTree.setVisible(false);
			}
 		}
 	}).placeAt(id).addStyleClass("leftNavTree");
}