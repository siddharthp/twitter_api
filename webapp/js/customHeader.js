loadCSS("customControls", "css/customControls.css");
var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "dd-MMM-yyyy HH:mm:ss"});

sap.ui.define([
	"sap/ui/core/Control"
], function (Control){
	"use strict";
	return Control.extend("Header", {
		metadata:{
			properties: {
				vendorLogo: 		{type: "sap.ui.core.URI", group: "Misc", defaultValue: null},
				heading: 			{type: "string", group: "Misc", defaultValue: null},
				clientLogo: 		{type: "sap.ui.core.URI", group:"Misc", defaultValue: null},
				displayUsrName: 	{type: "boolean", group: "Misc", defaultValue: false},
				userName:			{type: "string", group: "Misc", defaultValue: "Guest"},
				lastLogin: 			{type: "string", group: "Misc", defaultValue: null}
			}
		},

		setVendorLogo: function(oSrc) {
			this.initializationDone=false;
			this.setProperty("vendorLogo", oSrc, true);
		},

		setHeading: function(oTxt) {
			this.initializationDone=false;
			this.setProperty("heading", oTxt, true);
		},

		setClientLogo: function(oSrc) {
			this.initializationDone=false;
			this.setProperty("clientLogo", oSrc, true);
		},

		setDisplayUsrName: function(oUsr) {
			this.initializationDone=false;
			this.setProperty("displayUsrName", oUsr, true);
		},
		
		setUserName: function(oName) {
			this.initializationDone = false;
			this.setProperty("userName", oName, true);
		},
		
		setLastLogin: function(oTs) {
			this.initializationDone = false;
			this.setProperty("lastLogin", oTs, true);
		},
		
		init: function(){
			this.initializationDone = false;
		},
		
		initControls: function() {
			var a=this.getId();
			var r=sap.ui.getCore().getLibraryResourceBundle("sap.ui.commons");
			
			this.oVendorLogo&&this.oVendorLogo.destroy();
			this.oVendorLogo=new sap.ui.commons.Image(a+"-vLogoImg");
			this.oVendorLogo.setTooltip(r.getText("APPHDR_LOGO_TOOLTIP"));
			this.oVendorLogo.setParent(this);
			this.oVendorLogo.setWidth("100%");
			this.oVendorLogo.setHeight("60px");

			this.oClientLogo&&this.oClientLogo.destroy();
			this.oClientLogo=new sap.ui.commons.Image(a+"-cLogoImg");
			this.oClientLogo.setTooltip(r.getText("APPHDR_LOGO_TOOLTIP"));
			this.oClientLogo.setParent(this);
			this.oClientLogo.setWidth("100%");
			this.oClientLogo.setHeight("60px");

			this.oHeading&&this.oHeading.destroy();
			this.oHeading=new sap.ui.commons.TextView(a+"-heading");
			this.oHeading.setWidth("100%");
			this.oHeading.setAccessibleRole(sap.ui.core.AccessibleRole.Heading);
			this.oHeading.setParent(this);
			
			this.oUsrTV = new sap.ui.commons.FormattedTextView();
			this.oUsrTV.setParent(this);
		},
		
		renderer: {
			render: function(oRM, oControl){
				if(!this.initializationDone){
					oControl.initControls();
					oControl.initializationDone = true;
				}
				if(!this.initializationDone){
					oControl.initControls();
					oControl.initializationDone=true;
				}
				var b = oControl.getId();
				oRM.write("<header");
				oRM.writeControlData(oControl);
				oRM.addClass("maskUiAppHdr");
				/*oRM.addClass("animate0");
				oRM.addClass("rotateInDownRight");*/
				oRM.writeClasses();
				oRM.write(">");
				oRM.write("<div id=\""+b+"-vLogoArea\" class=\"hdrLogoV\">");
				this.renderLogoAreaV(oRM, oControl);
				oRM.write("</div>");
				oRM.write("<div id=\""+b+"-headingArea\" class=\"uiHdrTitle\">");
				this.renderHeadingArea(oRM, oControl);
				oRM.write("</div>");
				oRM.write("<div id=\""+b+"-usrDetailsArea\" class=\"uiHdrUsrDtlsArea\">");
				this.renderUserDetailsArea(oRM, oControl);
				oRM.write("</div>");
				oRM.write("<div id=\""+b+"-cLogoArea\" class=\"hdrLogoC\">");
				this.renderLogoAreaC(oRM, oControl);
				oRM.write("</div>");
				oRM.write("</header>");
			},
			
			renderLogoAreaV: function(oRM, oControl){
				var s=oControl.getVendorLogo();
				oControl.oVendorLogo.setSrc(s);
				oRM.renderControl(oControl.oVendorLogo);
			},
			
			renderLogoAreaC: function(oRM, oControl){
				var s=oControl.getClientLogo();
				oControl.oClientLogo.setSrc(s);
				oRM.renderControl(oControl.oClientLogo);
			},
			
			renderHeadingArea:function(oRM, oControl){
				if(oControl.getHeading()!=""){
					oControl.oHeading.setText(oControl.getHeading());
					oControl.oHeading.setTooltip(oControl.getHeading());
					oRM.renderControl(oControl.oHeading);
				}
			},
			
			renderUserDetailsArea: function(oRM, oControl){
				if (oControl.getDisplayUsrName()){
					var oData = "Welcome <span class='userName'>Arush</span>";
					if (oControl.getUserName() != 'Guest' && null != oControl.getLastLogin() && oControl.getLastLogin().length > 0){
						oData = oData + "<div class='lastLoginArea'>last login: " + oDateFormat.format(new Date(parseInt(oControl.getLastLogin()))) + "</div>";
					}
					oControl.oUsrTV.setHtmlText(oData);
					oRM.renderControl(oControl.oUsrTV);
				}
			}
		},
		
		exit: function(){
			this.oVendorLogo&&this.oVendorLogo.destroy();
			this.oClientLogo&&this.oClientLogo.destroy();
			this.oHeading&&this.oHeading.destroy();
			this.oUsrTV&&this.oUsrTV.destroy();
			this.oUsrLoginTV&&this.oUsrLoginTV.destroy();
		}
	});
});