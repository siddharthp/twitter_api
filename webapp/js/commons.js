sap.ui.localResources("js");
jQuery.sap.require("jquery.sap.resources");
jQuery.sap.require("sap.ui.Device");
jQuery.sap.require("sap.ui.commons.MessageBox");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("jquery.sap.storage");
oStorage = jQuery.sap.storage("session");
jQuery.sap.require("sap.ui.core.format.DateFormat");
sap.ui.localResources('chartControls');  
jQuery.sap.require("chartControls.linechart","chartControls.barLineComboChart",
	"chartControls.customBullet","chartControls.minMaxAvgBar","chartControls.estimatedVsPlannedTimeChart",
	"chartControls.dropDownTemplates","chartControls.myLegend","js.timeinterval","chartControls.piechart","chartControls.stackedBarChart",
	"chartControls.gaugechart"); 

/*var countryLocale = "de-DE"; 
var defaultLocale = "en-EN";
oBundle1 = jQuery.sap.resources({url : "i18n/labels.properties", locale: defaultLocale});
oBundle2 = jQuery.sap.resources({url: "i18n/labels.properties", locale: countryLocale});*/

sap.ui.Device.orientation.attachHandler(function(mParams) {
	var cLeftNav = sap.ui.getCore().byId("leftNavigation");
	if (cLeftNav){ cLeftNav.setHeight(($( window ).height()-97) + "px"); cLeftNav.rerender();}
    if (mParams.landscape) {} else {}
});

function displayHeader() {
	jQuery.sap.require("js.customHeader");
	//var showMenuOnHover = false;

	//JQuery handler to close already opened sub menus on clicking open area of the page
	/*$(document).click(function() {
		oSettingsMenu.close();
		if (!oSettingsMenu.bOpen && showMenuOnHover)
			showMenuOnHover = false;
	});

	//Settings menu setup
	var oSetgsMenuBar = new sap.ui.commons.MenuBar("setgsMenuBar", {width: "auto"});
	var oMnuItm0 = new sap.ui.unified.MenuItem("settings-item-0", {text: "Anand Madhupada"});

	var oSettingsMenu = new sap.ui.unified.Menu("setgsMenu", {ariaDescription: "Settings Menu"});
	var oMnuItm1 = new sap.ui.unified.MenuItem("scup", {text: "Change Password"});
	var oMnuItm2 = new sap.ui.unified.MenuItem("about", {text: "About"});
	var oMnuItm3 = new sap.ui.unified.MenuItem("logout", {text: "Log Out"});
	oSettingsMenu.addItem(oMnuItm1);
	oSettingsMenu.addItem(oMnuItm2);
	oSettingsMenu.addItem(oMnuItm3);
	oSettingsMenu.attachItemSelect(function(oEvent){
		if (oEvent.getParameter("item").getId() === "scup"){
		}else if (oEvent.getParameter("item").getId() == 'about'){
			sap.ui.core.BusyIndicator.show(0);
			var oAboutDialog = new sap.ui.commons.Dialog({title: "About", modal: true, width: "450px"});
			var oSimpleForm1 = new sap.ui.layout.form.SimpleForm({
				maxContainerCols: 1,
				content:
				[
				 	new sap.ui.commons.Label({text: "Version", textAlign: "Center", design: "Bold", width: "100%"}),
				 	new sap.ui.commons.TextView({text: oStorage.get("guiver")}),
				 	new sap.ui.commons.Label({text: "Copyright", textAlign: "Center", design: "Bold", width: "100%"}),
				 	new sap.ui.commons.TextView({text: "Copyright 2015 Prospecta Technologies India Pvt Ltd"}),
				 	new sap.ui.commons.Label({text: "License", textAlign: "Center", design: "Bold", width: "100%"}),
				 	new sap.ui.commons.TextView({text: "This product includes software developed by the Free Software Foundation which is licensed under the terms of the GNU GPL (http://www.fsf.org)."}),
				 	new sap.ui.commons.Label({text: "Open Source Credits", textAlign: "Center", design: "Bold", width: "100%"}),
				 	new sap.ui.commons.TextView({text: "This product includes software developed by the Free Software Foundation which is licensed under the terms of the GNU GPL (http://www.fsf.org)."})
				]
			});
			oAboutDialog.addContent(oSimpleForm1);
			
			oAboutDialog.attachClosed(function(){
				sap.ui.core.BusyIndicator.hide();
			});
			oAboutDialog.open();
		}else
			window.location=oEvent.getParameter("item").getId()
	});
	oMnuItm0.setSubmenu(oSettingsMenu);
	oSetgsMenuBar.addItem(oMnuItm0);
	oMnuItm0.onclick = function(){
		showMenuOnHover = true;
		oSettingsMenu.toggleStyleClass("firstMnuItmSelect", true);
	};
	oMnuItm1.onmouseout = function(){
		oSettingsMenu.toggleStyleClass("firstMnuItmSelect");
	};
	oMnuItm1.onmouseover = function(){
		if (!oSettingsMenu.hasStyleClass("firstMnuItmSelect"))
			oSettingsMenu.toggleStyleClass("firstMnuItmSelect");
	};
	oMnuItm0.onmouseover = function(){
		if (oSettingsMenu.bOpen) oSettingsMenu.close();
	 	if (showMenuOnHover)
	 		oSettingsMenu.open(true, oMnuItm0.getDomRef(), "left top", "left bottom", oMnuItm0, "0 0", "none");
	};*/
	
	new Header("appHeader", {
		vendorLogo: oStorage.get("vendorLogoURL"),
		clientLogo: "images/aSMS.jpg", 
		heading: "Safety Management System (aSMS)",
		displayUsrName: true,
		userName: loginUserName,
		//lastLogin: new Date().getTime()
	}).placeAt("header");
}

function loadCSS(id, href) {
	var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.id   = id;
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = href;
    head.appendChild(link);	
}

function removeCSS(id) {
	var head  = document.getElementsByTagName('head')[0];
	var link = document.getElementById(id);
	head.removeChild(link);
}