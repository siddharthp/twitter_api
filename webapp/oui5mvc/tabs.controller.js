sap.ui.controller("oui5mvc.tabs", {
	doAjax : function(path, content, type, async) {
		var appPath = window.location.pathname.split('/')[1];
		var params = {
			url : "/" + appPath + path,
			dataType : "json",
			contentType : "application/json ; charset=utf-8",
			context : this,
			cache : false,
			error : handleAjaxError
		};
		params["type"] = type || "POST";
		if (async === false) {
			params["async"] = async;
		}
		if (content) {
			params["data"] = JSON.stringify(content);
		}
		return jQuery.ajax(params);
	},
	onInit : function() {
		sap.ui.Device.orientation.attachHandler(function(mParams) {
			if (mParams.landscape) {
				setTimeout(
					function(){
						
							// code to fireselect for selected tab again to render charts 
							var oTabCont = sap.ui.getCore().byId("tabContainer");
						
								jQuery.each(oTabCont.getItems(), function(idx, item){
								
									if (item.toString().indexOf(oTabCont.getSelectedItem().toString()) > -1) {
									
										oTabCont.setSelectedItem(item);
										
									}
								});
						
					},100
				);
		     } 
			else {
				setTimeout(
					function(){
							// code to fireselect for selected tab again to render charts 
							var oTabCont = sap.ui.getCore().byId("tabContainer");
						
								jQuery.each(oTabCont.getItems(), function(idx, item){
								
									if (item.toString().indexOf(oTabCont.getSelectedItem().toString()) > -1) {
										
										oTabCont.setSelectedItem(item);
										
									}
								});
						
					},100
				);
		     }
		});
	},
	onAfterRendering : function() {
	
	}

});