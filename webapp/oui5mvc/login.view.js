var loginGlobalId;
sap.ui.jsview("oui5mvc.login", {
	getControllerName: function() {
		return "oui5mvc.login";
	},

	createContent: function(oController) {
		var countryLocale = "de_DE"; 
			//sap.ui.getCore().getConfiguration().getLanguage();
		var defaultLocale = "en_EN";
		oResBundle1 = jQuery.sap.resources({url : "i18n/labels.properties", locale: defaultLocale});
		oResBundle2 = jQuery.sap.resources({url: "i18n/labels.properties", locale: countryLocale});
		
		loginGlobalId = this.getId();
		var view = sap.ui.getCore().byId(loginGlobalId);
		
		$(document).ready(function(){
			sap.ui.getCore().byId("usr").focus();
		});	

		var oHeader = new sap.ui.core.HTML({content: "<div><img src='http://iserveu.in/wp-content/uploads/2016/09/iServeU.png'/></div>"});
		oHeader.placeAt("header");
		
		//login page Action
		var oLoginLayout = new sap.ui.commons.layout.AbsoluteLayout(loginGlobalId +"layout",{width: "100%", height: "200px"}).addStyleClass("loginFormLyt");
		
		var oNameInput = new sap.ui.commons.TextField("usr", {width : "90%", placeholder: "Username",
			liveChange: function(){ /*this.setValue(this.getLiveValue().toUpperCase());*/ }
		}).addStyleClass("userNameInput");
		oLoginLayout.addContent(oNameInput, {left : "5%",top : "10%"});

		oPWInput = new sap.ui.commons.PasswordField("pw", {width : "90%", placeholder: "Password",liveChange: function(){}}).addStyleClass("passwordInput");
		oLoginLayout.addContent(oPWInput, {left : "5%",top : "37%"});

		sap.ui.getCore().byId("usr").onsapenter = function(e) {oLoginButton.firePress();}
		sap.ui.getCore().byId("pw").onsapenter = function(e) {oLoginButton.firePress();}

		var oLoginButton = new sap.ui.commons.Button("loginbutton",{text : oResBundle1.getText("login"), width : "90%", style: sap.ui.commons.ButtonStyle.Emph,
			press: function() {
				sap.ui.getCore().byId("loginbutton").setEnabled(false);
				var username=oNameInput.getLiveValue();//.toUpperCase();
				if (username)
				{
					if (oPWInput.getLiveValue())
					{
						//oController.loginAuth({"username":"demoarush","password":"QiO3ey"});
						oController.loginAuth({"username":username,"password":oPWInput.getLiveValue()});	
					}
					else
					{
			        	jQuery.sap.require("sap.ui.commons.MessageBox");
					 	sap.ui.commons.MessageBox.show(oResBundle1.getText("login.pwd.mandatory"), sap.ui.commons.MessageBox.Icon.ERROR, "Error");
					 	sap.ui.getCore().byId("loginbutton").setEnabled(true);
					}
				}
				else
				{
		        	jQuery.sap.require("sap.ui.commons.MessageBox");
				 	sap.ui.commons.MessageBox.show(oResBundle1.getText("login.uname.mandatory"), sap.ui.commons.MessageBox.Icon.ERROR, "Error");
				 	sap.ui.getCore().byId("loginbutton").setEnabled(true);
				}
			},
		});
		oLoginLayout.addContent(oLoginButton, {left : "5%", top : "65%"});
		var oForgotPwdLink = new sap.ui.commons.Link({text: "forgot password", href: "", target: "_blank", width: "100%"});
		//oLoginLayout.addContent(oForgotPwdLink, {left: "5%", top: "90%"});
		oLoginLayout.placeAt("loginArea");
		
		var oFooter = new sap.ui.core.HTML({content: "<div><span class='loginFormFtSpan' href='http://www.siddharthpolisiti.com/'>Powered by</span> <img src='images/powered-footer-logo.jpg'/></div>"});
		sap.ui.getCore().byId("usr").focus();
		oFooter.placeAt("footer");
	}
});