sap.ui.controller("oui5mvc.home", { 
	
	onInit:function() {
		viewId = this.getView();
	},
 
	doAjax : function(path, content, type, async) {
		var params = {
			url : "/" +appPath+ path,
			dataType : "json",
			contentType : "application/json",
			context : this,
			cache : false,
			error : handleAjaxError
		};
		params["type"] = type || "POST";
		if (async === false) {
			params["async"] = async;
		}
		if (content) {
			params["data"] = JSON.stringify(content);
		}
		return jQuery.ajax(params);
	},
})