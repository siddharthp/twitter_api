sap.ui.controller("oui5mvc.iserveudashboard", {
	
	doAjax : function(path, content, type, async) {
		var appPath = window.location.pathname.split('/')[1];
		var params = {
			url : path,
			dataType: "json",
			"method": "GET",
		      "headers": {
		        "authorization": oStorage.get("tokenId"),
		        /*"cache-control": "no-cache",*/
		      },
			//context : this,
			//accessControlAllowHeaders: "*",
			//cache : false,
			error : handleAjaxError
		};
		params["type"] = type || "GET";
		if (async === false) {
			params["async"] = async;
		}
		if (content) {
			params["data"] = JSON.stringify(content);
		}
		return jQuery.ajax(params);
	},

	onInit: function() {
		var jsonObjToSend = {} ;
		jsonObjToSend["dialogue"] = "test";
		jsonObjToSend["cid"] = "test";
		var srachmap = {} ;
		srachmap["type"] =  "test";
		jsonObjToSend["search"] =  srachmap;
		that = this;
		console.log(oStorage.get("tokenId"));
				this.doAjax("https://apiserveu.mybluemix.net/user/dashboard.json",oStorage.get("tokenId")).done(function(modelData){
					//alert("data"+JSON.stringify(modelData));
					if(modelData != null || modelData != ""){
						that.renderData(modelData);
					}
				});
				
		/*var settings = {
			      "async": true,
			      "crossDomain": true,
			      "url": "https://apiserveu.mybluemix.net/user/dashboard.json",
			      "method": "GET",
			      "headers": {
			        "authorization": oStorage.get("tokenId"),
			        "cache-control": "no-cache",
			      }
			    }

			$.ajax(settings).done(function (response) {
				alert(JSON.stringify(response));
			  console.log(response);
			});*/
	},
 

	renderData : function(modelData) {
		userNameLabelMain.setText("Username : "+modelData.userInfo.userName);
		userTypeLabel.setText("Role : "+modelData.userInfo.userType);
		userBalance.setText("Balance : "+modelData.userInfo.userBalance);
		adminName.setText("Admin Name : "+modelData.userInfo.adminName);
		promotionalMessage.setText(modelData.userInfo.promotionalMessage);
		
		
		
		
		userBrandSection = new sap.ui.commons.AccordionSection();
		userBrandSection.setTitle("User Brand");
		userBrandSection.setTooltip("User Brand");
		brandNameLabel = new sap.ui.commons.Label({text:"Brand : ",layoutData: new sap.ui.layout.GridData({span: "L6 M6 S6"}),});
		brandNameTextView = new sap.ui.commons.TextView({layoutData: new sap.ui.layout.GridData({span: "L6 M6 S6"}),});
		nameLabel = new sap.ui.commons.Label({text:"Name : ",layoutData: new sap.ui.layout.GridData({span: "L6 M6 S6"}),});
		nameTextView = new sap.ui.commons.TextView({layoutData: new sap.ui.layout.GridData({span: "L8 M8 S8"})});
		
		// set userbrand details
		var dat = modelData.userInfo.userBrand;
		var temp = dat.replace(/\\/g, "");
		var finaluserbrandDat = JSON.parse(temp);
		brandNameTextView.setText(finaluserbrandDat.brand);
		nameTextView.setText(finaluserbrandDat.name);
		userBrandSection.setCollapsed(finaluserbrandDat.isMenuCollapsed);
		userBrandSection.addContent(brandNameLabel);
		userBrandSection.addContent(brandNameTextView);
		userBrandSection.addContent(nameLabel);
		userBrandSection.addContent(nameTextView);
		userBrandAccordion.addSection( userBrandSection );
		
		userFeatureTable.setModel(new sap.ui.model.json.JSONModel(modelData.userInfo));
		userFeatureTable.bindRows({path: "/userFeature"});
		userFeatureTable.setVisibleRowCount((modelData.userInfo).length);
		userFeatureTable.getModel().refresh();
	},
	onAfterRendering: function() {
		var h = $( window ).height()-136;
		 $(".tabContentArea").height(h);
	},
	
	onExit: function(){
	},
});