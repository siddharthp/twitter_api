var oTabStrip;
sap.ui.jsview("oui5mvc.tabs", {
	getControllerName: function() {
		return "oui5mvc.tabs";
	},
	
	// defines the UI of this View
	createContent: function(oController) 
	{
		jQuery.sap.require("js.custNavListItem");
		oStorage.put("openTabs", 0);
		oStorage.put("maxOpenTabs", 0);

		displayHeader();
		new sap.ui.core.Icon({src: "sap-icon://menu2", size: "26px", color: "white", hoverColor: "white", tooltip: "Show/Hide Menu",
			press: function() {
				// code to toogling menu
				var oTree = sap.ui.getCore().byId("leftNavigation");
				if (oTree){
					if (oTree.getVisible()){
						oTree.setVisible(false);
						$("#leftNav").removeClass("showLeftNav");
						$("#content").removeClass("squeezeContentArea");
						$("#content").addClass("fullArea");
					} 
					else {
						$("#leftNav").addClass("showLeftNav");
						$("#content").addClass("squeezeContentArea");
						$("#content").removeClass("fullArea");
						oTree.setVisible(true);
					}
				}
				/*
				// code to fireselect for selected tab again to render charts 
				var oTabCont = sap.ui.getCore().byId("tabContainer");
			
					jQuery.each(oTabCont.getItems(), function(idx, item){
					
						if (item.toString().indexOf(oTabCont.getSelectedItem().toString()) > -1) {
						
							oTabCont.setSelectedItem(item);
							
						}
					});
				//	code to set height
					 var h = $( window ).height()-136;
					 $(".tabContentArea").height(h);
					
			*/}
		}).placeAt("navIcon");
		oToolbar = new sap.m.Toolbar({
			content:
			[
				new sap.ui.core.Icon({src: "sap-icon://overview-chart", size: "26px", color: "white", hoverColor: "white", tooltip: "About System",
					press: function() {
						var oAboutDialog = new sap.ui.commons.Dialog({title: "About", modal: true, width: "450px"});
						var oSimpleForm1 = new sap.ui.layout.form.SimpleForm({
							maxContainerCols: 1,
							content:
							[
							 	new sap.ui.commons.Label({text: "Version", textAlign: "Center", design: "Bold", width: "100%"}),
							 	new sap.ui.commons.TextView({text: oStorage.get("guiver")}),
							 	new sap.ui.commons.Label({text: "Copyright", textAlign: "Center", design: "Bold", width: "100%"}),
							 	new sap.ui.commons.TextView({text: "Copyright 2015 Prospecta Technologies India Pvt Ltd"}),
							 	new sap.ui.commons.Label({text: "License", textAlign: "Center", design: "Bold", width: "100%"}),
							 	new sap.ui.commons.TextView({text: "This product includes software developed by the Free Software Foundation which is licensed under the terms of the GNU GPL (http://www.fsf.org)."}),
							 	new sap.ui.commons.Label({text: "Open Source Credits", textAlign: "Center", design: "Bold", width: "100%"}),
							 	new sap.ui.commons.TextView({text: "This product includes software developed by the Free Software Foundation which is licensed under the terms of the GNU GPL (http://www.fsf.org)."})
							]
						});
						oAboutDialog.addContent(oSimpleForm1);
						
						oAboutDialog.attachClosed(function(){
							sap.ui.core.BusyIndicator.hide();
						});
						oAboutDialog.open();
					}
				}),
			/*	new sap.ui.core.Icon({src: "sap-icon://sys-help", size: "26px", color: "white", hoverColor: "white", tooltip: "Help",
					press: function() {}
				}),*/
			/*	new sap.ui.core.Icon({src: "sap-icon://key", size: "26px", color: "white", hoverColor: "goldenrod", tooltip: "Change Password",
					press: function() {}
				}),*/
				new sap.ui.core.Icon({src: "sap-icon://log", size: "26px", color: "white", hoverColor: "red", tooltip: "Logout",
					press: function() {
						var jsonObjToSend = {} ;
						
						oController.doAjax("/unified/logout",jsonObjToSend).done(function(modelData){
					        window.location = "login";
					      });
					}
				})
			]
		}).placeAt("navIcon").addStyleClass("iconToolbar");
		showLeftNavigation("leftNav");
		
		oTabContainer = new sap.m.TabContainer("tabContainer", {
			items:
			[
			 	new sap.m.TabContainerItem({name: "I serve U Dashboard", content: [ new sap.ui.core.mvc.JSView({id:"iserveudashboard",viewName:"oui5mvc.iserveudashboard"}).addStyleClass("tabContentArea") ]}),
			],
			itemClose: function(event){
				var closingTabItem = event.getParameter("item");
				closingTabItem.removeContent();
				closingTabItem.destroyContent();
				
			}
		}).placeAt("content").addStyleClass("tabContainer");
	}
});