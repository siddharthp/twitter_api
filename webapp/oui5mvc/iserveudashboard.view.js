sap.ui.jsview("oui5mvc.iserveudashboard", {
	getControllerName : function() {
		//sap.ui.getCore().byId(this.getId()).setBusyIndicatorDelay(0).setBusy(true).addStyleClass("opaqueBusyIndicator");
		return "oui5mvc.iserveudashboard";
	},
	createContent : function(oController) {
		adppassesGlobalId = this.getId();
		oFlexbox = new sap.m.FlexBox({
			 layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12"}),
			  items: [],
			 
			}).addStyleClass("dashBoardCustomRFL"),
			oFlexbox.placeAt(this.getId());
			new sap.ui.layout.form.Form({
			layout: new sap.ui.layout.form.ResponsiveGridLayout(),
			formContainers: 
			[
				new sap.ui.layout.form.FormContainer({
					layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12"}),
					formElements:
					[
						new sap.ui.layout.form.FormElement({
							fields: 
							[
							  userNameLabelMain = new sap.ui.commons.TextView({layoutData: new sap.ui.layout.GridData({span: "L8 M8 S8"}),}),
							  userTypeLabel = new sap.ui.commons.TextView({layoutData: new sap.ui.layout.GridData({span: "L4 M4 S4"}),}),
							  userBalance = new sap.ui.commons.TextView({layoutData: new sap.ui.layout.GridData({span: "L8 M8 S8"}),}),
							  adminName = new sap.ui.commons.TextView({layoutData: new sap.ui.layout.GridData({span: "L4 M4 S4"}),}),
							  promotionalMessage = new sap.ui.commons.TextView({layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12"}),}),
							 
							 ]
						})
					]
				}),
				

				new sap.ui.layout.form.FormContainer({
					layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12"}),
					formElements:
					[
						new sap.ui.layout.form.FormElement({
							fields: 
							[
							 	userBrandAccordion = new sap.ui.commons.Accordion(),
								
									userFeatureTable = new sap.ui.table.Table({
										//width:"60%",
										selectionMode: sap.ui.table.SelectionMode.None,
										selectionBehavior: sap.ui.table.SelectionBehavior.RowOnly,
										//navigationMode: sap.ui.table.NavigationMode.Scrollable,
										//navigationMode: sap.ui.table.NavigationMode.Paginator,
										showNoData: false,
										expandFirstLevel: true,
										enableColumnReordering: true,
										layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12"}),
										//columnHeaderVisible:false,
										columns:
										[	
											new sap.ui.table.Column({
												label: new sap.ui.commons.Label({text:"Id",textAlign:"Center"}),
												template: new sap.ui.commons.TextView({textAlign:"Center"}).bindProperty("text", "id"),
												sortProperty: "id",
												filterProperty: "id",
												width: "20%",
												autoResizable: true,
											}),
											new sap.ui.table.Column({
												label: new sap.ui.commons.Label({text:"Feature Name",textAlign:"Center"}),
												template: new sap.ui.commons.TextView({textAlign:"Center"}).bindProperty("text", "featureName"),
												sortProperty: "featureName",
												filterProperty: "featureName",
												width: "20%",
												autoResizable: true,
											}),
											new sap.ui.table.Column({
												label: new sap.ui.commons.Label({text:"Active",textAlign:"Center"}),
												template: new sap.ui.commons.TextView({textAlign:"Center"}).bindProperty("text", "active"),
												sortProperty: "active",
												filterProperty: "active",
												width: "20%",
												autoResizable: true,
											}),
											
									],
												
									 }).addStyleClass("customTable"),
							 ]
						})
					]
				}),
			 
			 ]
		}).placeAt(this.getId()).addStyleClass("asmsDashboard adpPassDashboard");
		
	}
});