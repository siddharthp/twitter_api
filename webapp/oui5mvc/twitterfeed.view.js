var viewId;
sap.ui.jsview("oui5mvc.twitterfeed", {
	getControllerName: function() {
		return "oui5mvc.twitterfeed";
	},
	
	// defines the UI of this View
	createContent: function(oController) 
	{		
		pageGlobalId = this.getId();
		oFlexbox = new sap.m.FlexBox({
			 layoutData: new sap.ui.layout.GridData({span: "L12 M12 S12"}),
			  items: [
			          new sap.m.List({headerText : "This is a test."}),
			         /* new sap.m.FeedListItem({
			        	  
			          }),  */
			          
			          ],
			 
			}).addStyleClass("flex"),
			oFlexbox.placeAt(this.getId());
	}
});