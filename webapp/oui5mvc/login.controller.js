//get your appPath
var appPath = window.location.pathname.split('/')[1];
sap.ui.controller("oui5mvc.login", {
	// implement an event handler in the Controller
	loginAuth: function(login) {
	//url:"/"+appPath+path,
		sap.ui.core.BusyIndicator.show(1000);
	//	window.location="main";
		var that = this;
			this.doAjax("https://apiserveu.mybluemix.net/logintoken.json",login).done(function(modelData){
				oStorage.put("tokenId",modelData.token);
				oStorage.put("adminName",modelData.adminName);
				that.updateModelData();
			});
	},
	
	doAjax: function (path, content, type, async) {
        var params = {
            url:path,
            dataType: "json",
            contentType: "application/json",
            //accessControlAllowOrigin: '*',
            "collectionId":"4ffdc0bc-bee1-7ea4-0986-f07a63fa087c","responses":[],
            context: this,
            cache: false,
			error : handleAjaxError
        };
		
        params["type"] = type || "POST";
        if (async === false) {
             params["async"] = async;
        }
        if (content) {
             params["data"] = JSON.stringify(content);
        }
        return jQuery.ajax(params);
    },
	
  
	
    updateModelData: function (modelData) {
    
    	console.debug("Login Ajax Response: ", modelData);
    	
        var loginRes = "success";// modelData["result"];
        if( (loginRes != null)  && (loginRes=="success") )	{
        	window.location = "main";
        }
        else 
        {
        	jQuery.sap.require("sap.ui.commons.MessageBox");
        	sap.ui.commons.MessageBox.show(modelData["errortext"], sap.ui.commons.MessageBox.Icon.ERROR);
        	sap.ui.getCore().byId("loginbutton").setEnabled(true);
    		sap.ui.core.BusyIndicator.hide();
        }
    },
});