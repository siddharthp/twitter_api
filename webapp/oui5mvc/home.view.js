var viewId;
var address=false;
surListBox = new sap.ui.commons.ListBox("", 											
		{items :
			[
			 new sap.ui.core.ListItem( {text:"Mr", key:"Mr"}),
             new sap.ui.core.ListItem( {text:"Ms", key:"Ms"}),
             ]
		}
 );
typeListBox = new sap.ui.commons.ListBox("", 											
		{items :
			[
			 new sap.ui.core.ListItem( {text:"Own", key:"Own"}),
             new sap.ui.core.ListItem( {text:"Agency", key:"Agency"}),
             ]
		}
 );
contractListBox = new sap.ui.commons.ListBox("", 											
		{items :
			[
			 new sap.ui.core.ListItem( {text:"Regular", key:"Regular"}),
             new sap.ui.core.ListItem( {text:"Contract", key:"Contract"}),
             ]
		}
 );
sap.ui.jsview("oui5mvc.home", {
	getControllerName: function() {
		return "oui5mvc.home";
	},
	
	// defines the UI of this View
	createContent: function(oController) 
	{
		flightGlobalId = this.getId();
		
		 
		
		var oPage1 = new sap.m.Page("page1",{
			  title: "Page1", // string
			  showNavButton: false, // boolean
			  showHeader: false, // boolean
			  navButtonText: undefined, // string
			  enableScrolling: false, // boolean
			  icon: undefined, // sap.ui.core.URI
			  backgroundDesign: sap.m.PageBackgroundDesign.Standard, // sap.m.PageBackgroundDesign
			  navButtonType: sap.m.ButtonType.Back, // sap.m.ButtonType, since 1.12
			  showFooter: false, // boolean, since 1.13.1
			  tooltip: undefined, // sap.ui.core.TooltipBase
			     content:[
					
			     new sap.ui.view({id:"employee",viewName:"oui5mvc.employee", type:sap.ui.core.mvc.ViewType.JS}),
			     ]
			  });
		var oPage2 = new sap.m.Page("page2",{
			  title: "Page2", // string
			  showNavButton: false, // boolean
			  showHeader: false, // boolean
			  navButtonText: undefined, // string
			  enableScrolling: false, // boolean
			  icon: undefined, // sap.ui.core.URI
			  backgroundDesign: sap.m.PageBackgroundDesign.Standard, // sap.m.PageBackgroundDesign
			  navButtonType: sap.m.ButtonType.Back, // sap.m.ButtonType, since 1.12
			  showFooter: false, // boolean, since 1.13.1
			  tooltip: undefined, // sap.ui.core.TooltipBase
			    
			    content:[
			             new sap.ui.view({id:"address",viewName:"oui5mvc.address", type:sap.ui.core.mvc.ViewType.JS}),
			  	 ]
			  });
		
		
		var oPage3 = new sap.m.Page("page3",{
			  title: "Page3", // string
			  showNavButton: false, // boolean
			  showHeader: false, // boolean
			  navButtonText: undefined, // string
			  enableScrolling: false, // boolean
			  icon: undefined, // sap.ui.core.URI
			  backgroundDesign: sap.m.PageBackgroundDesign.Standard, // sap.m.PageBackgroundDesign
			  navButtonType: sap.m.ButtonType.Back, // sap.m.ButtonType, since 1.12
			  showFooter: false, // boolean, since 1.13.1
			  tooltip: undefined, // sap.ui.core.TooltipBase
			    
			    content:[
			             new sap.ui.view({id:"kinship",viewName:"oui5mvc.kinship", type:sap.ui.core.mvc.ViewType.JS}),
			  	 ]
			  });
		
		
		var oPage4 = new sap.m.Page("page4",{
			  title: "Page4", // string
			  showNavButton: false, // boolean
			  showHeader: false, // boolean
			  navButtonText: undefined, // string
			  enableScrolling: false, // boolean
			  icon: undefined, // sap.ui.core.URI
			  backgroundDesign: sap.m.PageBackgroundDesign.Standard, // sap.m.PageBackgroundDesign
			  navButtonType: sap.m.ButtonType.Back, // sap.m.ButtonType, since 1.12
			  showFooter: false, // boolean, since 1.13.1
			  tooltip: undefined, // sap.ui.core.TooltipBase
			    
			    content:[
			             new sap.ui.view({id:"trainings",viewName:"oui5mvc.trainings", type:sap.ui.core.mvc.ViewType.JS}),
			  	 ]
			  });
		
		
		var oPage5 = new sap.m.Page("page5",{
			  title: "Page5", // string
			  showNavButton: false, // boolean
			  showHeader: false, // boolean
			  navButtonText: undefined, // string
			  enableScrolling: false, // boolean
			  icon: undefined, // sap.ui.core.URI
			  backgroundDesign: sap.m.PageBackgroundDesign.Standard, // sap.m.PageBackgroundDesign
			  navButtonType: sap.m.ButtonType.Back, // sap.m.ButtonType, since 1.12
			  showFooter: false, // boolean, since 1.13.1
			  tooltip: undefined, // sap.ui.core.TooltipBase
			    
			    content:[
			             new sap.ui.view({id:"certificate",viewName:"oui5mvc.certificate", type:sap.ui.core.mvc.ViewType.JS}),
			  	 ]
			  });
		
		var oPage6 = new sap.m.Page("page6",{
			  title: "Page6", // string
			  showNavButton: false, // boolean
			  showHeader: false, // boolean
			  navButtonText: undefined, // string
			  enableScrolling: false, // boolean
			  icon: undefined, // sap.ui.core.URI
			  backgroundDesign: sap.m.PageBackgroundDesign.Standard, // sap.m.PageBackgroundDesign
			  navButtonType: sap.m.ButtonType.Back, // sap.m.ButtonType, since 1.12
			  showFooter: false, // boolean, since 1.13.1
			  tooltip: undefined, // sap.ui.core.TooltipBase
			    
			    content:[
			             new sap.ui.view({id:"additionalinfo",viewName:"oui5mvc.additionalinfo", type:sap.ui.core.mvc.ViewType.JS}),
			  	 ]
			  });
		
		
		
		var oAppContainer = new sap.m.NavContainer({
		    //	defaultTransitionName : "Show",
		    	initialPage : null,
		    	width:'100%',
		     afterNavigate : function (oEvent) {
		  //    alert('After Navigate');
		    	// alert(oAppContainer.getCurrentPage());
		     }
		    }).addStyleClass("navContainerdash");
		oAppContainer.addPage(oPage1);

		var oForm3 = new sap.ui.layout.form.Form({
			editable: true,
			layout: new sap.ui.layout.form.ResponsiveGridLayout(),
			formContainers: [
				new sap.ui.layout.form.FormContainer({
					formElements: [
						new sap.ui.layout.form.FormElement({
							fields: [
							         
									oSegmentedButtonIconLite = new sap.m.SegmentedButton({
									    items: [
									        new sap.m.SegmentedButtonItem({text: "Address",
									        	press:function(oEvent){
										        	  oAppContainer.addPage(oPage2);
												      oAppContainer.to("page2");
									        }}), 
									        new sap.m.SegmentedButtonItem({text: "Kinship",
									        	press:function(oEvent){
										        	  oAppContainer.addPage(oPage3);
												      oAppContainer.to("page3");
									            }
									        }),
									        new sap.m.SegmentedButtonItem({text: "Trainings",
									        	press:function(oEvent){
										        	  oAppContainer.addPage(oPage4);
												      oAppContainer.to("page4");
									            }	
									        }),
									        new sap.m.SegmentedButtonItem({text: "Certiﬁcates",
									        	press:function(oEvent){
										        	  oAppContainer.addPage(oPage5);
												      oAppContainer.to("page5");
									            }	
									        }),
									        new sap.m.SegmentedButtonItem({text: "Additional Info",
									        	press:function(oEvent){
										        	  oAppContainer.addPage(oPage6);
												      oAppContainer.to("page6");
									            }		
									        }),
									    ],
									    select: function () {
									    }
									}).setSelectedButton(""),
							         
							         
									/*	Address = new sap.ui.commons.Button({text:"Address", styled: true, lite: true,layoutData: new sap.ui.layout.ResponsiveFlowLayoutData({weight: 4}), 
											press:function(oEvent){
												  oAppContainer.addPage(oPage2);
											      oAppContainer.to("page2");
											     
											        Address.addStyleClass("highlighting");
											        Kinship.removeStyleClass("highlighting");
											        Trainings.removeStyleClass("highlighting");
											        Certiﬁcates.removeStyleClass("highlighting");
											        Additional.removeStyleClass("highlighting");
											     
											}
										}).addStyleClass("customButton"),
										Kinship = new sap.ui.commons.Button({text:"Kinship", styled: true, lite: true,layoutData: new sap.ui.layout.ResponsiveFlowLayoutData({weight: 4}), 
											press:function(oEvent){
												  oAppContainer.addPage(oPage3);
											      oAppContainer.to("page3");
											     
											        Address.removeStyleClass("highlighting");
											        Kinship.addStyleClass("highlighting");
											        Trainings.removeStyleClass("highlighting");
											        Certiﬁcates.removeStyleClass("highlighting");
											        Additional.removeStyleClass("highlighting");
											}
										}).addStyleClass("customButton"),
									 
									  Trainings = new sap.ui.commons.Button({text:"Trainings", styled: true, lite: true,layoutData: new sap.ui.layout.ResponsiveFlowLayoutData({weight: 4}), 
											press:function(oEvent){
												  oAppContainer.addPage(oPage4);
											      oAppContainer.to("page4");
											      
											       Address.removeStyleClass("highlighting");
											        Kinship.removeStyleClass("highlighting");
											        Trainings.addStyleClass("highlighting");
											        Certiﬁcates.removeStyleClass("highlighting");
											        Additional.removeStyleClass("highlighting");
											}
										}).addStyleClass("customButton"),
									 
									Certiﬁcates = new sap.ui.commons.Button({text:"Certiﬁcates", styled: true, lite: true,layoutData: new sap.ui.layout.ResponsiveFlowLayoutData({weight: 4}), 
											press:function(oEvent){
												  oAppContainer.addPage(oPage5);
											      oAppContainer.to("page5");
											     
											        Address.removeStyleClass("highlighting");
											        Kinship.removeStyleClass("highlighting");
											        Trainings.removeStyleClass("highlighting");
											        Certiﬁcates.addStyleClass("highlighting");
											        Additional.removeStyleClass("highlighting");
											}
										}).addStyleClass("customButton"),
									 
									Additional = new sap.ui.commons.Button({text:"Additional Info", styled: true, lite: true,layoutData: new sap.ui.layout.ResponsiveFlowLayoutData({weight: 4}), 
											press:function(oEvent){
												  oAppContainer.addPage(oPage6);
											      oAppContainer.to("page6");
											      
											        Address.removeStyleClass("highlighting");
											        Kinship.removeStyleClass("highlighting");
											        Trainings.removeStyleClass("highlighting");
											        Certiﬁcates.removeStyleClass("highlighting");
											        Additional.addStyleClass("highlighting");
											      
											}
										}).addStyleClass("customButton"),*/
							
							],
							layoutData: new sap.ui.layout.ResponsiveFlowLayoutData({linebreak: true, margin: false})
						}),
					],
					layoutData: new sap.ui.core.VariantLayoutData({
						multipleLayoutData: [new sap.ui.layout.form.GridContainerData({halfGrid: true}),
						            	             new sap.ui.layout.ResponsiveFlowLayoutData({minWidth: 150}),
																			 new sap.ui.layout.GridData({linebreakL: false})]
						})
				}),
			]
		});
		
		oForm3.placeAt(this.getId());
		
		oAppContainer.placeAt(this.getId());
		
		
		}
});