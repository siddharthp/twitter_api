package com.api.twitter.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.ibatis.session.SqlSession;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.api.twitter.mapper.TweetMapper;
import com.api.twitter.service.MyBatisUtil;
import com.api.twitter.service.TwitterService;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

@Controller
@RequestMapping("/main")
public class TwitterHandlerController {
//heroku
	public void sendTweetToDB(Map tweet){
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try
        {
            TweetMapper mapper = sqlSession.getMapper(TweetMapper.class);
            mapper.insertTweet(tweet);
            sqlSession.commit();
        } finally
        {
            sqlSession.close();
        }
	}
	@RequestMapping(value = { "/twitter_data" }, method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody Map getTweetsWithHashTag(@Valid @RequestBody String requestBody,HttpSession session) throws TwitterException
	{ 
			Map guiMapMessage = jsonToMap(requestBody);
			long highestId = 0;
			if(guiMapMessage.get("highestId") instanceof Long){
				highestId = (Long)guiMapMessage.get("highestId");
			}
		    String hashtag = "#thankyoutaker";
		 	/*ConfigurationBuilder cb = new ConfigurationBuilder();
		    cb.setDebugEnabled(true)
		            .setOAuthConsumerKey("xGAktkk9ycliJ99Dd3nzzacp2 ")
		            .setOAuthConsumerSecret("scWm2vZfJxoJOiJmfklOi9G7XjFNoda0BPxjm19U4Ef5UJaLmT")
		            .setOAuthAccessToken("247837571-LuxOq6Wl0zy9Hkgt9rPhQajq0lOOvmVw4zvMVfST")
		            .setOAuthAccessTokenSecret("8stdn4hX7fFX9gBCsOH4MysKpPLj6De9Ld8qprLstpnsf");

		    TwitterFactory tf = new TwitterFactory(cb.build());*/
		    Twitter twitter = new TwitterFactory().getInstance();
		    twitter.setOAuthConsumer("xGAktkk9ycliJ99Dd3nzzacp2", "scWm2vZfJxoJOiJmfklOi9G7XjFNoda0BPxjm19U4Ef5UJaLmT");
            twitter.setOAuthAccessToken(new AccessToken("247837571-LuxOq6Wl0zy9Hkgt9rPhQajq0lOOvmVw4zvMVfST", "8stdn4hX7fFX9gBCsOH4MysKpPLj6De9Ld8qprLstpnsf"));
		 // Twitter twitter = new TwitterFactory().getInstance();
		 Query query = new Query(hashtag);
		 if(highestId != 0)
		 query.sinceId(highestId);
	     QueryResult qr = twitter.search(query);
	     List<Status> qrTweets = qr.getTweets();
	     Map responseMap = new HashMap();
	     responseMap.put("data", qrTweets);
	     // System.out.println(qrTweets.get(0));
		 return responseMap;
	}
	/*public static void main(String[] args) throws TwitterException {
		TwitterHandlerController thc = new TwitterHandlerController();
		thc.getTweetsWithHashTag("#thankyoutaker");
	}*/
	  /**
	    * General method to convert JSON format string to Map.
	    * 
	    * @param jsonStr
	    *           JSON input data from WebUI
	    * @return java.util.Map
	    * @throws org.json.JSONException
	    *            occurs when there is an issue with JSON operations
	    */
	   public static Map<String, Object> jsonToMap(String jsonStr) throws JSONException
	   {
	      Map<String, Object> retMap = new HashMap<String, Object>();
	      JSONObject jsonObject = new JSONObject(jsonStr);

	      if (null != jsonObject)
	      {
	         retMap = toMap(jsonObject);
	      }
	      return retMap;
	   }
	   
	   /**
	    * General method to convert JSON object to Map.
	    * 
	    * @param object
	    *           JSON object input from WebUI
	    * @return java.util.Map
	    * @throws org.json.JSONException
	    *            occurs when there is issue with JSON operations
	    */
	   public static Map<String, Object> toMap(JSONObject object) throws JSONException
	   {
	      Map<String, Object> map = new HashMap<String, Object>();

	      Iterator<String> keysItr = object.keys();
	      while (keysItr.hasNext())
	      {
	         String key = keysItr.next();
	         Object value = object.get(key);
	         if (value instanceof JSONArray)
	         {
	            value = toList((JSONArray) value);
	         }
	         else if (value instanceof JSONObject)
	         {
	            value = toMap((JSONObject) value);
	         }
	         map.put(key, value);
	      }
	      return map;
	   }
	   
	   /**
	    * General method to convert JSONArray to Map.
	    * 
	    * @param array
	    *           JSON input data from WebUI
	    * @return java.util.Map
	    * @throws org.json.JSONException
	    *            occurs when there is issue with JSON operations
	    */
	   public static List<Object> toList(JSONArray array) throws JSONException
	   {
	      List<Object> list = new ArrayList<Object>();
	      for (int i = 0; i < array.length(); i++)
	      {
	         Object value = array.get(i);
	         if (value instanceof JSONArray)
	         {
	            value = toList((JSONArray) value);
	         }
	         else if (value instanceof JSONObject)
	         {
	            value = toMap((JSONObject) value);
	         }
	         list.add(value);
	      }
	      return list;
	   }
}
